import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;

public class SimulationFrame {

	private JPanel panel;
	private JFrame frame;
	private JTextArea textArea;
	public JTextField textField;
	public JTextField maxTime_text;
	public JTextField minProcessingTime_text;
	public JTextField maxProcessingTime_text;
	public JTextField numberOfClients_text;
	public JTextField maxClientsPerServer_text;
	public JTextField nrOfServers_text;
	private JLabel maxTime_label;
	private JLabel minProcessingTime_label;
	private JLabel maxProcessingTime_label;
	private JLabel numberOfClients_label;
	private JLabel maxClientsPerServer_label;
	private JLabel nrOfServers_label;
	private JScrollPane scroll;
	public JButton start;
	private JToggleButton modeSwitch;
	private int WIDTH = 600, HEIGHT = 300;

	public SimulationFrame() {
		panel = new JPanel();
		frame = new JFrame();
		maxTime_text = new JTextField();
		minProcessingTime_text = new JTextField();
		maxProcessingTime_text = new JTextField();
		numberOfClients_text = new JTextField();
		maxClientsPerServer_text = new JTextField();
		nrOfServers_text = new JTextField();
		start = new JButton("Start");
		modeSwitch = new JToggleButton("Mod");
		nrOfServers_label = new JLabel("Numar case:");
		maxClientsPerServer_label = new JLabel("Nr max/casa:");
		minProcessingTime_label = new JLabel("Min procesare:");
		maxProcessingTime_label	= new JLabel("Max procesare:");
		numberOfClients_label = new JLabel("Numar clienti:");
		maxTime_label = new JLabel("Perioada de functionare");
		textField = new JTextField();
		panel.setLayout(null);
		frame.setResizable(false);
		textArea = new JTextArea();
		textArea.setEditable(false);
		scroll = new JScrollPane(textArea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
//		textArea.setBounds(10, 10, 320, 400);
		modeSwitch.setBounds(50,220,80,20);
		maxTime_label.setBounds(370, 10,150, 20);
		numberOfClients_label.setBounds(370, 30,150, 20);
		maxProcessingTime_label.setBounds(370, 50,150, 20);
		minProcessingTime_label.setBounds(370, 70,150, 20);
		maxClientsPerServer_label.setBounds(370, 90,150, 20);
		nrOfServers_label.setBounds(370,110,150, 20);
		
		maxTime_text.setBounds(530, 10, 40, 20);
		numberOfClients_text.setBounds(530, 30,40, 20);
		maxProcessingTime_text.setBounds(530, 50,40, 20);
		minProcessingTime_text.setBounds(530, 70,40, 20);
		maxClientsPerServer_text.setBounds(530, 90,40, 20);
		nrOfServers_text.setBounds(530, 110,40, 20);
		start.setBounds(440, 140, 70, 30);
		scroll.setBounds(10, 10, 350, 200);
		textField.setBounds(10, 220, 30, 20);
		
		start.addActionListener(new ActionListener(){
			public int timeLimit = 0;
			public int maxProcessingTime = 0;
			public int minProcessingTime = 0;
			public int numberOfServers = 0;
			public int numberOfClients = 0;
			public int maxClientsPerServer = 0;
			public boolean mode = false;
			
			public void actionPerformed(ActionEvent e) {
				timeLimit = Integer.parseInt(maxTime_text.getText());
				maxProcessingTime = Integer.parseInt(maxProcessingTime_text.getText());
				minProcessingTime = Integer.parseInt(minProcessingTime_text.getText());
				numberOfServers = Integer.parseInt(nrOfServers_text.getText());
				numberOfClients = Integer.parseInt(numberOfClients_text.getText());
				maxClientsPerServer = Integer.parseInt(maxClientsPerServer_text.getText());
				mode = modeSwitch.isSelected();
				SimulationManager manager = new SimulationManager(timeLimit, maxProcessingTime,  minProcessingTime,  numberOfServers,
						 numberOfClients,  maxClientsPerServer,  mode, frame);
				Thread t = new Thread(manager);
				t.start();
			}
			
		});
		
		panel.add(nrOfServers_text);
		panel.add(nrOfServers_label);
		panel.add(maxClientsPerServer_text);
		panel.add(maxClientsPerServer_label);
		panel.add(scroll);
		panel.add(modeSwitch);
		panel.add(textField);
		panel.add(maxTime_label);
		panel.add(numberOfClients_label);
		panel.add(maxProcessingTime_label);
		panel.add(minProcessingTime_label);
		panel.add(start);
		panel.add(maxTime_text);
		panel.add(numberOfClients_text);
		panel.add(maxProcessingTime_text);
		panel.add(minProcessingTime_text);
		
		
		frame.setSize(WIDTH, HEIGHT);
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void displayData(ArrayList<Server> servers) {
		frame.revalidate();
		Iterator<Server> s;
		Iterator<Client> c;
		String toPrint = "";
		for(s = servers.iterator();s.hasNext();){
			Server temp = s.next();
			toPrint = toPrint + " " + temp.getName() + "  :  ";
			for(c = temp.getClients().iterator();c.hasNext();){
				Client tempp = c.next();
				toPrint = toPrint + tempp.getName() + "  ";
			}
			toPrint += "\n\n";
		}
		textArea.setText(toPrint);
//		System.out.print(toPrint);
	}
	
}
