
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	int counterServiceTime = 0;
	int counterClients = 0;
	int counterWaitingTime = 0;
	int counterEmptyQueue = 0;
	int serviceTimeAverage = 0;
	int waitingTimeAverage = 0;
	private String name;
	private AtomicInteger waitingPeriod = new AtomicInteger(0);
	private ArrayList<Client> clients;
	private int maxClientsPerServer;
	public int maxTime;
	public PrintWriter writer;
	boolean first = false;
	int delay = 0;
	int timer = 0;

	public Server(String name, int maxSize, PrintWriter writer) {
		clients = new ArrayList<Client>(maxSize);
		this.name = name;
		maxClientsPerServer = maxSize;
		this.writer = writer;
	}

	public void addClient(Client newClient) {
		int counter = 0;
		if(first == false){
			delay = newClient.getArrivalTime();
			first = true;
		}
		if (clients.size() == maxClientsPerServer) {
			counter++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			newClient.setArrivalTime(newClient.getArrivalTime() + counter);
			int var = newClient.getProcessingTime() + timer + delay;
			newClient.setFinishTime(var);
			counter = 0;

			System.out.println(name + ": a ajuns " + newClient.getName() + "(" + newClient.getProcessingTime() + ") "
					+ " la " + newClient.getArrivalTime());
			writer.println(name + ": a ajuns " + newClient.getName() + "(" + newClient.getProcessingTime() + ") "
					+ " la " + newClient.getArrivalTime());
			counterClients++;
			counterServiceTime += newClient.getProcessingTime();
			
			clients.add(newClient);
			timer += newClient.getProcessingTime();
			waitingPeriod.addAndGet(newClient.getProcessingTime());
		}
	}

	public ArrayList<Client> getClients() {
		return clients;
	}

	public void run() {
		while (true) {
			if (clients.size() != 0) {
				Client temp = clients.get(0);
				try {
					Thread.sleep(temp.getProcessingTime() * 1000);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				waitingPeriod.addAndGet(-temp.getProcessingTime());
				counterWaitingTime += temp.getFinishTime() - temp.getArrivalTime();
				System.out.println(
						name + ": a plecat " + temp.getName() + "(" + temp.getProcessingTime() + ") " + " la " + temp.getFinishTime());
				writer.println(
						name + ": a plecat " + temp.getName() + "(" + temp.getProcessingTime() + ") " + " la " + temp.getFinishTime());
				
				clients.remove(0);
			}else
			{
				counterEmptyQueue++;
			}

			try {
				Thread.sleep(1000); 
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}

	}

	public String getName() {
		return name;
	}

	public int getWaitingPeriod() {
		return waitingPeriod.get();
	}

	public void printClients() {
		Iterator<Client> i;
		for (i = clients.iterator(); i.hasNext();) {
			Client temp = i.next();
			System.out.print(
					"( " + temp.getName() + "  " + temp.getArrivalTime() + " " + temp.getProcessingTime() + " )   ");
		}
		System.out.println();
	}
}
