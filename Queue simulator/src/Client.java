
public class Client implements Comparable<Client> {
private int arrivalTime;
private int processingTime;
private int finishTime;
private String name;
public Client(String name, int arrivalTime, int processingTime){
	this.arrivalTime = arrivalTime;
	this.processingTime = processingTime;
	this.name = name;
}
public String getName(){
	return this.name;
}
public int getProcessingTime(){
	return this.processingTime;
}
public void setFinishTime(int time){
	finishTime = time;
}
public int getFinishTime(){
	return finishTime;
}
public int getArrivalTime(){
	return this.arrivalTime;
}
public void setArrivalTime(int a){
	arrivalTime = a;
}
@Override
public int compareTo(Client o) {
	if(this.arrivalTime > o.getArrivalTime()){
		return 1;
	}else{
		if(this.arrivalTime < o.getArrivalTime()){
			return -1;
		}
	}
	return 0;
}
}

