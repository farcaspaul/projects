import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class SimulationManager implements Runnable {
	public int timeLimit = 0;
	public int maxProcessingTime = 0;
	public int minProcessingTime = 0;
	public int numberOfServers = 0;
	public int numberOfClients = 0;
	public int maxClientsPerServer = 0;
	public int peakMoment = 0;
	public int peakMomentClients = 0;
	public boolean mode = false;
	public ArrayList<Client> list;
	private Scheduler scheduler;
	private JFrame frame;
	private JTextArea textArea;
	private JScrollPane scroll;

	public SimulationManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int numberOfServers,
			int numberOfClients, int maxClientsPerServer, boolean mode, JFrame frame) {
		this.mode = mode;
		this.frame = frame;
		this.timeLimit = timeLimit;
		this.maxProcessingTime = maxProcessingTime;
		this.minProcessingTime = minProcessingTime;
		this.numberOfServers = numberOfServers;
		this.numberOfClients = numberOfClients;
		this.maxClientsPerServer = maxClientsPerServer;
		frame.setLayout(null);
		textArea = new JTextArea();
		scroll = new JScrollPane(textArea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		textArea.setBounds(10, 10, 350, 200);
		scroll.setBounds(10, 10, 350, 200);

		frame.add(scroll);
		scheduler = new Scheduler(numberOfServers, maxClientsPerServer, timeLimit);
	}

	private void generateNRandomClients() {
		list = new ArrayList<Client>(numberOfClients);
		Random r = new Random();
		for (int i = 1; i <= numberOfClients; i++) {
			list.add(new Client("Client" + i + "", r.nextInt((timeLimit - 1) + 1) + 1,
					r.nextInt((maxProcessingTime - minProcessingTime) + 1) + minProcessingTime));
		}
	}

	public void run() {
		generateNRandomClients();
		int currentTime = 0;
		while (currentTime < (timeLimit + maxProcessingTime)) {
			Iterator<Client> i;
			textArea.setText(displayData(scheduler.getServers()));
			scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			for (i = list.iterator(); i.hasNext();) {
				Client temp = i.next();
				if (temp.getArrivalTime() == currentTime) {
					scheduler.dispatchClient(temp, true);
					numberOfClients--;
				}
			}
			Iterator<Server> it;
			int tempSumClients = 0;
			for (it = scheduler.getServers().iterator(); it.hasNext();) {
				Server temp = it.next();
				tempSumClients += temp.getClients().size();
				if (tempSumClients > peakMomentClients) {
					peakMomentClients = tempSumClients;
					peakMoment = currentTime;
				}
			}
			tempSumClients = 0;
			currentTime++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		ArrayList<Server> listTemp = scheduler.getServers();

		while (scheduler.checkIfEmpty() != 0) {
			currentTime++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			textArea.setText(displayData(scheduler.getServers()));
		}
		textArea.setText(displayData(scheduler.getServers()));
		for (int i = 0; i < numberOfServers; i++) {
			double avg = (double) listTemp.get(i).counterServiceTime / listTemp.get(i).counterClients;
			scheduler.writer.println();

			System.out.printf("\n" + listTemp.get(i).getName() + " avg service time = " + "%.2f", avg);
			scheduler.writer.printf("\n" + listTemp.get(i).getName() + " avg service time = " + "%.2f" + "\n", avg);
			scheduler.writer.println();

			avg = (double) listTemp.get(i).counterWaitingTime / listTemp.get(i).counterClients;
			System.out.printf("\n" + listTemp.get(i).getName() + " avg waiting time = " + "%.2f", avg);
			scheduler.writer.printf("\n" + listTemp.get(i).getName() + " avg waiting time = " + "%.2f" + "\n", avg);
			scheduler.writer.println();

			System.out.printf("\n" + listTemp.get(i).getName() + " empty queue time = " + "%d",
					listTemp.get(i).counterEmptyQueue - 1);
			scheduler.writer.printf("\n" + listTemp.get(i).getName() + " empty queue time = " + "%d" + "\n" + "\n",
					listTemp.get(i).counterEmptyQueue - 1);
			scheduler.writer.println();
		}
		System.out.printf("\nPeak time = %d - clients at peak time = %d\n", peakMoment, peakMomentClients);
		scheduler.writer.printf("\nPeak time = %d - clients at peak time = %d\n", peakMoment, peakMomentClients);
		scheduler.writer.println();

		System.out.println("\nDonezo");
		scheduler.writer.println("Done");
		scheduler.writer.close();
	}

	public String displayData(ArrayList<Server> servers) {
		frame.revalidate();
		Iterator<Server> s;
		Iterator<Client> c;
		String toPrint = "";
		for (s = servers.iterator(); s.hasNext();) {
			Server temp = s.next();
			toPrint = toPrint + " " + temp.getName() + "  :  ";
			for (c = temp.getClients().iterator(); c.hasNext();) {
				Client tempp = c.next();
				toPrint = toPrint + tempp.getName() + "  ";
			}
			toPrint += "\n\n";
		}
		return toPrint;
	}
}
