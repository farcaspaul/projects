import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Scheduler {
	private ArrayList<Server> servers;
	private ArrayList<Thread> threads;
	private int maxNoServers;
	private int maxClientsPerServer;
	private int maxTime;
	public PrintWriter writer;
	public Scheduler(int maxNoServers, int maxClientsPerServer, int maxTime) {
		this.maxTime = maxTime;
		this.maxNoServers = maxNoServers;
		this.maxClientsPerServer = maxClientsPerServer;
		
		servers = new ArrayList<Server>(maxNoServers);
		threads = new ArrayList<Thread>(maxNoServers);
		try{
		    writer = new PrintWriter("log.txt", "UTF-8");
		} catch (IOException e) {
		}
		for (int i = 0; i < this.maxNoServers; i++) {
			int a = i + 1;
			String s = "Casa" + a + "";
			Server temp = new Server(s, maxClientsPerServer, writer);
			temp.maxTime = maxTime;
			servers.add(temp);
			
			Thread th = new Thread(new Thread(temp));
			th.start();
			threads.add(th);
		}
	}

	// false = dupa numar true = dupa timp
	public void dispatchClient(Client client, boolean mode) {
		if (mode == false) {
			int min = servers.get(0).getClients().size();
			Server temp = servers.get(0);
			for (int i = 1; i < maxNoServers; i++) {
				if (servers.get(i).getClients().size() < min) {
					temp = servers.get(i);
					min = servers.get(i).getClients().size();
				}
			}
			temp.addClient(client);
		} else {
			int min = servers.get(0).getWaitingPeriod();
			Server temp = servers.get(0);
			for (int i = 1; i < maxNoServers; i++) {
				if (servers.get(i).getWaitingPeriod() < min) {
					temp = servers.get(i);
					min = servers.get(i).getWaitingPeriod();
				}
			}
			temp.addClient(client);
		}
	}
	
	public ArrayList<Server> getServers() {
		return servers;
	}


	public void printServers() {
		for (int i = 0; i < maxNoServers; i++) {
			System.out.print(servers.get(i).getName());
		}
	}
	
	public int checkIfEmpty(){
		for (int i = 0; i < maxNoServers; i++) {
			if(servers.get(i).getClients().size()!=0){
				return 1;
			}
		}
		return 0;
	}
}
