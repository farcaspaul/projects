package bll;

import java.util.ArrayList;

import dbaccess.ClientQueries;
import model.Client;

public class ClientBLL {
	public static ArrayList<Client> getClients(){
		return ClientQueries.getClients();
	}
	public static void insert(Client client){
		ClientQueries.insert(client);
	}
	public static void update(Client client, int id){
		ClientQueries.update(client, id);
	}
	public static void deleteById(int id){
		ClientQueries.deleteById(id);
	}
}
