package bll;

import java.util.ArrayList;

import dbaccess.ProdusQueries;
import model.Produs;

public class ProdusBLL {
	public static ArrayList<Produs> getProducts(){
		return ProdusQueries.getProducts();
	}
	public static void insert(Produs produs){
		ProdusQueries.insert(produs);
	}
	public static void update(Produs client, int id){
		ProdusQueries.update(client, id);
	}
	public static void deleteById(int id){
		ProdusQueries.deleteById(id);
	}
}
