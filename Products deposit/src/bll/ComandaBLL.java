package bll;

//import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import dbaccess.ComandaQueries;

public class ComandaBLL {
	private static PrintWriter writer;
	public static boolean checkCantitate(int cerere, int id){
		if(ComandaQueries.getCantitate(id) >= cerere){
			return true;
		}else{
			return false;
		}
	}
	public static void creare_comanda(int idClient, int idProdus, int cantitate){
		ComandaQueries.creare_comanda(idClient,idProdus,cantitate);
	}
	public static void printareComanda(int counter,String[][] dateClient, String[][] dateProdus, int clientIndex, int produsIndex, String cantitate){
		try {
			writer = new PrintWriter("C:/Users/Paul/Desktop/Comanda" + counter + ".txt");
			writer.println("Nume: " + dateClient[clientIndex][1]);
			writer.println("Adresa: " + dateClient[clientIndex][2]);
			writer.println("Email: " + dateClient[clientIndex][3]);
			writer.println("Varsta: " + dateClient[clientIndex][4]);
			writer.println("Produs: " + dateProdus[produsIndex][1]);
			writer.println("Bucati: " + cantitate);
			writer.println("De plata: " + Integer.parseInt(cantitate)*Float.parseFloat(dateProdus[produsIndex][2]));
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
