package model;

public class Comanda {
	private int id;
	private int id_client;
	private int id_produs;
	private int cantitate;

	public Comanda() {
	}

	public Comanda(int id_client, int id_produs, int cantitate) {
		this.id_client = id_client;
		this.id_produs = id_produs;
		this.cantitate = cantitate;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	public void setId_produs(int id_produs) {
		this.id_produs = id_produs;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public int getId_client() {
		return this.id_client;
	}

	public int getId_produs() {
		return this.id_produs;
	}

	public 	int getCantitate() {
		return this.cantitate;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}
}

