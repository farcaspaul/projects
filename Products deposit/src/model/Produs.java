package model;

public class Produs {
	private int id;
	private String nume;
	private float pret;
	private int cantitate;

	public Produs() {
	}

	public Produs(String nume, float pret, int cantitate) {
		this.nume = nume;
		this.pret = pret;
		this.cantitate = cantitate;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public void setPret(float pret) {
		this.pret = pret;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public String getNume() {
		return nume;
	}

	public float getPret() {
		return pret;
	}

	public int getCantitate() {
		return cantitate;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}
}
