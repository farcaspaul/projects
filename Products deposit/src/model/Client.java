package model;

public class Client {
	private int id;
	private String name;
	private String address;
	private String email;
	private int age;

	public Client() {
	}

	public Client(String name, String address, String email, int age) {
		this.name = name;
		this.address = address;
		this.email = email;
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return this.name;
	}

	public String getAddress() {
		return this.address;
	}

	public String getEmail() {
		return this.email;
	}

	public int getAge() {
		return this.age;
	}
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id = id;
	}
}
