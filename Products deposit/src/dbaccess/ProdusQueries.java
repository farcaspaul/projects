package dbaccess;

import model.Produs;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import connection.ConnectionFactory;
public class ProdusQueries {
		private final static String deleteStatementString = "DELETE FROM produs WHERE id = ?";
		private final static String safeDelete = "DELETE FROM comanda WHERE id_produs = ?";
		private final static String insertStatementString = "INSERT INTO produs (nume, pret, cantitate) VALUES(?,?,?)";
		private final static String updateStatementString = "UPDATE produs SET nume = ?,pret= ?, cantitate = ? WHERE id = ? ";
		private final static String printStatementString = "SELECT * FROM produs";
		
		public static void deleteById(int id) {

			Connection dbConnection = ConnectionFactory.getConnection();
			PreparedStatement findStatement = null;
			PreparedStatement statement_safeDelete = null;
			ResultSet rs = null;
			try {
				statement_safeDelete = (PreparedStatement) dbConnection.prepareStatement(safeDelete);
				findStatement = (PreparedStatement) dbConnection.prepareStatement(deleteStatementString);
				findStatement.setInt(1, id);
				statement_safeDelete.setInt(1, id);
				statement_safeDelete.executeUpdate();
				findStatement.executeUpdate();

				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(statement_safeDelete);
				ConnectionFactory.close(dbConnection);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		public static void insert(Produs produs) {

			Connection dbConnection = ConnectionFactory.getConnection();
			PreparedStatement findStatement = null;
			try {
				findStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString);
				findStatement.setString(1, produs.getNume());
				findStatement.setFloat(2, produs.getPret());
				findStatement.setInt(3, produs.getCantitate());
				findStatement.executeUpdate();

				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		public static void update(Produs produs, int id){
			Connection dbConnection = ConnectionFactory.getConnection();
			PreparedStatement findStatement = null;
			ResultSet rs = null;
			try {
				findStatement = (PreparedStatement) dbConnection.prepareStatement(updateStatementString);
				findStatement.setString(1, produs.getNume());
				findStatement.setFloat(2, produs.getPret());
				findStatement.setInt(3, produs.getCantitate());
				findStatement.setInt(4, id);
				findStatement.executeUpdate();

				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		public static ArrayList<Produs> getProducts(){
			ArrayList<Produs> list = new ArrayList<Produs>();
			Produs temp;
			Connection dbConnection = ConnectionFactory.getConnection();
			Statement findStatement = null;
			ResultSet rs = null;
			try {
				findStatement = (PreparedStatement) dbConnection.prepareStatement(printStatementString);
				rs = findStatement.executeQuery(printStatementString);
				while(rs.next()){
					temp = new Produs();
					temp.setId(rs.getInt("id"));
					temp.setNume(rs.getString("nume"));
					temp.setPret(rs.getFloat("pret"));
					temp.setCantitate(rs.getInt("cantitate"));
					list.add(temp);
				}
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list;
		}
	}
