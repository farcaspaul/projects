package dbaccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import connection.ConnectionFactory;
import model.Comanda;

public class ComandaQueries {
	final static String crearecomanda = "call creare_comanda(?,?,?);";
	final static String verificarestoc = "SELECT cantitate FROM produs WHERE id = ?";
	final static String printStatementString = "SELECT * FROM comanda";
	
	public static int getCantitate(int id) {
		int toReturn = 0;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(verificarestoc);
			findStatement.setInt(1, id);
			rs = findStatement.executeQuery();
			rs.next();
			toReturn = rs.getInt("cantitate");
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return toReturn;
	}

	public static void creare_comanda(int id_client,int id_produs, int cantitate) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(crearecomanda);
			findStatement.setInt(1, id_client);
			findStatement.setInt(2, id_produs);
			findStatement.setInt(3, cantitate);
			findStatement.executeUpdate();

			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public  static ArrayList<Comanda> getProducts(){
		ArrayList<Comanda> list = new ArrayList<Comanda>();
		Comanda temp;
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(printStatementString);
			rs = findStatement.executeQuery(printStatementString);
			while(rs.next()){
				temp = new Comanda();
				temp.setId(rs.getInt("id"));
				temp.setId_client(rs.getInt("id_client"));
				temp.setId_produs(rs.getInt("id_produs"));
				temp.setCantitate(rs.getInt("cantitate"));
				list.add(temp);
			}
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

}
