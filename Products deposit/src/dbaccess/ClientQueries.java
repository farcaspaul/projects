package dbaccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import connection.ConnectionFactory;
import model.Client;

public class ClientQueries {
	private final static String deleteStatementString = "DELETE FROM client WHERE id = ?";
	private final static String safeDelete = "DELETE FROM comanda WHERE id_client = ?";
	private final static String insertStatementString = "INSERT INTO client (name, address, email,age) VALUES(?,?,?,?)";
	private final static String updateStatementString = "UPDATE client SET name = ?,address = ?, email = ?, age = ? WHERE id = ? ";
	private final static String printStatementString = "SELECT * FROM CLIENT";
	
	public static void deleteById(int id) {

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		PreparedStatement statement_safeDelete = null;

		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(deleteStatementString);
			statement_safeDelete = (PreparedStatement) dbConnection.prepareStatement(safeDelete);
			findStatement.setInt(1, id);
			statement_safeDelete.setInt(1, id);
			statement_safeDelete.executeUpdate();
			findStatement.executeUpdate();

			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(statement_safeDelete);
			ConnectionFactory.close(dbConnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void insert(Client client) {

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString);
			findStatement.setString(1, client.getName());
			findStatement.setString(2, client.getAddress());
			findStatement.setString(3, client.getEmail());
			findStatement.setInt(4,client.getAge());
			findStatement.executeUpdate();

			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void update(Client client, int id){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(updateStatementString);
			findStatement.setString(1, client.getName());
			findStatement.setString(2, client.getAddress());
			findStatement.setString(3, client.getEmail());
			findStatement.setInt(4,client.getAge());
			findStatement.setInt(5, id);
			findStatement.executeUpdate();

			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Client> getClients(){
		ArrayList<Client> list = new ArrayList<Client>();
		Client temp;
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(printStatementString);
			rs = findStatement.executeQuery(printStatementString);
			while(rs.next()){
				temp = new Client();
				temp.setId(rs.getInt("id"));
				temp.setName(rs.getString("name"));
				temp.setAddress(rs.getString("address"));
				temp.setEmail(rs.getString("email"));
				temp.setAge(rs.getInt("age"));
				list.add(temp);
			}
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
}
