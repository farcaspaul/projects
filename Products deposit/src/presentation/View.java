package presentation;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.*;

import bll.ClientBLL;
import bll.ComandaBLL;
import bll.ProdusBLL;
//import dbaccess.ClientQueries;
import dbaccess.ProdusQueries;
import model.Client;
import model.Produs;

public class View {

	public JFrame frame, addClientFrame, editClientFrame, deleteClientFrame, deleteProductFrame, addProductFrame,
			editProductFrame;
	public JPanel panel, panelClient, panelProduct, panelOrder, addClientPanel, editClientPanel, deleteClientPanel,
			deleteProductPanel, editProductPanel, addProductPanel;
	public JFrame clientOperations;
	public JFrame productOperations;
	public JFrame createOrder;
	public JTable table, tableAdd, tableEdit, tableProdusAdd, tableProdusEdit, tableProdus, tableOrderProdus,
			tableOrderClient;
	public JScrollPane scroll, scrollAdd, scrollEdit, scrolltableProdus, scrollAddProduct, scrollEditProduct,
			scrollOrderClient, scrollOrderProdus;
	public Object[][] data = { { "", "", "", "", "" } };
	public Object[][] dataProdus = { { "", "", "", "" } };
	public Object[][] dataProdusNoID = { { "", "", "" } };
	public Object[][] dataProdusNoIDAdd = { { "", "", "" } };
	public Object[][] dataProdusNoIDEdit = { { "", "", "" } };
	public Object[][] dataNoID = { { "", "", "", "" } };
	public Object[][] dataNoIDAdd = { { "", "", "", "" } };
	public Object[][] dataNoIDEdit = { { "", "", "", "" } };
	public String[] columnNames = { "Id", "Name", "Adresa", "Email", "Varsta", };
	public String[] columnNamesNoID = { "Name", "Adresa", "Email", "Varsta", };
	public String[] columnNamesProdus = { "Id", "Nume", "Pret", "Cantitate" };
	public String[] columnNamesProdusNoID = { "Nume", "Pret", "Cantitate" };
	public JButton client, product, order, addClient, editClient, deleteClient, viewClients, addProduct, editProduct,
			deleteProduct, viewProducts, goClientAdd, goProdusAdd, goClientEdit, goClientDelete, goProductDelete,
			goProductEdit, goOrder;
	JFrame frameOrder;
//	File file;
	private int counter = 0;
	private PrintWriter writer;;

	public View() {
		panelOrder = new JPanel();
		frameOrder = new JFrame("Comanda");
		frame = new JFrame("Aplicatie");
		panel = new JPanel();
		panel.setLayout(null);
		client = new JButton("Operatii client");
		product = new JButton("Operatii produse");
		order = new JButton("Creare comanda");
		data = new String[0][5];
		table = new JTable(data, columnNames);
		scroll = new JScrollPane(table);
		tableProdus = new JTable(dataProdus, columnNamesProdus);
		scrolltableProdus = new JScrollPane(tableProdus);
		frame.setSize(412, 100);
		client.setBounds(10, 10, 115, 30);
		product.setBounds(130, 10, 130, 30);
		order.setBounds(265, 10, 130, 30);

		client.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				panelClient = new JPanel();
				panelClient.setLayout(null);
				addClient = new JButton("Add");
				editClient = new JButton("Edit");
				deleteClient = new JButton("Delete");
				viewClients = new JButton("View");

				table = new JTable(data, columnNames);

				viewClients.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						ArrayList<Client> list = ClientBLL.getClients();
						Object[][] dataAfisat = new String[list.size()][5];
						Iterator<Client> it;
						int cnt = 0;
						for (it = list.iterator(); it.hasNext();) {
							Client temp = it.next();
							dataAfisat[cnt][0] = temp.getId() + "";
							dataAfisat[cnt][1] = temp.getName();
							dataAfisat[cnt][2] = temp.getAddress();
							dataAfisat[cnt][3] = temp.getEmail();
							dataAfisat[cnt][4] = temp.getAge() + "";
							cnt++;
						}
						table = new JTable(dataAfisat, columnNames);
						table.setPreferredScrollableViewportSize(new Dimension(100, 100));
						table.setFillsViewportHeight(true);
						scroll = new JScrollPane(table);
						scroll.setBounds(10, 50, 390, 300);
						panelClient.add(scroll);
					}
				});

				addClient.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						addClientFrame = new JFrame("Adaugare client");
						addClientPanel = new JPanel();
						goClientAdd = new JButton("Insert");
						addClientFrame.setSize(412, 120);
						addClientPanel.setLayout(null);
						dataNoIDAdd = new String[1][4];
						tableAdd = new JTable(dataNoIDAdd, columnNamesNoID);
						scrollAdd = new JScrollPane(tableAdd);
						scrollAdd.setBounds(10, 10, 384, 38);
						goClientAdd.setBounds(145, 55, 100, 20);
						addClientPanel.add(scrollAdd);
						addClientPanel.add(goClientAdd);
						addClientFrame.add(addClientPanel);
						addClientFrame.setLocationRelativeTo(null);
						addClientFrame.setResizable(false);
						addClientFrame.setVisible(true);
						addClientFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

						goClientAdd.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								tableAdd.getCellEditor().stopCellEditing();
								Client temp = new Client();
								temp.setName((String) tableAdd.getValueAt(0, 0));
								temp.setAddress((String) tableAdd.getValueAt(0, 1));
								temp.setEmail((String) tableAdd.getValueAt(0, 2));
								temp.setAge(Integer.parseInt((String) tableAdd.getValueAt(0, 3)));
								ClientBLL.insert(temp);
								addClientFrame.dispose();
							}
						});
					}
				});

				editClient.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						JTextField id_text = new JTextField("Id-ul..");
						id_text.addMouseListener(new MouseListener() {

							public void mouseClicked(MouseEvent arg0) {
								id_text.setText("");
							}

							public void mouseEntered(MouseEvent arg0) {

							}

							@Override
							public void mouseExited(MouseEvent arg0) {

							}

							public void mousePressed(MouseEvent arg0) {

							}

							public void mouseReleased(MouseEvent arg0) {

							}

						});
						editClientFrame = new JFrame("Editare client");
						editClientPanel = new JPanel();
						goClientEdit = new JButton("Change");
						editClientFrame.setSize(412, 120);
						editClientPanel.setLayout(null);
						dataNoIDEdit = new String[1][4];
						tableEdit = new JTable(dataNoIDEdit, columnNamesNoID);
						scrollEdit = new JScrollPane(tableEdit);
						scrollEdit.setBounds(10, 10, 384, 38);
						goClientEdit.setBounds(145, 55, 100, 20);
						id_text.setBounds(10, 55, 50, 20);
						editClientPanel.add(scrollEdit);
						editClientPanel.add(goClientEdit);
						editClientPanel.add(id_text);
						editClientFrame.add(editClientPanel);
						editClientFrame.setLocationRelativeTo(null);
						editClientFrame.setResizable(false);
						editClientFrame.setVisible(true);
						editClientFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

						goClientEdit.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								tableEdit.getCellEditor().stopCellEditing();
								Client temp = new Client();
								// ClientQueries operatii = new ClientQueries();
								temp.setName((String) tableEdit.getValueAt(0, 0));
								temp.setAddress((String) tableEdit.getValueAt(0, 1));
								temp.setEmail((String) tableEdit.getValueAt(0, 2));
								temp.setAge(Integer.parseInt((String) tableEdit.getValueAt(0, 3)));
								ClientBLL.update(temp, Integer.parseInt(id_text.getText()));
								editClientFrame.dispose();
							}
						});
					}
				});

				deleteClient.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						JTextField id_text = new JTextField("Id-ul..");
						id_text.addMouseListener(new MouseListener() {

							public void mouseClicked(MouseEvent arg0) {
								id_text.setText("");
							}

							public void mouseEntered(MouseEvent arg0) {

							}

							public void mouseExited(MouseEvent arg0) {

							}

							public void mousePressed(MouseEvent arg0) {

							}

							public void mouseReleased(MouseEvent arg0) {

							}

						});
						goClientDelete = new JButton("Delete");
						deleteClientPanel = new JPanel();
						deleteClientFrame = new JFrame("Delete client");
						id_text.setBounds(10, 55, 50, 30);
						deleteClientFrame.setSize(182, 90);
						deleteClientPanel.add(goClientDelete);
						deleteClientPanel.add(id_text);
						deleteClientFrame.add(deleteClientPanel);
						deleteClientFrame.setLocationRelativeTo(null);
						deleteClientFrame.setResizable(false);
						deleteClientFrame.setVisible(true);
						deleteClientFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

						goClientDelete.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								// ClientQueries operatii = new ClientQueries();
								ClientBLL.deleteById(Integer.parseInt(id_text.getText()));
								deleteClientFrame.dispose();
							}
						});
					}
				});

				clientOperations = new JFrame("Operatii client");
				clientOperations.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				clientOperations.setResizable(false);
				scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
				clientOperations.setSize(412, 400);
				addClient.setBounds(50, 10, 70, 30);
				editClient.setBounds(130, 10, 70, 30);
				deleteClient.setBounds(210, 10, 70, 30);
				viewClients.setBounds(290, 10, 70, 30);

				panelClient.add(addClient);

				panelClient.add(editClient);
				panelClient.add(deleteClient);
				panelClient.add(viewClients);
				clientOperations.add(panelClient);
				clientOperations.setLocationRelativeTo(null);
				clientOperations.setVisible(true);
			}

		});

		product.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				panelProduct = new JPanel();
				panelProduct.setLayout(null);
				addProduct = new JButton("Add");
				editProduct = new JButton("Edit");
				deleteProduct = new JButton("Delete");
				viewProducts = new JButton("View");

				tableProdus = new JTable(dataProdus, columnNamesProdus);

				viewProducts.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						// ProdusQueries operatie = new ProdusQueries();
						ArrayList<Produs> list = ProdusQueries.getProducts();
						Object[][] dataAfisatProdus = new String[list.size()][4];
						Iterator<Produs> it;
						int cnt = 0;
						for (it = list.iterator(); it.hasNext();) {
							Produs temp = it.next();
							dataAfisatProdus[cnt][0] = temp.getId() + "";
							dataAfisatProdus[cnt][1] = temp.getNume();
							dataAfisatProdus[cnt][2] = temp.getPret() + "";
							dataAfisatProdus[cnt][3] = temp.getCantitate() + "";
							cnt++;
						}
						tableProdus = new JTable(dataAfisatProdus, columnNamesProdus);
						tableProdus.setPreferredScrollableViewportSize(new Dimension(100, 100));
						tableProdus.setFillsViewportHeight(true);
						scrolltableProdus = new JScrollPane(tableProdus);
						scrolltableProdus.setBounds(10, 50, 390, 300);
						panelProduct.add(scrolltableProdus);
					}
				});

				deleteProduct.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						JTextField id_text = new JTextField("Id-ul..");
						id_text.addMouseListener(new MouseListener() {

							public void mouseClicked(MouseEvent arg0) {
								id_text.setText("");
							}

							public void mouseEntered(MouseEvent arg0) {

							}

							@Override
							public void mouseExited(MouseEvent arg0) {

							}

							public void mousePressed(MouseEvent arg0) {

							}

							public void mouseReleased(MouseEvent arg0) {

							}

						});
						goProductDelete = new JButton("Delete");
						deleteProductPanel = new JPanel();
						deleteProductFrame = new JFrame("Delete product");
						id_text.setBounds(10, 55, 50, 30);
						deleteProductFrame.setSize(182, 90);
						deleteProductPanel.add(goProductDelete);
						deleteProductPanel.add(id_text);
						deleteProductFrame.add(deleteProductPanel);
						deleteProductFrame.setLocationRelativeTo(null);
						deleteProductFrame.setResizable(false);
						deleteProductFrame.setVisible(true);
						deleteProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

						goProductDelete.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								// ProdusQueries operatii = new ProdusQueries();
								ProdusBLL.deleteById(Integer.parseInt(id_text.getText()));
								deleteProductFrame.dispose();
							}
						});
					}
				});

				editProduct.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						JTextField id_text = new JTextField("Id-ul..");
						id_text.addMouseListener(new MouseListener() {

							public void mouseClicked(MouseEvent arg0) {
								id_text.setText("");
							}

							public void mouseEntered(MouseEvent arg0) {

							}

							@Override
							public void mouseExited(MouseEvent arg0) {

							}

							public void mousePressed(MouseEvent arg0) {

							}

							public void mouseReleased(MouseEvent arg0) {

							}

						});
						editProductFrame = new JFrame("Editare produs");
						editProductPanel = new JPanel();
						goProductEdit = new JButton("Change");
						editProductFrame.setSize(412, 120);
						editProductPanel.setLayout(null);
						dataProdusNoIDEdit = new String[1][3];
						tableProdusEdit = new JTable(dataProdusNoIDEdit, columnNamesProdusNoID);
						scrollEditProduct = new JScrollPane(tableProdusEdit);
						scrollEditProduct.setBounds(10, 10, 384, 38);
						goProductEdit.setBounds(145, 55, 100, 20);
						id_text.setBounds(10, 55, 50, 20);
						editProductPanel.add(scrollEditProduct);
						editProductPanel.add(goProductEdit);
						editProductPanel.add(id_text);
						editProductFrame.add(editProductPanel);
						editProductFrame.setLocationRelativeTo(null);
						editProductFrame.setResizable(false);
						editProductFrame.setVisible(true);
						editProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

						goProductEdit.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								tableProdusEdit.getCellEditor().stopCellEditing();
								Produs temp = new Produs();
								temp.setNume((String) tableProdusEdit.getValueAt(0, 0));
								temp.setPret(Float.parseFloat((String) tableProdusEdit.getValueAt(0, 1)));
								temp.setCantitate(Integer.parseInt((String) tableProdusEdit.getValueAt(0, 2)));
								ProdusBLL.update(temp, Integer.parseInt(id_text.getText()));
								editProductFrame.dispose();
							}
						});
					}
				});

				addProduct.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						JTextField id_text = new JTextField("Id-ul..");
						id_text.addMouseListener(new MouseListener() {

							public void mouseClicked(MouseEvent arg0) {
								id_text.setText("");
							}

							public void mouseEntered(MouseEvent arg0) {

							}

							@Override
							public void mouseExited(MouseEvent arg0) {

							}

							public void mousePressed(MouseEvent arg0) {

							}

							public void mouseReleased(MouseEvent arg0) {

							}

						});
						addProductFrame = new JFrame("Adaugare produs");
						addProductPanel = new JPanel();
						goProdusAdd = new JButton("Adauga");
						addProductFrame.setSize(412, 120);
						addProductPanel.setLayout(null);
						dataProdusNoIDAdd = new String[1][3];
						tableProdusAdd = new JTable(dataProdusNoIDAdd, columnNamesProdusNoID);
						scrollAddProduct = new JScrollPane(tableProdusAdd);
						scrollAddProduct.setBounds(10, 10, 384, 38);
						goProdusAdd.setBounds(145, 55, 100, 20);
						addProductPanel.add(scrollAddProduct);
						addProductPanel.add(goProdusAdd);
						addProductPanel.add(id_text);
						addProductFrame.add(addProductPanel);
						addProductFrame.setLocationRelativeTo(null);
						addProductFrame.setResizable(false);
						addProductFrame.setVisible(true);
						addProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

						goProdusAdd.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								tableProdusAdd.getCellEditor().stopCellEditing();
								Produs temp = new Produs();
								temp.setNume((String) tableProdusAdd.getValueAt(0, 0));
								temp.setPret(Float.parseFloat((String) tableProdusAdd.getValueAt(0, 1)));
								temp.setCantitate(Integer.parseInt((String) tableProdusAdd.getValueAt(0, 2)));
								ProdusBLL.insert(temp);
								addProductFrame.dispose();
							}
						});
					}
				});

				productOperations = new JFrame("Operatii produs");
				productOperations.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				productOperations.setResizable(false);
				scrolltableProdus.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
				productOperations.setSize(412, 400);
				addProduct.setBounds(50, 10, 70, 30);
				editProduct.setBounds(130, 10, 70, 30);
				deleteProduct.setBounds(210, 10, 70, 30);
				viewProducts.setBounds(290, 10, 70, 30);

				panelProduct.add(addProduct);

				panelProduct.add(editProduct);
				panelProduct.add(deleteProduct);
				panelProduct.add(viewProducts);
				productOperations.add(panelProduct);
				productOperations.setLocationRelativeTo(null);
				productOperations.setVisible(true);
			}

		});

		order.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				JTextField cantitate_text = new JTextField("Cantitatea..");
				cantitate_text.addMouseListener(new MouseListener() {

					public void mouseClicked(MouseEvent arg0) {
						cantitate_text.setText("");
					}

					public void mouseEntered(MouseEvent arg0) {

					}

					@Override
					public void mouseExited(MouseEvent arg0) {

					}

					public void mousePressed(MouseEvent arg0) {

					}

					public void mouseReleased(MouseEvent arg0) {

					}

				});
				frameOrder.setSize(830, 220);
				frameOrder.setLocationRelativeTo(null);
				ArrayList<Client> clientList = new ArrayList<Client>();
				ArrayList<Produs> produsList = new ArrayList<Produs>();
				// ClientQueries opClient = new ClientQueries();
				// ProdusQueries opProdus = new ProdusQueries();
				produsList = ProdusBLL.getProducts();
				clientList = ClientBLL.getClients();
				int cnt1 = 0, cnt2 = 0;
				String[][] dateProdus = new String[produsList.size()][4];
				String[][] dateClient = new String[clientList.size()][5];
				Iterator<Produs> it1;
				Iterator<Client> it2;
				for (it1 = produsList.iterator(); it1.hasNext();) {
					Produs temp = it1.next();
					dateProdus[cnt1][0] = temp.getId() + "";
					dateProdus[cnt1][1] = temp.getNume();
					dateProdus[cnt1][2] = temp.getPret() + "";
					dateProdus[cnt1][3] = temp.getCantitate() + "";
					cnt1++;
				}
				for (it2 = clientList.iterator(); it2.hasNext();) {
					Client temp = it2.next();
					dateClient[cnt2][0] = temp.getId() + "";
					dateClient[cnt2][1] = temp.getName();
					dateClient[cnt2][2] = temp.getAddress();
					dateClient[cnt2][3] = temp.getEmail();
					dateClient[cnt2][4] = temp.getAge() + "";
					cnt2++;
				}

				tableOrderProdus = new JTable(dateProdus, columnNamesProdus);
				tableOrderClient = new JTable(dateClient, columnNames);

				tableOrderProdus.setPreferredScrollableViewportSize(new Dimension(100, 100));
				tableOrderProdus.setFillsViewportHeight(true);

				tableOrderClient.setPreferredScrollableViewportSize(new Dimension(100, 100));
				tableOrderClient.setFillsViewportHeight(true);

				scrollOrderClient = new JScrollPane(tableOrderClient);
				scrollOrderProdus = new JScrollPane(tableOrderProdus);
				goOrder = new JButton("Comanda");

				goOrder.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						int idProdus = Integer.parseInt(dateProdus[tableOrderProdus.getSelectedRow()][0]);
						int idClient = Integer.parseInt(dateClient[tableOrderClient.getSelectedRow()][0]);
//						file = new File("C:/Users/Paul/Desktop/Comanda" + counter + ".txt");
						
						if (ComandaBLL.checkCantitate(Integer.parseInt(cantitate_text.getText()), idProdus)==false) {

							JOptionPane.showMessageDialog(frameOrder, "Stoc insuficient");
//							ComandaBLL.printareComanda(counter,dateClient,dateProdus,tableOrderClient.getSelectedRow(),tableOrderProdus.getSelectedRow(),cantitate_text.getText());
//							counter++;
//							writer.close();
						} else {
							try {
								writer = new PrintWriter("C:/Users/Paul/Desktop/Comanda" + counter + ".txt");
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}
							tableOrderProdus.setValueAt(
									Integer.parseInt(dateProdus[tableOrderProdus.getSelectedRow()][3])
											- Integer.parseInt(cantitate_text.getText()) + "",
									tableOrderProdus.getSelectedRow(), 3);

							ComandaBLL.creare_comanda(idClient, idProdus, Integer.parseInt(cantitate_text.getText()));
							ComandaBLL.printareComanda(counter,dateClient,dateProdus,tableOrderClient.getSelectedRow(),tableOrderProdus.getSelectedRow(),cantitate_text.getText());
							counter++;
							writer.close();
						}
					}

				});

				scrollOrderClient.setBounds(10, 10, 390, 130);
				scrollOrderProdus.setBounds(420, 10, 390, 130);
				panelOrder.setLayout(null);
				cantitate_text.setBounds(300, 150, 100, 20);
				goOrder.setBounds(420, 150, 90, 20);
				panelOrder.add(goOrder);
				panelOrder.add(cantitate_text);
				panelOrder.add(scrollOrderProdus);
				panelOrder.add(scrollOrderClient);
				frameOrder.add(panelOrder);
				frameOrder.setVisible(true);
				frameOrder.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			}
		});

		table.setPreferredScrollableViewportSize(new Dimension(100, 100));
		table.setFillsViewportHeight(true);
		panel.add(scroll);
		panel.add(order);
		panel.add(product);
		panel.add(client);
		frame.add(panel);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public JFrame getJFrame() {
		return frame;
	}
}
