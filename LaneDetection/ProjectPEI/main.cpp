//
//  main.cpp
//  ProjectPEI
//
//  Created by Fărcaș Paul on 05/05/2018.
//  Copyright © 2018 Fărcaș Paul. All rights reserved.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include <opencv2/imgproc/imgproc.hpp>



using namespace cv;
using namespace std;

Mat regionOfInterest(Mat img){
    Mat result(img.rows,img.cols, CV_8UC1);
    Mat mask = Mat::zeros(img.rows, img.cols, CV_8UC1);
    
    Point points[1][3];
    points[0][0] = Point( img.cols/6, img.rows );
    points[0][1] = Point( img.cols/2, img.rows*7/10 );
    points[0][2] = Point( img.cols-img.cols/6, img.rows );
    
    const Point* ppt[1] = { points[0] };
    int npt[] = { 3 };
    
    fillPoly( mask, ppt, npt, 1, Scalar( 255 ), 8 );

    bitwise_and(img, mask, result);
    return result;
}

Mat laneDetect(Mat image){
    Mat grayImage, HSVimage, edgesImage, croppedImage, yellowMask, whiteMask, yellowAndWhiteMask, yellowAndWhiteImage, linesImage;
        Scalar lowerYellow(20, 100, 100);
        Scalar upperYellow(30, 255, 255);
    //    image = imread("/Users/farcaspaul/XCode-workspace/ProjectPEI/ProjectPEI/Images/lane1.jpg");
  //      printf("The image is : height = %d  width = %d\n", image.cols,image.rows);
     //   imshow("Initial image",image);
    //    imshow("ROI image",regionOfInterest(image));
       // Mat image = regionOfInterest(img);
        cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
        cv::cvtColor(image, HSVimage, cv::COLOR_BGR2HSV);
    
        cv::inRange(grayImage,lowerYellow , upperYellow, yellowMask);
        cv::inRange(grayImage,200,255,whiteMask);
    
        bitwise_or(whiteMask, yellowMask, yellowAndWhiteMask);
    
        bitwise_and(grayImage, yellowAndWhiteMask, yellowAndWhiteImage);
    
        cv::GaussianBlur(yellowAndWhiteImage, yellowAndWhiteImage, Size(5,5), 0);
    
        cv::Canny(yellowAndWhiteImage,edgesImage,50,150);
    
        croppedImage = regionOfInterest(edgesImage);
   //     imshow("croppedImage ", croppedImage);
    
    
    //
    //    void HoughLinesP( InputArray image, OutputArray lines,
    //                     double rho, double theta, int threshold,
    //                     double minLineLength = 0, double maxLineGap = 0 );
    //
    
        Mat imageToDrawOn;
    
      // cv::cvtColor(croppedImage, imageToDrawOn, CV_GRAY2BGR);
        imageToDrawOn = Mat::zeros(croppedImage.rows, croppedImage.cols, CV_8UC3);
    
        std::vector<Vec4i> lines;
    
    
        HoughLinesP(croppedImage, lines, 1, CV_PI/180, 20, 10, 35);
        Vec4i leftFinalLine(lines[0][0],0,0,lines[0][3]);
        Vec4i rightFinalLine(0,0,lines[0][2],lines[0][3]);
        std::vector<Vec4i> leftLines, rightLines;
        for( size_t i = 0; i < lines.size(); i++ )
        {
            Vec4i currentLine = lines[i];
            int x1 = currentLine[0];
            int y1 = currentLine[1];
            int x2 = currentLine[2];
            int y2 = currentLine[3];
    
       //     printf("x1 = %d y1 = %d x2 = %d y2 = %d\n",x1,y1,x2,y2);
            double slope = (double)(y2-y1)/(x2-x1);
       //     printf("slope = %f\n",slope);
            if( abs(slope) < 0.5 ){
                continue; // trebuie panta mai mare
            }
                if(slope <= 0){
                    leftLines.push_back(currentLine);
                    if(currentLine[0]<leftFinalLine[0]){
                        leftFinalLine[0] = currentLine[0];
                    }
                    if(currentLine[1]>leftFinalLine[1]){
                        leftFinalLine[1] = currentLine[1];
                    }
                    if(currentLine[2]>leftFinalLine[2]){
                        leftFinalLine[2] = currentLine[2];
                    }
                    if(currentLine[3]<leftFinalLine[3]){
                        leftFinalLine[3] = currentLine[3];
                    }
    
    
                   // leftFinalLine[0]= 5; //x1 = minim x1     x2 = maxim x2      y1  = maxim y1      y2 = minim y2
                  //  printf("ACI %f\n",slope);
               //     line( imageToDrawOn, Point(currentLine[0], currentLine[1]), Point(currentLine[2], currentLine[3]), Scalar(0,0,255), 3, CV_AA);
                }else{
                    rightLines.push_back(currentLine);
    
                    leftLines.push_back(currentLine);
                    if(currentLine[0]>rightFinalLine[0]){
                        rightFinalLine[0] = currentLine[0];
                    }
                    if(currentLine[1]>rightFinalLine[1]){
                        rightFinalLine[1] = currentLine[1];
                    }
                    if(currentLine[2]<rightFinalLine[2]){
                        rightFinalLine[2] = currentLine[2];
                    }
                    if(currentLine[3]<rightFinalLine[3]){
                        rightFinalLine[3] = currentLine[3];
                    }
    
                    // rightFinalLine[0]= 5; //x1 = maxim x1     x2 = minim x2      y1  = maxim y1      y2 = minim y2
    
    
    
                 //   printf("KEK %f\n",slope);
             //       line( imageToDrawOn, Point(currentLine[0], currentLine[1]), Point(currentLine[2], currentLine[3]), Scalar(255,255,255), 3, CV_AA);
                }
        }
    
        int x1left = leftFinalLine[0];
        int y1left = leftFinalLine[1];
        int x2left = leftFinalLine[2];
        int y2left = leftFinalLine[3];
    
        int newXLeft = (x2left-x1left)*(image.cols-y1left)/(y2left-y1left)+x1left;
    
        int x1right = rightFinalLine[0];
        int y1right = rightFinalLine[1];
        int x2right = rightFinalLine[2];
        int y2right = rightFinalLine[3];
    
        if(y2right==y1right) y2right-=1;
        if(y2left==y1left) y2left-=1;
        if(x2right==x1right) x2right-=1;
        if(x2left==x1left) x2left-=1;
        
        int newXRight = (x2right-x1right)*(image.cols-y1right)/(y2right-y1right)+x1right;
        
        line( imageToDrawOn, Point(newXLeft, image.cols), Point(leftFinalLine[2], leftFinalLine[3]), Scalar(0,255,255), 9, CV_AA);
        line( imageToDrawOn, Point(newXRight, image.cols), Point(rightFinalLine[2], rightFinalLine[3]), Scalar(255,0,255), 9, CV_AA);
    
    
        Mat result;
        addWeighted( image, 0.5 , imageToDrawOn, 0.5, 0.0, result);
    
    //    imshow("detected lines", result);
//    cv::cvtColor(croppedImage, croppedImage, cv::COLOR_GRAY2BGR);

    return result;
    
}


int main(int argc, const char * argv[]) {
    
    String filename = "/Users/farcaspaul/XCode-workspace/ProjectPEI/ProjectPEI/Images/roadvideo.mp4";
    VideoCapture cap(filename);
    
    // Check if camera opened successfully
    if(!cap.isOpened())
    {
        cout << "Error opening video stream" << endl;
        return -1;
    }
    
    // Default resolution of the frame is obtained.The default resolution is system dependent.
    int frame_width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
    int frame_height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    
    // Define the codec and create VideoWriter object.The output is stored in 'outcpp.avi' file.
    VideoWriter video("outcpp.avi",CV_FOURCC('M','J','P','G'),10, Size(frame_width,frame_height));
    while(1)
    {
        Mat frame;
        
        // Capture frame-by-frame
        cap >> frame;
        
        // If the frame is empty, break immediately
        if (frame.empty())
            break;
        frame = laneDetect(frame);
        // Write the frame into the file 'outcpp.avi'
        video.write(frame);
        
        // Display the resulting frame
        imshow( "Frame", frame );
        
        // Press  ESC on keyboard to  exit
        char c = (char)waitKey(1);
        if( c == 27 )
            break;
    }
    
    // When everything done, release the video capture and write object
    cap.release();
    video.release();

    return 0;
}
