import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Utility {
	private static final String FILENAME = "da.txt";
	public static ArrayList<String> getInfo() {
		
		ArrayList<String> input = new ArrayList<String>();

		BufferedReader br = null;
		FileReader fr = null;
		try {

			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String sCurrentLine;

			br = new BufferedReader(new FileReader(FILENAME));

			while ((sCurrentLine = br.readLine()) != null) {
				input.add(sCurrentLine);
				// System.out.println(sCurrentLine);
			}

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
		return input;
	}
	public static int dateDifference(Date a, Date b){

		
		long difference =  (a.getTime()-b.getTime());
		long seconds = Math.abs(TimeUnit.MILLISECONDS.toSeconds(difference));
//		System.out.println(a + " - " + b);
//		System.out.println("Seconds: " + seconds);
//		System.out.println("Minutes: " + seconds/60);
//		System.out.println("Hours: " + seconds/3600);
		return (int)seconds;
	}
}
