import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Start {
	public static Integer nrBelow5 = 0;
	public static Integer nrTotal = 0;

	@SuppressWarnings("resource")
	public static void main(String argzez[]) throws FileNotFoundException {

		// 2011-11-28 02:27:59 2011-11-28 10:18:11 Sleeping
		// 2011-11-28 02:27:59 2011-11-28 10:18:11 Sleeping

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		File file = new File("C:/Users/Paul/Desktop/file.txt");
		PrintWriter printWriter;
		//
		printWriter = new PrintWriter(file);

		ArrayList<String> input = new ArrayList<String>();
		input = Utility.getInfo();
		List<MonitoredData> data = input.stream().map(s -> {
			Date startTime = null, endTime = null;
			String activity = null;
			try {
				String parts[] = s.split("		");
				startTime = dateFormat.parse(parts[0]);
				endTime = dateFormat.parse(parts[1]);
				activity = parts[2];
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return new MonitoredData(startTime, endTime, activity);
		}).collect(Collectors.toList());

		// getData
		data.forEach(p -> System.out.format("start:%s   end:%s   activity:%s\n", dateFormat.format(p.getStartTime()),
				dateFormat.format(p.getEndTime()), p.getActivity()));
		// getData

		// countDistinctDays
		Calendar calendar = Calendar.getInstance();
		long distinctDays = data.stream().map(s -> {
			calendar.setTime(s.getStartTime());
			return calendar.get(Calendar.DAY_OF_YEAR);
		}).distinct().count();
		System.out.println();
		printWriter.println("Numarul de zile distincte este " + distinctDays + ".");
		System.out.println("Numarul de zile distincte este " + distinctDays + ".");
		// countDistinctDays

		System.out.println();

		// countDistinctActivites
		Map<String, Integer> actionOccurence = data.stream().map(s -> s.getActivity())
				.collect(Collectors.groupingBy(e -> e, Collectors.summingInt(e -> 1)));

		actionOccurence.forEach((k, v) -> {
			printWriter.println("Activity : " + k + "\tCount : " + v);
			System.out.println("Activity : " + k + "\tCount : " + v);
		});
		// countDistinctActivites

		System.out.println();
		// countDistinctActivitiesPerDay
		Map<Integer, Map<String, Integer>> actionOccurencePerDay = data.stream().collect(Collectors.groupingBy(p -> {
			calendar.setTime(p.getStartTime());
			return (calendar.get(Calendar.DAY_OF_YEAR));
		}, Collectors.groupingBy(s -> s.getActivity(), Collectors.summingInt(s -> 1))));

		actionOccurencePerDay.entrySet().stream().forEach(k -> printWriter.println(k.getKey() + " " + k.getValue()));

		Map<String, Integer> actionDuration = data.stream().collect(Collectors.groupingBy(e -> e.getActivity(),
				Collectors.summingInt(e -> Utility.dateDifference(e.getEndTime(), e.getStartTime()))));
		Map<String, Integer> actionDurationAbove10 = actionDuration.entrySet().stream()
				.filter(k -> (k.getValue() / 3600) >= 10).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));

		actionDurationAbove10.entrySet().stream().forEach(k -> {
			System.out.println(k.getKey() + " " + " Hours: " + k.getValue() / (3600) + " Minutes: " + k.getValue() / 60
					+ " Seconds: " + k.getValue());
			printWriter.println(k.getKey() + " " + " Hours: " + k.getValue() / (3600) + " Minutes: " + k.getValue() / 60
					+ " Seconds: " + k.getValue());
		});

		System.out.println("\n\n");

		Map<String, List<Integer>> activities90percent = data.stream()
				.collect(Collectors.groupingBy(e -> e.getActivity(), Collectors.mapping(
						e -> Utility.dateDifference(e.getEndTime(), e.getStartTime()) / 60, Collectors.toList())));

		List<String> ceva = activities90percent.entrySet().stream().filter(s -> {
			nrBelow5 = 0;
			nrTotal = 0;
			s.getValue().stream().forEach(p -> {
				if ((int) p < 5) {
					nrBelow5++;
				}
				nrTotal++;
			});
			double raport = (double) nrBelow5 / nrTotal;
			return raport >= 0.9;
		}).map(lel -> lel.getKey()).collect(Collectors.toList());
		System.out.println();
		System.out.println("Activities with 90% of the monitoring data < 5 minutes:");

		printWriter.println();
		printWriter.println();
		printWriter.println("Activities with 90% of the monitoring data < 5 minutes:");
		ceva.stream().forEach(p -> {
			System.out.println(p);
			printWriter.println(p);
		});

		printWriter.close();
	}

}
