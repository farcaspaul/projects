
public class Monom implements Comparable<Monom> {

	private double coeficient = 0;
	private int putere = 0;

	public Monom(int putere, double coeficient) {
		this.coeficient = coeficient;
		this.putere = putere;
	}

	public double getCoeficient() {
		return coeficient;
	}

	public int getPutere() {
		return putere;
	}

	public int compareTo(Monom arg0) {
		if (this.getPutere() > arg0.getPutere()) {
			return -1;
		} else {
			if (this.getPutere() < arg0.getPutere()) {
				return 1;
			}
		}
		return 0;
	}
}
