import java.util.ArrayList;
import java.util.Iterator;

public class Operatii {

	public Polinom add(Polinom pol1, Polinom pol2) {
		if (pol1.getGrad() >= pol2.getGrad()) {
			return this.adunare(pol1, pol2);
		} else
			return this.adunare(pol2, pol1);
	}

	private Polinom adunare(Polinom pol1, Polinom pol2) {
		byte ok = 0;
		Polinom pol3 = new Polinom(pol1.getGrad() + pol2.getGrad() - 1);

		int i = pol1.getGrad() - 1;
		int j = pol2.getGrad() - 1;
		for (; i >= 0; i--) {
			ok = 0;
			j = pol2.getGrad() - 1;
			for (; j >= 0; j--) {
				if (i == j) {
					pol3.adaugare(new Monom(i,
							pol1.getElementAtIndex(i).getCoeficient() + pol2.getElementAtIndex(i).getCoeficient()));
					ok = 1;
				}
			}
			if (ok != 1) {
				pol3.adaugare(new Monom(i, pol1.getElementAtIndex(i).getCoeficient()));
				ok = 0;
			}
		}

		return pol3;
	}

	public Polinom sub(Polinom pol1, Polinom pol2) {
		Polinom temp = new Polinom(pol2.getGrad());
		int i = pol2.getGrad() - 1;
		for (; i >= 0; i--) {
			temp.adaugare(
					new Monom(pol2.toataLista().get(i).getPutere(), (-1) * pol2.toataLista().get(i).getCoeficient()));
		}
		return this.add(pol1, temp);
	}

	public Polinom inmultire(Polinom pol1, Polinom pol2) {
		Polinom pol3 = new Polinom(pol1.getGrad() * pol2.getGrad() - 1);
		Polinom pol4 = new Polinom(pol1.getGrad() * pol2.getGrad() - 1);

		int i = pol1.getGrad() - 1;
		int j = pol2.getGrad() - 1;
		for (; i >= 0; i--) {
			j = pol2.getGrad() - 1;
			for (; j >= 0; j--) {
				if (pol1.getElementAtIndex(i).getCoeficient() != 0 && pol2.getElementAtIndex(j).getCoeficient() != 0) {
					pol4.adaugare(new Monom(i + j,
							pol1.getElementAtIndex(i).getCoeficient() * pol2.getElementAtIndex(j).getCoeficient()));
				}
			}
			
			pol3 = adunare(pol3, pol4);
			pol3.afisare();
			System.out.println();
			pol4 = new Polinom(pol1.getGrad() * pol2.getGrad() - 1);

		}
		return pol3;
	}

	public Polinom derivare(Polinom pol1) {
		Polinom pol3 = new Polinom(pol1.getGrad());
		int i;
		for (i = pol1.getGrad() - 1; i >= 0; i--) {
			if (pol1.toataLista().get(i).getPutere() > 0) {
				double coeficient = pol1.toataLista().get(i).getCoeficient();
				int putere = pol1.toataLista().get(i).getPutere();
				pol3.adaugare(new Monom(putere - 1, coeficient * putere));
			}
		}
		return pol3;
	}

	public Polinom integrare(Polinom pol1) {
		Polinom pol3 = new Polinom(pol1.getGrad() + 1);
		int i;
		for (i = pol1.getGrad() - 1; i >= 0; i--) {
			double coeficient = pol1.toataLista().get(i).getCoeficient();
			int putere = pol1.toataLista().get(i).getPutere();
			pol3.adaugare(new Monom(putere + 1, coeficient / (putere + 1)));
		}
		return pol3;
	}

	public Polinom impartire(Polinom pol1, Polinom pol2) {
		int dimensiune = pol1.getGrad() + pol2.getGrad() - 1;
		Polinom rezultat = new Polinom(dimensiune);
		Polinom temp = new Polinom(dimensiune);
		Polinom aux = new Polinom(dimensiune);
		while (pol1.getMaxMonom().getPutere() >= pol2.getMaxMonom().getPutere()) {
			rezultat.adaugare(new Monom(pol1.getMaxMonom().getPutere() - pol2.getMaxMonom().getPutere(),
					pol1.getMaxMonom().getCoeficient() / pol2.getMaxMonom().getCoeficient()));
			aux.adaugare(new Monom(pol1.getMaxMonom().getPutere() - pol2.getMaxMonom().getPutere(),
					pol1.getMaxMonom().getCoeficient() / pol2.getMaxMonom().getCoeficient()));
			temp = this.inmultire(aux, pol2);
			aux = new Polinom(dimensiune);
			pol1 = this.sub(pol1, temp);
			temp = new Polinom(dimensiune);
		}
		rezultat = this.adunare(rezultat, pol1); // restul
		return rezultat;
	}

	public ArrayList<Monom> copy(ArrayList<Monom> lista) {
		ArrayList<Monom> lista_copiata = new ArrayList<Monom>();
		Iterator<Monom> i;
		for (i = lista.iterator(); i.hasNext();) {
			lista_copiata.add(i.next());
		}
		return lista_copiata;
	}

	public String formatareString(String polinom) {
		int i = 1;
		int j;
		int ok = 0;

		String modificat = polinom;
		if (modificat.charAt(0) != '+' && modificat.charAt(0) != '-') {
			modificat = "+".concat(modificat);
		}
		modificat = modificat.replace("-", "+-");
		for (i = 1; i < modificat.length(); i++) {
			if (modificat.charAt(i) == 'x') {
				if (modificat.charAt(i - 1) == '+' || modificat.charAt(i - 1) == '-') {
					modificat = modificat.substring(0, i) + "1" + modificat.substring(i, modificat.length());
					break; /// aici face gunoi
				}
			}
		}
		for (i = 1; i < modificat.length(); i++) {
			if (modificat.charAt(i) == '+') {
				if (modificat.charAt(i - 1) == '+' || modificat.charAt(i - 1) == '-') {
					modificat = modificat.substring(0, i) + "1" + modificat.substring(i, modificat.length());
				}
			}
		}

		for (i = 1; i < modificat.length(); i++) {

			if (modificat.charAt(i) == '+') {
				if (i + 2 < modificat.length()) {

					for (j = i + 2; j < modificat.length(); j++) {
						if (modificat.charAt(j) == '+') {
							if (!modificat.substring(i, j).contains("x")) {
								modificat = modificat.substring(0, j) + "x^0"
										+ modificat.substring(j, modificat.length());
							}
						}
					}
				}
			}
		}

		for (i = 1; i < modificat.length(); i++) {
			if (modificat.charAt(i) == '+') {
				if (modificat.charAt(i - 1) == 'x') {
					modificat = modificat.substring(0, i) + "^1" + modificat.substring(i, modificat.length());
				}
			}
		}

		if (modificat.charAt(modificat.length() - 1) == 'x') {
			modificat = modificat.substring(0, modificat.length()) + "^1"
					+ modificat.substring(modificat.length(), modificat.length());
		}

		i = modificat.length() - 1;
		ok = 0;
		for (; i >= 0; i--) {
			if (modificat.charAt(i) == '^') {
				ok = 1;
				break;
			}
			if (modificat.charAt(i) == '+') {
				ok = 1;
				modificat = modificat + "x^0";
				break;
			}
		}
		if (ok == 0) {
			modificat = modificat + "x^0";
		}
		return modificat;

	}

	public Polinom getPolinomFromString(String polinom) {

		polinom = this.formatareString(polinom);

		String[] parts = polinom.split("\\+");
		if (parts.length == 0) {
			return new Polinom(0);
		}
		int i = 0, j = 0;
		Monom mon;
		int max = 0;
		Polinom pol1;
		String[] numar = parts[1].split("x");
		for (i = 1; i < parts.length; i++) {
			numar = parts[i].split("x");
			if (Integer.parseInt(numar[j + 1].substring(1)) > max) {
				max = Integer.parseInt(numar[j + 1].substring(1));
			}
		}
		pol1 = new Polinom(max + 1);
		for (i = 1; i < parts.length; i++) {
			numar = parts[i].split("x");

			mon = new Monom(Integer.parseInt(numar[j + 1].substring(1)), (double) Integer.parseInt(numar[j]));
			pol1.adaugare(mon);
		}
		return pol1;
	}

	public String getStringFromPolinom(Polinom pol) {
		String polinom = "";
		byte ok = 0;
		int i = pol.getGrad() - 1;
		for (; i >= 0; i--) {
			Monom mon = pol.getElementAtIndex(i);
			if (mon.getCoeficient() != 0) {
				if (mon.getCoeficient() > 0 && ok == 0) {
					polinom = polinom + mon.getCoeficient() + "x^" + mon.getPutere();
					ok = 1;
				} else {
					if (mon.getCoeficient() > 0 && ok == 1) {
						polinom = polinom + "+" + mon.getCoeficient() + "x^" + mon.getPutere();
					} else
						polinom = polinom + mon.getCoeficient() + "x^" + mon.getPutere();
					ok = 1;
				}
			}
		}
		return polinom;
	}

	public boolean verificareInput(String inp) {
		String test = "abcdefghijklmnopqrstuvwyz!@#$%&_*][\'/?`~";
		int i;
		if (!inp.contains("x")) {
			return true;
		}
		for (i = 0; i < test.length(); i++) {

			if (inp.contains(test.charAt(i) + "")) {
				return false;
			}
		}
		return true;
	}
}
