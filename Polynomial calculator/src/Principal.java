import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Principal {
	public static void main(String args[]) {
		Operatii operator = new Operatii();
		//
		JFrame frame = new JFrame("Polinoame");
		JPanel panel = new JPanel();

		JLabel eticheta1 = new JLabel("Polinom 1");
		JLabel eticheta2 = new JLabel("Polinom 2");
		JLabel eticheta3 = new JLabel("Rezultat");

		JTextField text1 = new JTextField();
		JTextField text2 = new JTextField();
		JTextField text3 = new JTextField();

		JButton adunare = new JButton("Adunare");
		JButton scadere = new JButton("Scadere");
		JButton inmultire = new JButton("Inmultire");
		JButton impartire = new JButton("Impartire");
		JButton derivare = new JButton("Derivare");
		JButton integrare = new JButton("Integrare");

		panel.setLayout(null);
		eticheta1.setFont(new Font("Serif", Font.PLAIN, 18));
		eticheta2.setFont(new Font("Serif", Font.PLAIN, 18));
		eticheta3.setFont(new Font("Serif", Font.PLAIN, 18));

		eticheta1.setBounds(40, 55, 80, 20);
		text1.setBounds(160, 50, 400, 30);

		eticheta2.setBounds(40, 120, 80, 20);
		text2.setBounds(160, 115, 400, 30);

		eticheta3.setBounds(40, 185, 80, 20);
		text3.setBounds(160, 180, 400, 30);
		text3.setEditable(false);

		adunare.setBounds(40, 240, 110, 50);
		scadere.setBounds(180, 240, 110, 50);
		inmultire.setBounds(40, 310, 110, 50);
		impartire.setBounds(180, 310, 110, 50);
		derivare.setBounds(40, 380, 110, 50);
		integrare.setBounds(180, 380, 110, 50);
		frame.setSize(640, 540);
		frame.setResizable(false);
		panel.setSize(300, 400);

		panel.add(eticheta1);
		panel.add(text1);
		panel.add(eticheta2);
		panel.add(text2);
		panel.add(eticheta3);
		panel.add(text3);

		adunare.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Polinom pol1, pol2;
				if (!text1.getText().contains(".") && !text2.getText().contains(".")
						&& operator.verificareInput(text1.getText()) && operator.verificareInput(text2.getText())) {
					pol1 = operator.getPolinomFromString(text1.getText());
					pol2 = operator.getPolinomFromString(text2.getText());
					// if (operator.getStringFromPolinom(operator.add(pol1,
					// pol2)).length() > 2) {
					text3.setText(operator.getStringFromPolinom(operator.add(pol1, pol2)));
					// } else {
					// text3.setText("0");
					// }
				}
			}
		});

		scadere.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Polinom pol1, pol2;
				if (!text1.getText().contains(".") && !text2.getText().contains(".")
						&& operator.verificareInput(text1.getText()) && operator.verificareInput(text2.getText())) {
					pol1 = operator.getPolinomFromString(text1.getText());
					pol2 = operator.getPolinomFromString(text2.getText());
					if (text1.getText().equals(text2.getText())) {
						text3.setText("0");
					} else {
						text3.setText(operator.getStringFromPolinom(operator.sub(pol1, pol2)));

					}
				}
			}
		});

		inmultire.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Polinom pol1, pol2;
				if (!text1.getText().contains(".") && !text2.getText().contains(".")
						&& operator.verificareInput(text1.getText()) && operator.verificareInput(text2.getText())) {
					pol1 = operator.getPolinomFromString(text1.getText());
					pol2 = operator.getPolinomFromString(text2.getText());
					if (pol1.getMaxMonom().getCoeficient() != 0 && pol2.getMaxMonom().getCoeficient() != 0) {
						text3.setText(operator.getStringFromPolinom(operator.inmultire(pol1, pol2)));
						// text3.setText("0");
					}
				}
			}
		});

		impartire.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Polinom pol1, pol2;
				if (!text1.getText().contains(".") && !text2.getText().contains(".")
						&& operator.verificareInput(text1.getText()) && operator.verificareInput(text2.getText())) {
					pol1 = operator.getPolinomFromString(text1.getText());
					pol2 = operator.getPolinomFromString(text2.getText());
					// if (pol1.getMaxMonom().getCoeficient() != 0 &&
					// pol2.getMaxMonom().getCoeficient() != 0) {
					text3.setText(operator.getStringFromPolinom(operator.impartire(pol1, pol2)));
					// }
				}
			}

		});

		derivare.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Polinom pol1, pol2;
				if (text1.getText() != null && !text1.getText().contains(".")
						&& operator.verificareInput(text1.getText())) {
					pol1 = operator.getPolinomFromString(text1.getText());
					if (pol1.getMaxMonom().getCoeficient() != 0) {
						text1.setText(operator.getStringFromPolinom(operator.derivare(pol1)));
					}
					text3.setText(":^)");
				}
				if (text2.getText() != null && !text2.getText().contains(".")
						&& operator.verificareInput(text2.getText())) {
					pol2 = operator.getPolinomFromString(text2.getText());
					if (pol2.getMaxMonom().getCoeficient() != 0) {
						text2.setText(operator.getStringFromPolinom(operator.derivare(pol2)));
					}
					text3.setText(":^)");
				}
			}
		});

		integrare.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Polinom pol1, pol2;
				if (text1.getText() != null && !text1.getText().contains(".")
						&& operator.verificareInput(text1.getText())) {
					pol1 = operator.getPolinomFromString(text1.getText());
					if (pol1.getMaxMonom().getCoeficient() != 0) {
						text1.setText(operator.getStringFromPolinom(operator.integrare(pol1)));
					}
					text3.setText(":^)");
				}
				if (text2.getText() != null && !text2.getText().contains(".")
						&& operator.verificareInput(text2.getText())) {
					pol2 = operator.getPolinomFromString(text2.getText());
					if (pol2.getMaxMonom().getCoeficient() != 0) {
						text2.setText(operator.getStringFromPolinom(operator.integrare(pol2)));
					}
					text3.setText(":^)");
				}
			}
		});

		panel.add(adunare);
		panel.add(scadere);
		panel.add(inmultire);
		panel.add(impartire);
		panel.add(derivare);
		panel.add(integrare);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);
		frame.setVisible(true);
		frame.setResizable(true);
		// Polinom pol1 = operator.getPolinomFromString("-2x^3");
		// Polinom pol2 = operator.getPolinomFromString("2x^3+1x^2");
		// System.out.println(operator.getPolinomFromString("1"));
		// pol1.afisare();
		// System.out.println();
		// pol2.afisare();
		// System.out.println();
		// operator.inmultire(pol1, pol2).afisare();
	}
}
