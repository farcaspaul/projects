import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Polinom {
	private List<Monom> lista;
	private int grad;

	public Polinom(int gradul) {
		lista = new ArrayList<Monom>(gradul);
		int i;
		for (i = 0; i < gradul; i++) {
			lista.add(new Monom(i, 0));
		}
		this.grad = gradul;
	}
	public Monom getElementAtIndex(int index) {
		return lista.get(index);
	}

	public int getGrad() {
		return grad;
	}

	public ArrayList<Monom> toataLista() {
		return (ArrayList<Monom>) lista;
	}
	
	public void adaugare(Monom monom) {
		lista.set((int) monom.getPutere(), monom);
	}
	
	public Monom getMaxMonom()
	{
		int i=this.getGrad()-1;
		Monom max=new Monom(0,0);
		for(;i>=0;i--)
		{
			if(this.toataLista().get(i).getCoeficient()!=0)
			{
				if(this.toataLista().get(i).getPutere()>max.getPutere()){
				max=this.toataLista().get(i);
				}
			}
		}
		return max;
	}

	public void afisare() {
		Iterator<Monom> i;
		byte ok = 0;
		Operatii copiator = new Operatii();
		ArrayList<Monom> list = new ArrayList<Monom>();
		list = copiator.copy(this.toataLista());
		Collections.sort(list);
		for (i = list.iterator(); i.hasNext();) {
			Monom mon = i.next();
			if (mon.getCoeficient() != 0) {
				if (mon.getCoeficient() > 0 && ok == 0) {
					System.out.print(mon.getCoeficient() + "x^" + mon.getPutere());
					ok = 1;
				} else {
					if (mon.getCoeficient() > 0 && ok == 1) {
						System.out.print("+" + mon.getCoeficient() + "x^" + mon.getPutere());
					} else
						System.out.print(mon.getCoeficient() + "x^" + mon.getPutere());
					ok = 1;
				}
			}
		}
	}
}
