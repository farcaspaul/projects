import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Bank extends Observable implements BankProc,Serializable {

	private HashMap<Integer, ArrayList<Account>> accounts;
	private ArrayList<Person> persons;

	public Bank() {
		accounts = new HashMap<Integer, ArrayList<Account>>(31);
		persons = new ArrayList<Person>(31);
	}

	public ArrayList<Person> getPersons() {
		return persons;
	}

	public ArrayList<Account> getAccounts(Person p) {
		assert p != null : "Invalid person";
		ArrayList<Account> temp = accounts.get(p.hashCode());
		
		if(temp==null){
			JOptionPane.showMessageDialog(new JFrame(), "No accounts");
			assert temp!=null : "No accounts";
		}
		return temp;
	}

	public void addPerson(Person p) {
		assert p != null : "Invalid person";
		if(p.getVarsta() < 18){
			JOptionPane.showMessageDialog(new JFrame(), "Underage person");
			assert p.getVarsta() >= 18 : "Underage.";
		}
		Iterator<Person> it;
		boolean ok = false;
		for (it = persons.iterator(); it.hasNext();) {
			if (p.equals(it.next())) {
				ok = true;
				break;
			}
		}
		if(ok==true){
			JOptionPane.showMessageDialog(new JFrame(), "Person already exists");
			assert ok == false : "Person already exists";
		}
		
		persons.add(p);

	}

	public void removePerson(Person p) {
		assert p != null : "Invalid person";
		accounts.put(p.hashCode(), null);
		Iterator<Person> it;
		for(it = persons.iterator();it.hasNext();){
			Person temp = it.next();
			if(temp.equals(p)){
				it.remove();
			}
		}

	}

	public void addAccount(Person p, Account a) {
		assert p != null : "Invalid person";
		assert a != null : "Invalid account";
		ArrayList<Account> temp = accounts.get(p.hashCode());
		if (temp == null) {
			temp = new ArrayList<Account>();
		}
		
		Iterator<Person> ii;
		for(ii = persons.iterator();ii.hasNext();){
			Person dum = ii.next();
			if(dum.equals(p)){
				String type = "";
				if(a.getClass() == new SavingsAccount().getClass()){
					type = "Savings account";
				}else{
					type = "Spendings account";
				}
				dum.update(this, type + " a fost creat id = " + a.getId()+ " Balance = " + a.getBalance());
			}
		}
		
		Iterator<Account> it;
		boolean ok = false;
		for (it = temp.iterator(); it.hasNext();) {
			if (a.equals(it.next())) {
				ok = true;
				break;
			}
		}
		if(ok==true){
			JOptionPane.showMessageDialog(new JFrame(), "Account already exists");
			assert ok == false : "Account already exists";
		}
		temp.add(a);
		accounts.put(p.hashCode(), temp);
	}

	public void removeAccount(Person p, Account a) {
		assert p != null : "Invalid person";
		assert a != null : "Invalid account";
		ArrayList<Account> temp = accounts.get(p.hashCode());
		if (temp == null) {
			temp = new ArrayList<Account>();
		}
		Iterator<Person> ii;
		for(ii = persons.iterator();ii.hasNext();){
			Person dum = ii.next();
			if(dum.equals(p)){
				String type = "";
				if(a.getClass() == new SavingsAccount().getClass()){
					type = "Savings account";
				}else{
					type = "Spendings account";
				}
				dum.update(this, type + " a fost sters id = " + a.getId()+ " Balance = " + a.getBalance());
			}
		}
		Iterator<Account> it;
		for(it = temp.iterator();it.hasNext();){
			if(it.next().equals(a)){
				it.remove();
			}
		}
//		temp.remove(a);
		accounts.put(p.hashCode(), temp);
	}

	public Account readAccount(Person p, int id) {
		assert p != null : "Invalid person";
		ArrayList<Account> temp = accounts.get(p.hashCode());
		Iterator<Account> it;
		Account returned = null;
		if (temp != null) {
			for (it = temp.iterator(); it.hasNext();) {
				Account ac = it.next();
				if (ac.getId() == id) {
					returned = ac;
					break;
				}
			}
		}
		assert returned != null : "Account not found";
		return returned;
	}

	public void writeAccount(Person p, int id, Account a) {
		assert p != null : "Invalid person";
		assert a != null : "Invalid account";
		Iterator<Account> it;
		ArrayList<Account> temp = accounts.get(p.hashCode());
		Account returned = null;
		for (it = temp.iterator(); it.hasNext();) {
			Account ac = it.next();
			if (ac.getId() == id) {
				returned = ac;
				break;
			}
		}
		Iterator<Person> ii;
		for(ii = persons.iterator();ii.hasNext();){
			Person dum = ii.next();
			if(dum.equals(p)){
				String type = "";
				if(a.getClass() == new SavingsAccount().getClass()){
					type = "Savings account";
				}else{
					type = "Spendings account";
				}
				dum.update(this, type + " a fost editat id = " + a.getId()+ " Balance = " + a.getBalance());
			}
		}
		assert returned != null : "Account not found";
		returned.setBalance(a.getBalance());
		returned.setDescription(a.getDescription());
		

	}

	public void editPerson(Person p1, Person p2) {
		assert p1 != null : "Invalid person";
		this.addPerson(p2);
		ArrayList<Account> temp = accounts.get(p1.hashCode());
		accounts.put(p2.hashCode(), temp);
		accounts.remove(p1.hashCode());
		removePerson(p1);

	}

	public void checkIfEmpty(){
		assert persons.size()>0 : "The bank contains no persons";
	}
}
