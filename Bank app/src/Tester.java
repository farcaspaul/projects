
public class Tester {
	public void go(Bank bank){
		Person p = new Person("Test1","mail1",18);
		Person p1 = new Person("Test2","mail2",19);
		Person p2 = new Person("Test3","mail3",20);
		bank.addPerson(p);
		bank.addPerson(p1);
		bank.addPerson(p2);
		SpendingsAccount a = new SpendingsAccount(1,10,"Spendings");
		a.setBalance(100);
		a.substractAmount(100);
		SavingsAccount a1 = new SavingsAccount(2,0,"Savings");
		a1.addAmount(1501);
		a1.substractAmount(1501);
		bank.addAccount(p, a);
		bank.addAccount(p, a1);
		bank.removeAccount(p, a);
		bank.removeAccount(p, a1);
		bank.removePerson(p);
		bank.removePerson(p2);
		bank.removePerson(p1);
	}
}
