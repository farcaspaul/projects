

public interface BankProc {
void addPerson(Person p);
void removePerson(Person p);
void editPerson(Person p1, Person p2);
void addAccount(Person p, Account a);
void removeAccount(Person p,Account a);
Account readAccount(Person p, int id);
void writeAccount(Person p,int id, Account a);
}
