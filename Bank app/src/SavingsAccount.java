import java.io.Serializable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SavingsAccount extends Account implements Serializable{

	public SavingsAccount(int i, int j, String string) {
		super(i,j,string);
	}
	public SavingsAccount() {
		
	}
	public void addAmount(int amount){
		if(amount<1500){
			JOptionPane.showMessageDialog(new JFrame(), "Insufficient amount");
			assert amount>1500 : "Insufficient ammount.";
		}
		this.setBalance(this.getBalance()+amount+1*amount/500);
	}
	public void substractAmount(int amount){
		if(amount<1500){
			JOptionPane.showMessageDialog(new JFrame(), "Insufficient amount");
			assert amount>1500 : "Insufficient ammount.";
		}
		this.setBalance(this.getBalance()-amount-1*amount/100);
	}
	public void le_update_add(Person temp, int val) {
		temp.update(this,"S-au depus " + val + " in contul cu id = " + this.getId());
		
	}
	public void le_update_sub(Person temp, int val) {
		temp.update(this,"S-au extras " + val + " in contul cu id = " + this.getId());
		
	}
}
