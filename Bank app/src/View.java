import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class View extends Observable{
	JFrame frame_main;
	JPanel panel_main, panel_accounts, panel_persons;
	JFrame frame_persons;
	JFrame frame_accounts;
	JButton persons_main, accounts_main, persons_add, persons_edit, persons_delete, persons_refresh, accounts_refresh,
			accounts_add_spending, accounts_add_saving, accounts_edit, accounts_delete, accounts_substract_amount,
			accounts_add_amount,save;
	JScrollPane scroll_persons, scroll_accounts, scroll_persons_text, scroll_accounts_text;
	JTable table_persons, table_accounts, table_persons_text, table_accounts_text;
	Bank bank;
	public String[] columnNames = { "Nume", "Email", "Varsta" };
	public String[] columnNamesAcc = { "Id", "Balance", "Description" };

	public View(Bank bank) {
		this.bank = bank;
		panel_main = new JPanel();
		panel_main.setLayout(null);
		persons_main = new JButton("Operatii persoane");
		accounts_main = new JButton("Operatii conturi");
		save = new JButton("Save");
		save.setBounds(140,70,100,30);
		persons_main.setBounds(30, 20, 150, 30);
		accounts_main.setBounds(200, 20, 150, 30);
		panel_main.add(persons_main);
		panel_main.add(accounts_main);
		panel_main.add(save);
		
		save.addActionListener(new ActionListener(){

			
			public void actionPerformed(ActionEvent e) {
				try {
			         FileOutputStream fileOut = new FileOutputStream("bank.ser");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeObject(bank);
			         out.close();
			         fileOut.close();
			         
			      }catch(IOException i) {
			         i.printStackTrace();
			      }
				
			}
			
		});
		
		persons_main.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				frame_persons = new JFrame("Persons");
				frame_persons.setSize(400, 400);
				panel_persons = new JPanel();
				panel_persons.setLayout(null);
				persons_add = new JButton("Add");
				persons_edit = new JButton("Edit");
				persons_delete = new JButton("Del");
				persons_refresh = new JButton("Refr");
				persons_add.setBounds(320, 320, 60, 30);
				persons_edit.setBounds(320, 280, 60, 30);
				persons_delete.setBounds(320, 240, 60, 30);
				persons_refresh.setBounds(320, 200, 60, 30);
				ArrayList<Person> list = bank.getPersons();
				Object[][] dataAfisat = new String[list.size()][3];
				Iterator<Person> it;
				int cnt = 0;
				for (it = list.iterator(); it.hasNext();) {
					Person temp = it.next();
					dataAfisat[cnt][0] = temp.getName();
					dataAfisat[cnt][1] = temp.getEmail();
					dataAfisat[cnt][2] = temp.getVarsta() + "";
					cnt++;
				}
				table_persons = new JTable(dataAfisat, columnNames);
				table_persons.setPreferredScrollableViewportSize(new Dimension(100, 100));
				table_persons.setFillsViewportHeight(true);
				scroll_persons = new JScrollPane(table_persons);
				scroll_persons.setBounds(10, 10, 290, 300);

				table_persons_text = new JTable(new String[1][3], columnNames);
				table_persons_text.setPreferredScrollableViewportSize(new Dimension(100, 100));
				table_persons_text.setFillsViewportHeight(true);
				scroll_persons_text = new JScrollPane(table_persons_text);
				scroll_persons_text.setBounds(10, 320, 290, 38);

				persons_refresh.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						ArrayList<Person> list = bank.getPersons();
						Object[][] dataAfisat = new String[list.size()][3];
						Iterator<Person> it;
						int cnt = 0;
						for (it = list.iterator(); it.hasNext();) {
							Person temp = it.next();
							dataAfisat[cnt][0] = temp.getName();
							dataAfisat[cnt][1] = temp.getEmail();
							dataAfisat[cnt][2] = temp.getVarsta() + "";
							cnt++;
						}
						table_persons = new JTable(dataAfisat, columnNames);
						panel_persons.remove(scroll_persons);

						scroll_persons = new JScrollPane(table_persons);
						panel_persons.add(scroll_persons);
						scroll_persons.setBounds(10, 10, 290, 300);
						frame_persons.revalidate();
					}

				});

				persons_edit.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						Person oldP = new Person();
						Person newP = new Person();

						table_persons_text.getCellEditor().stopCellEditing();
						newP.setName((String) table_persons_text.getValueAt(0, 0));
						newP.setEmail((String) table_persons_text.getValueAt(0, 1));
						newP.setAge(Integer.parseInt((String) table_persons_text.getValueAt(0, 2)));

						oldP.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						oldP.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						oldP.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));
						bank.editPerson(oldP, newP);
//						Iterator<Person> it;
//						ArrayList<Person> list = bank.getPersons();

//						for (it = list.iterator(); it.hasNext();) {
//							Person temp = it.next();
//							System.out.println(temp.getName() + " " + temp.getVarsta() + " " + temp.getEmail());
//						}
						table_persons_text.setValueAt("", 0, 0);
						table_persons_text.setValueAt("", 0, 1);
						table_persons_text.setValueAt("", 0, 2);
					}

				});

				persons_add.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						Person temp = new Person();
						table_persons_text.getCellEditor().stopCellEditing();
						temp.setName((String) table_persons_text.getValueAt(0, 0));
						temp.setEmail((String) table_persons_text.getValueAt(0, 1));
						temp.setAge(Integer.parseInt((String) table_persons_text.getValueAt(0, 2)));
						bank.addPerson(temp);

						table_persons_text.setValueAt("", 0, 0);
						table_persons_text.setValueAt("", 0, 1);
						table_persons_text.setValueAt("", 0, 2);
					}

				});

				persons_delete.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						Person oldP = new Person();

						oldP.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						oldP.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						oldP.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));
						bank.removePerson(oldP);

						table_persons_text.setValueAt("", 0, 0);
						table_persons_text.setValueAt("", 0, 1);
						table_persons_text.setValueAt("", 0, 2);
					}

				});
				panel_persons.add(persons_add);
				panel_persons.add(persons_edit);
				panel_persons.add(persons_delete);
				panel_persons.add(persons_refresh);
				panel_persons.add(scroll_persons);
				panel_persons.add(scroll_persons_text);
				frame_persons.add(panel_persons);
				frame_persons.setResizable(false);
				frame_persons.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

				frame_persons.setVisible(true);
			}

		});

		accounts_main.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				ArrayList<Person> list = bank.getPersons();
				Object[][] dataAfisat = new String[list.size()][3];
				Iterator<Person> it;
				int cnt = 0;
				for (it = list.iterator(); it.hasNext();) {
					Person temp = it.next();
					dataAfisat[cnt][0] = temp.getName();
					dataAfisat[cnt][1] = temp.getEmail();
					dataAfisat[cnt][2] = temp.getVarsta() + "";
					cnt++;
				}
				table_persons = new JTable(dataAfisat, columnNames);
				table_persons.setPreferredScrollableViewportSize(new Dimension(100, 100));
				table_persons.setFillsViewportHeight(true);
				scroll_persons = new JScrollPane(table_persons);
				scroll_persons.setBounds(10, 360, 290, 200);

				table_accounts_text = new JTable(new String[1][3], columnNamesAcc);
				table_accounts_text.setPreferredScrollableViewportSize(new Dimension(100, 100));
				table_accounts_text.setFillsViewportHeight(true);
				scroll_accounts_text = new JScrollPane(table_accounts_text);
				scroll_accounts_text.setBounds(10, 313, 290, 38);

				frame_accounts = new JFrame("Accounts");
				frame_accounts.setSize(400, 600);
				panel_accounts = new JPanel();
				panel_accounts.setLayout(null);
				accounts_add_spending = new JButton("Addsp");
				accounts_add_saving = new JButton("Addsa");
				accounts_edit = new JButton("Edit");
				accounts_delete = new JButton("Del");
				accounts_refresh = new JButton("Refr");
				accounts_add_amount = new JButton("+");
				accounts_substract_amount = new JButton("-");
				accounts_add_amount.setBounds(310, 80, 60, 30);
				accounts_substract_amount.setBounds(310, 120, 60, 30);
				accounts_add_saving.setBounds(310, 280, 70, 30);
				accounts_add_spending.setBounds(310, 320, 70, 30);
				accounts_edit.setBounds(310, 240, 60, 30);
				accounts_delete.setBounds(310, 200, 60, 30);
				accounts_refresh.setBounds(310, 160, 60, 30);
				table_accounts = new JTable(new String[0][3], columnNamesAcc);
				scroll_accounts = new JScrollPane(table_accounts);

				accounts_refresh.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Person temp = new Person();
						temp.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						temp.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						temp.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));

						ArrayList<Account> list = bank.getAccounts(temp);
						Object[][] dataAfisat = new String[list.size()][3];
						Iterator<Account> it;
						int cnt = 0;
						for (it = list.iterator(); it.hasNext();) {
							Account se = it.next();
							dataAfisat[cnt][0] = se.getId() + "";
							dataAfisat[cnt][1] = se.getBalance() + "";
							dataAfisat[cnt][2] = se.getDescription();
							cnt++;
						}
						table_accounts = new JTable(dataAfisat, columnNamesAcc);
						panel_accounts.remove(scroll_accounts);
						frame_accounts.revalidate();
						scroll_accounts = new JScrollPane(table_accounts);
						panel_accounts.add(scroll_accounts);
						scroll_accounts.setBounds(10, 10, 290, 300);
						frame_accounts.revalidate();
					}

				});

				accounts_add_saving.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						Person p = new Person();
						p.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						p.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						p.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));
						Account temp = new SavingsAccount();
						table_accounts_text.getCellEditor().stopCellEditing();
						temp.setId(Integer.parseInt((String) table_accounts_text.getValueAt(0, 0)));
						temp.setBalance(Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
						temp.setDescription((String) table_accounts_text.getValueAt(0, 2));
						table_accounts_text.setValueAt("", 0, 0);
						table_accounts_text.setValueAt("", 0, 1);
						table_accounts_text.setValueAt("", 0, 2);
						bank.addAccount(p, temp);
					}

				});

				accounts_add_spending.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						Person p = new Person();
						p.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						p.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						p.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));
						Account temp = new SpendingsAccount();
						table_accounts_text.getCellEditor().stopCellEditing();
						temp.setId(Integer.parseInt((String) table_accounts_text.getValueAt(0, 0)));
						temp.setBalance(Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
						temp.setDescription((String) table_accounts_text.getValueAt(0, 2));
						table_accounts_text.setValueAt("", 0, 0);
						table_accounts_text.setValueAt("", 0, 1);
						table_accounts_text.setValueAt("", 0, 2);
						bank.addAccount(p, temp);
					}

				});

				accounts_edit.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						Person p = new Person();
						p.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						p.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						p.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));

						Account newA = new Account();
						table_accounts_text.getCellEditor().stopCellEditing();
						newA.setId(Integer.parseInt((String) table_accounts_text.getValueAt(0, 0)));
						newA.setBalance(Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
						newA.setDescription((String) table_accounts_text.getValueAt(0, 2));

						int id = Integer
								.parseInt((String) table_accounts.getValueAt(table_accounts.getSelectedRow(), 0));
						bank.writeAccount(p, id, newA);

						table_accounts_text.setValueAt("", 0, 0);
						table_accounts_text.setValueAt("", 0, 1);
						table_accounts_text.setValueAt("", 0, 2);
					}

				});

				accounts_delete.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						Person temp = new Person();
						temp.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						temp.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						temp.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));
						Account a = new Account();

						// table_accounts.getCellEditor().stopCellEditing();
						a.setId(Integer
								.parseInt((String) table_accounts.getValueAt(table_accounts.getSelectedRow(), 0)));
						a.setBalance(Integer
								.parseInt((String) table_accounts.getValueAt(table_accounts.getSelectedRow(), 1)));
						a.setDescription((String) table_accounts.getValueAt(table_accounts.getSelectedRow(), 2));
						bank.removeAccount(temp, a);
					}

				});

				accounts_add_amount.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						Person temp = new Person();
						temp.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						temp.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						temp.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));

						ArrayList<Account> list = bank.getAccounts(temp);
						int id = Integer
								.parseInt((String) table_accounts.getValueAt(table_accounts.getSelectedRow(), 0));
						Iterator<Account> it;
						SavingsAccount dummy1 = new SavingsAccount();
						SpendingsAccount dummy2 = new SpendingsAccount();
						int counter = 0;
						if(table_accounts_text.getCellEditor()!=null){
						table_accounts_text.getCellEditor().stopCellEditing();
						}
						for (it = list.iterator(); it.hasNext();) {

							Account el = it.next();
							if (el.getId() == id) {
								if (el.getClass() == dummy1.getClass()) {
//									System.out.println(counter);
									dummy1.setId(el.getId());
									dummy1.setBalance(el.getBalance());
									dummy1.setDescription(el.getDescription());
									dummy1.addAmount(Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									dummy1.le_update_add(temp,Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									list.set(counter, dummy1);
								} else {
//									System.out.println(counter);
									dummy2.setId(el.getId());
									dummy2.setBalance(el.getBalance());
									dummy2.setDescription(el.getDescription());
									dummy2.addAmount(Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									dummy2.le_update_add(temp,Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									list.set(counter, dummy2);
								}
							}
							counter++;
						}
						table_accounts_text.setValueAt("", 0, 0);
						table_accounts_text.setValueAt("", 0, 1);
						table_accounts_text.setValueAt("", 0, 2);
					}

				});

				accounts_substract_amount.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						Person temp = new Person();
						temp.setName((String) table_persons.getValueAt(table_persons.getSelectedRow(), 0));
						temp.setEmail((String) table_persons.getValueAt(table_persons.getSelectedRow(), 1));
						temp.setAge(
								Integer.parseInt((String) table_persons.getValueAt(table_persons.getSelectedRow(), 2)));

						ArrayList<Account> list = bank.getAccounts(temp);
						int id = Integer
								.parseInt((String) table_accounts.getValueAt(table_accounts.getSelectedRow(), 0));
						Iterator<Account> it;
						SavingsAccount dummy1 = new SavingsAccount();
						SpendingsAccount dummy2 = new SpendingsAccount();
						int counter = 0;
						table_accounts_text.getCellEditor().stopCellEditing();
						for (it = list.iterator(); it.hasNext();) {

							Account el = it.next();
							if (el.getId() == id) {
								if (el.getClass() == dummy1.getClass()) {
//									System.out.println(counter);
									dummy1.setId(el.getId());
									dummy1.setBalance(el.getBalance());
									dummy1.setDescription(el.getDescription());
									dummy1.substractAmount(
											Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									dummy1.le_update_sub(temp,Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									
									list.set(counter, dummy1);
								} else {
//									System.out.println(counter);
									dummy2.setId(el.getId());
									dummy2.setBalance(el.getBalance());
									dummy2.setDescription(el.getDescription());
									dummy2.substractAmount(
											Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									dummy2.le_update_sub(temp,Integer.parseInt((String) table_accounts_text.getValueAt(0, 1)));
									list.set(counter, dummy2);
								}
							}
							counter++;
						}
						table_accounts_text.setValueAt("", 0, 0);
						table_accounts_text.setValueAt("", 0, 1);
						table_accounts_text.setValueAt("", 0, 2);

					}

				});

				panel_accounts.add(accounts_add_spending);
				panel_accounts.add(accounts_add_saving);
				panel_accounts.add(accounts_edit);
				panel_accounts.add(accounts_delete);
				panel_accounts.add(accounts_refresh);
				panel_accounts.add(scroll_accounts_text);
				panel_accounts.add(scroll_persons);
				panel_accounts.add(accounts_substract_amount);
				panel_accounts.add(accounts_add_amount);
				frame_accounts.add(panel_accounts);
				frame_accounts.setResizable(false);
				frame_accounts.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

				frame_accounts.setVisible(true);
			}

		});

		frame_main = new JFrame("Bank");
		frame_main.setResizable(false);
		frame_main.add(panel_main);
		frame_main.setSize(400, 200);
		frame_main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_main.setLocationRelativeTo(null);
		frame_main.setVisible(true);
	}

}
