//import java.io.BufferedWriter;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.Observable;
//import java.util.Observer;

public class Person implements Serializable{

	private String name;
	private String email;
	private int varsta;

	public Person() {
	}

	public Person(String name, String email, int varsta) {
		this.name = name;
		this.email = email;
		this.varsta = varsta;
	}

	public String getName() {
		return this.name;
	}

	public String getEmail() {
		return this.email;
	}

	public int getVarsta() {
		return this.varsta;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAge(int age) {
		this.varsta = age;
	}

	public int hashCode() {
		int sum = 0;
		for (int i = 0; i < name.length(); i++) {
			sum += Character.getNumericValue(name.charAt(i));
		}
		return sum % 31;
	}

	public boolean equals(Person p) {
		if (this.name.equals(p.getName()) && this.email.equals(p.getEmail()) && this.varsta == p.getVarsta()) {
			return true;
		}
		return false;
	}

	public void update(Observable arg0, Object arg1) {
		try {
			FileWriter fw;
			fw = new FileWriter("log.txt" , true);
			BufferedWriter buffer = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(buffer);
			pw.println(arg1);
			pw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
