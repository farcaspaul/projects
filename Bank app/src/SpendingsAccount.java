import java.io.Serializable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SpendingsAccount extends Account implements Serializable {

	public SpendingsAccount(int i, int j, String string) {
		super(i, j, string);
	}

	public SpendingsAccount() {
	}

	public void addAmount(int amount) {
		if (amount >= 1500) {
			JOptionPane.showMessageDialog(new JFrame(), "Exceeding amount");
			assert amount <= 1500 : "Exceeding ammount.";
		}
		this.setBalance(this.getBalance() + amount);
	}

	public void substractAmount(int amount) {
		if (amount >= 1500) {
			JOptionPane.showMessageDialog(new JFrame(), "Exceeding amount");
			assert amount <= 1500 : "Exceeding ammount.";
		}
		this.setBalance(this.getBalance() - amount);
	}

	public void le_update_add(Person temp, int val) {
		temp.update(this, "S-au depus " + val + " in contul cu id = " + this.getId());

	}

	public void le_update_sub(Person temp, int val) {
		temp.update(this, "S-au extras " + val + " in contul cu id = " + this.getId());

	}
}
