import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable {

	private int balance;
	private int id;
	private String description;

	public Account() {
	};

	public Account(int id, int balance, String description) {
		this.balance = balance;
		this.description = description;
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getBalance() {
		return this.balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean equals(Account p) {
		if (this.id == p.getId() && this.balance == p.getBalance() && this.description.equals(p.getDescription())) {
			return true;
		}
		return false;
	}

	public void addAmount() {

	}

	public void substractAmount() {

	}
	
}
