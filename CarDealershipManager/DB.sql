CREATE DATABASE if not exists `dealer`  /*!40100 DEFAULT CHARACTER SET utf8 */ ;
use `dealer`;

drop table if exists  car;
CREATE TABLE `dealer`.`car` (
  `idcar` INT NOT NULL AUTO_INCREMENT,
  `carbrand` VARCHAR(45) NOT NULL,
  `carmodel` VARCHAR(45) NOT NULL,
  `cubiccapacity` INT NOT NULL,
  `fuel` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idcar`),
  UNIQUE INDEX `idcar_UNIQUE` (`idcar` ASC));


CREATE TABLE if not exists`client` (
  `idclient` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `cnp` varchar(13) NOT NULL,
  `password` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`idclient`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


CREATE TABLE if not exists`employee` (
  `idemployee` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`idemployee`),
  UNIQUE KEY `idemployee_UNIQUE` (`idemployee`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE if not exists `dealer`.`contract` (
  `idcontract` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `idcar` INT NOT NULL,
  PRIMARY KEY (`idcontract`),
  UNIQUE INDEX `idcontract_UNIQUE` (`idcontract` ASC),
  INDEX `idcar_idx` (`idcar` ASC),
  CONSTRAINT `idcar`
    FOREIGN KEY (`idcar`)
    REFERENCES `dealer`.`car` (`idcar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
 
 ALTER TABLE `dealer`.`contract` 
ADD COLUMN `idclient` INT NOT NULL AFTER `idcar`,
ADD INDEX `idclient_idx` (`idclient` ASC);
ALTER TABLE `dealer`.`contract` 
ADD CONSTRAINT `idclient`
  FOREIGN KEY (`idclient`)
  REFERENCES `dealer`.`client` (`idclient`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

#CREATE TABLE if not exists`contract` (
#  `idcontract` int(11) NOT NULL AUTO_INCREMENT,
#  `date` datetime NOT NULL,
#  `idemployee` int(11) NOT NULL,
#  PRIMARY KEY (`idcontract`),
#  KEY `idemployee_idx` (`idemployee`),
#  CONSTRAINT `idemployee` FOREIGN KEY (`idemployee`) REFERENCES `employee` (`idemployee`) ON DELETE NO ACTION ON UPDATE NO ACTION
#) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

