package BusinessLayer;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Utilities {
	public static String[] fuelTypeList = {"diesel","petrol","gas","benzina","GPL","gpl","electric","hybrid"};
	public static String transformString(String input) {
		return input.replaceAll("newLine", "\n");
	}
	public static boolean isAlpha(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetter(c) && !(c == '_'))
				return false;
		}

		return true;
	}
	
	public static boolean checkDate(String date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(date.trim());
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	}
	
	public static boolean isAlphaNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) && !(c == '_'))
				return false;
		}

		return true;
	}

	public static boolean isNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !(c == '.'))
				return false;
		}

		return true;
	}
}
