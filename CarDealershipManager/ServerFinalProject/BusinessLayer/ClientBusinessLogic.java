package BusinessLayer;

import java.util.ArrayList;

import DatabaseLayer.Car;
import DatabaseLayer.CarQueries;
import DatabaseLayer.ClientQueries;

public class ClientBusinessLogic {
	private CarQueries carQueries = new CarQueries();
	private ClientQueries clientQueries = new ClientQueries();
	public ArrayList<Car> getCar(String idcar) {
		return carQueries.getCar(idcar);
	}

	public ArrayList<Car> getAllCars() {
		return carQueries.getAllCars();
	}

	public ArrayList<Car> getCarsByBrand(String brand) {
		return carQueries.getCarsByBrand(brand);
	}

	public ArrayList<Car> getCarsByModel(String model) {
		return carQueries.getCarsByModel(model);
	}

	public ArrayList<Car> getCarsByCubiccapacity(String from, String to) {
		return carQueries.getCarsByCubiccapacity(from, to);
	}

	public ArrayList<Car> getCarsByFuel(String fuel) {
		return carQueries.getCarsByFuel(fuel);
	}

	public boolean isAvailable(String idcar) {
		return carQueries.isAvailable(idcar);
	}

	public boolean checkCar(String idcar) {
		return carQueries.checkCar(idcar);
	}
	public boolean checkPassword(char[] password, String name) {
    	return clientQueries.checkPassword(password, name);
    }
	public ArrayList<Car> filterCars(String brand, String model, String ccFrom, String ccTo, String priceFrom, String priceTo, String fuel){
		return carQueries.filterCars(brand, model, ccFrom, ccTo, priceFrom, priceTo, fuel);
	}
	public ArrayList<Car> recommendCar(String brand, String model, String ccFrom, String ccTo, String priceFrom, String priceTo, String fuel){
		return carQueries.recommendCar(brand, model, ccFrom, ccTo, priceFrom, priceTo, fuel);
	}
}
