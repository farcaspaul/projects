package BusinessLayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import DatabaseLayer.Car;
import DatabaseLayer.Client;
import DatabaseLayer.Contract;
import DatabaseLayer.Employee;
//import PresentationLayer.ClientRequest;

public class EchoRequest {
	ClientRequest client;
	ServerSocket serverSocket;
	Socket clientSocket;
	PrintWriter out;
	BufferedReader in;
	ClientBusinessLogic theClient;
	EmployeeBusinessLogic theEmployee;
	AdminBusinessLogic theAdmin;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void start(int port) throws IOException {
		theClient = new ClientBusinessLogic();
		theEmployee = new EmployeeBusinessLogic();
		theAdmin = new AdminBusinessLogic();

		serverSocket = new ServerSocket(port);
		clientSocket = serverSocket.accept();
		out = new PrintWriter(clientSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			if (".".equals(inputLine)) {
				out.println("good bye");
				break;
			}
			String[] words = inputLine.split("\\s+");
			if (words[0].equals("checkPassword")) {
				String password = words[1];
				String name = words[2];
				if (theClient.checkPassword(password.toCharArray(), name)) {
					out.println("valid client");
				} else {
					if (theEmployee.checkPassword(password.toCharArray(), name)) {
						out.println("valid employee");
					} else {
						if (name.equals("root") && new String(password).equals("root")) {
							out.println("valid admin");
						} else {
							out.println("invalid user");
						}
					}
				}
			} else {
				if (words[0].equals("client")) {
					// the client
					Object[] obj = Arrays.copyOfRange(words, 2, words.length);

					Class<?> params[] = new Class[obj.length];
					for (int i = 0; i < obj.length; i++) {
						params[i] = String.class;
					}
					try {
						Class myClass = theClient.getClass();
						Method method = myClass.getDeclaredMethod(words[1], params);

						ArrayList<Car> rez = (ArrayList<Car>) method.invoke(theClient, obj);
						String messageResult = "";
						Iterator<Car> it1 = rez.iterator();
						while (it1.hasNext()) {
							Car temp = it1.next();
							String availability = "available";
							if (temp.getAvailability() == false) {
								availability = "unavailable";
							}
							messageResult += temp.getIdcar() + " " + temp.getCarbrand() + "  " + temp.getCarmodel()
									+ "  " + temp.getCubiccapacity() + "  " + temp.getPrice() + "  " + temp.getFuel()
									+ " " + availability + "newLine";
						}
						out.println(messageResult);

					} catch (NoSuchMethodException | SecurityException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				} else {
					if (words[0].equals("employee")) {
						// the employee
						Object[] obj = Arrays.copyOfRange(words, 3, words.length);
						Class<?> params[] = new Class[obj.length];
						for (int i = 0; i < obj.length; i++) {
							params[i] = String.class;
						}
						try {
							Class myClass = theEmployee.getClass();
							Method method = myClass.getDeclaredMethod(words[2], params);

							if (words[1].equals("car")) {
								// car queries
								Object rez = method.invoke(theEmployee, obj);
								String messageResult = "";
								if (rez != null) {
									
									Iterator<Car> it1 = ((ArrayList<Car>) rez).iterator();
									while (it1.hasNext()) {
										
										Car temp = it1.next();
										String availability = "available";
										if (temp.getAvailability() == false) {
											availability = "unavailable";
										}
										messageResult += temp.getIdcar() + " " + temp.getCarbrand() + "  "
												+ temp.getCarmodel() + "  " + temp.getCubiccapacity() + "  "
												+ temp.getPrice() + "  " + temp.getFuel() + " " + availability
												+ " newLine";
									}
									System.out.println(messageResult);
									out.println(messageResult);
								} else {
									out.println("nothing");
								}
							} else {
								if (words[1].equals("contract")) {
									// contract queries
									Object rez = method.invoke(theEmployee, obj);
									String messageResult = "";
									if (rez != null) {
										
										Iterator<Contract> it1 = ((ArrayList<Contract>) rez).iterator();
										while (it1.hasNext()) {
											Contract temp = it1.next();
											String date = null;
											if(temp.getDate()!=null) {
												date = temp.getDate().substring(0, 10);
											}
											messageResult += temp.getIdcontract() + " "
													+ date + "  car " + temp.getIdcar()
													+ "  client " + temp.getIdclient() + "newLine";
										}
									//	System.out.println(messageResult);
										out.println(messageResult);
									} else {
										out.println("nothing");
									}
								} else {
									// client queries
									Object rez = method.invoke(theEmployee, obj);
									String messageResult = "";
									if (rez != null) {
										Iterator<Client> it1 = ((ArrayList<Client>) rez).iterator();
										while (it1.hasNext()) {
											Client temp = it1.next();
											messageResult += temp.getIdclient() + " " + temp.getName() + "newLine";
										}
										out.println(messageResult);
									} else {
										out.println("nothing");
									}
								}
							}

						} catch (NoSuchMethodException | SecurityException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
					} else {
						// theAdmin
						Object[] obj = Arrays.copyOfRange(words, 3, words.length);
						Class<?> params[] = new Class[obj.length];
						for (int i = 0; i < obj.length; i++) {
							params[i] = String.class;
						}
						try {
							Class myClass = theAdmin.getClass();
							Method method = myClass.getDeclaredMethod(words[2], params);

							if (words[1].equals("client")) {
								// client queries
								Object rez = method.invoke(theAdmin, obj);
								String messageResult = "";
								if (rez != null) {
									Iterator<Client> it1 = ((ArrayList<Client>) rez).iterator();
									while (it1.hasNext()) {
										Client temp = it1.next();

										messageResult += temp.getIdclient() + " " + temp.getName() + " "+temp.getAddress()+ " "+temp.getCnp()+"newLine";
									}
									out.println(messageResult);
								} else {
									out.println("nothing");
								}
							} else {
								if (words[1].equals("employee")) {
									// contract queries
									Object rez = method.invoke(theAdmin, obj);
									String messageResult = "";
									if (rez != null) {
										Iterator<Employee> it1 = ((ArrayList<Employee>) rez).iterator();
										while (it1.hasNext()) {
											Employee temp = it1.next();
											messageResult += temp.getIdemployee() + "  " + temp.getName() + "  "
													+ temp.getAddress() + "newLine";
										}
										out.println(messageResult);
									} else {
										out.println("nothing");
									}
								}
							}

						} catch (NoSuchMethodException | SecurityException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

//	public void tearDown() throws IOException {
//		client.stopConnection();
//	}

	public static void main(String[] args) throws IOException {
		EchoRequest server = new EchoRequest();
		server.start(4444);
	}
}