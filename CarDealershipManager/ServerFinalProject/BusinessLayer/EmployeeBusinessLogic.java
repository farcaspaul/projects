package BusinessLayer;

import java.util.ArrayList;

import DatabaseLayer.Car;
import DatabaseLayer.CarQueries;
import DatabaseLayer.Client;
import DatabaseLayer.ClientQueries;
import DatabaseLayer.Contract;
import DatabaseLayer.ContractQueries;
import DatabaseLayer.EmployeeQueries;

public class EmployeeBusinessLogic {
	private ContractQueries contractQueries = new ContractQueries();
	private EmployeeQueries employeeQueries = new EmployeeQueries();
	private CarQueries carQueries = new CarQueries();
	
	public void addCar(String carbrand, String carmodel, String cubiccapacity, String fuel, String price) {
        carQueries.addCar(carbrand, carmodel, cubiccapacity, fuel, price);
    }

    public void updateCar(String idcar, String carbrand, String carmodel, String cubiccapacity, String fuel, String availability, String price) {
        carQueries.updateCar(idcar, carbrand, carmodel, cubiccapacity, fuel, availability, price);
    }

    public void deleteCar(String idcar) {
        carQueries.deleteCar(idcar);
    }
    
    public ArrayList<Car> getCar(String idcar) {
		return carQueries.getCar(idcar);
	}

	public ArrayList<Car> getAllCars() {
		return carQueries.getAllCars();
	}

	public ArrayList<Car> getCarsByBrand(String brand) {
		return carQueries.getCarsByBrand(brand);
	}

	public ArrayList<Car> getCarsByModel(String model) {
		return carQueries.getCarsByModel(model);
	}

	public ArrayList<Car> getCarsByCubiccapacity(String from, String to) {
		return carQueries.getCarsByCubiccapacity(from, to);
	}
	public ArrayList<Car> getCarsByPrice(String from, String to) {
		return carQueries.getCarsByPrice(from, to);
	}
	public ArrayList<Car> getCarsByFuel(String fuel) {
		return carQueries.getCarsByFuel(fuel);
	}

	public boolean isAvailable(String idcar) {
		return carQueries.isAvailable(idcar);
	}

	public boolean checkCar(String idcar) {
		return carQueries.checkCar(idcar);
	}
	public ArrayList<Car> filterCars(String brand, String model, String ccFrom, String ccTo, String priceFrom, String priceTo, String fuel){
		return carQueries.filterCars(brand, model, ccFrom, ccTo, priceFrom, priceTo, fuel);
	}
	public ArrayList<Car> recommendCar(String brand, String model, String ccFrom, String ccTo, String priceFrom, String priceTo, String fuel) {
		return carQueries.recommendCar(brand, model, ccFrom, ccTo, priceFrom, priceTo, fuel);
	}
	public void addContract(String date, String idcar, String idclient) {
		contractQueries.addContract(date, idcar, idclient);
	}

	public void updateContract(String idcontract, String date, String idcar, String idclient) {
		contractQueries.updateContract(idcontract, date, idcar, idclient);
	}

	public void deleteContract(String idcontract) {
		contractQueries.deleteContract(idcontract);
	}

	public ArrayList<Contract> getContract(String idcontract) {
		return contractQueries.getContract(idcontract);
	}

	public ArrayList<Contract> getAllContracts() {
		return contractQueries.getAllContracts();
	}

	public ArrayList<Contract> getContractsByBrandFromTimeInterval(String brand, String startDate, String endDate) {
		return contractQueries.getContractsByBrandFromTimeInterval(brand, startDate, endDate);
	}

	public boolean checkContract(String idcontract) {
		return contractQueries.checkContract(idcontract);
	}
	
	public boolean checkPassword(char[] password, String name) {
		return employeeQueries.checkPassword(password, name);
	}
	
	public ArrayList<Client> getAllClients(){
		return new ClientQueries().getAllClients();
	}
	public ArrayList<Client> getClient(String id){
		return new ClientQueries().getClient(id);
	}
	public ArrayList<Car> getInterestReport(String startDate, String endDate){
		return contractQueries.getInterestReport(startDate, endDate);
	}
}
