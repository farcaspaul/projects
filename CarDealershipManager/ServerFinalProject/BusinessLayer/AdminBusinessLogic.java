package BusinessLayer;

import java.util.ArrayList;

import DatabaseLayer.Client;
import DatabaseLayer.ClientQueries;
import DatabaseLayer.Employee;
import DatabaseLayer.EmployeeQueries;

public class AdminBusinessLogic {
	EmployeeQueries employeeQueries = new EmployeeQueries();
	ClientQueries clientQueries = new ClientQueries();
	
	public void addClient(String name, String cnp, String password, String address) {
        clientQueries.addClient(name, cnp, password, address);
    }

    public void updateClient(String idclient, String name, String cnp, String address) {
        clientQueries.updateClient(idclient, name, cnp, address);
    }

    public void deleteClient(String idclient) {
        clientQueries.deleteClient(idclient);
    }
    
    public void deleteClientByName(String name) {
        clientQueries.deleteClientByName(name);
    }
   
    public ArrayList<Client> getClient(String idclient) {
        return clientQueries.getClient(idclient);
    }

    public ArrayList<Client> getClientByName(String name) {
    	return clientQueries.getClientByName(name);
    }
    
    public ArrayList<Client> getAllClients() {
    	return clientQueries.getAllClients();
    }

    public boolean checkClient(String idclient) {
        return clientQueries.checkClient(idclient);
    }

    public boolean checkClientByName(String name) {
        return clientQueries.checkClientByName(name);
    }
    
    public void addEmployee(String name, String address, String password) {
        employeeQueries.addEmployee(name, address, password);
    }

    public void updateEmployee(String idemployee, String name, String address) {
    	employeeQueries.updateEmployee(idemployee, name, address);
    }

    public void deleteEmployee(String idemployee) {
    	employeeQueries.deleteEmployee(idemployee);
    }
    
    public void deleteEmployeeByName(String name) {
    	employeeQueries.deleteEmployeeByName(name);
    }
    

    public ArrayList<Employee> getEmployee(String idemployee) {
    	return employeeQueries.getEmployee(idemployee);
    }

    public Employee getEmployeeByName(String name) {
    	return employeeQueries.getEmployeeByName(name);
    }
    
    public ArrayList<Employee> getAllEmployees() {
    	return employeeQueries.getAllEmployees();
    }

    public boolean checkEmployee(String idemployee) {
        return employeeQueries.checkEmployee(idemployee);
    }

    public boolean checkEmployeeByName(String name) {
    	return employeeQueries.checkEmployeeByName(name);
    }
}
