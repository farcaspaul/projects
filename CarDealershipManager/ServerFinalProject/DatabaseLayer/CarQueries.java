package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

public class CarQueries {

	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	private DBConnect connection = null;

	public CarQueries() {
		connection = new DBConnect();
	}

	public void addCar(String carbrand, String carmodel, String cubiccapacity, String fuel, String price) {
		try {
			String query = "INSERT INTO dealer.car (carbrand, carmodel, cubiccapacity, fuel, availability, price) VALUES ('"
					+ carbrand + "', '" + carmodel + "', '" + cubiccapacity + "', '" + fuel + "', '" + 1 + "', '"
					+ price + "');";
			preparedStatement = connection.getDBConnect().prepareStatement(query);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateCar(String idcar, String carbrand, String carmodel, String cubiccapacity, String fuel,
			String availability, String price) {
		try {
			String query = "update dealer.car set carmodel = '" + carmodel + "' , carbrand = '" + carbrand
					+ "' , cubiccapacity = '" + cubiccapacity + "' , availability = '" + availability + "' , fuel = '"
					+ fuel + "' , price = '" + price + "' where idcar = " + idcar + ";";
			preparedStatement = connection.getDBConnect().prepareStatement(query);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteCar(String idcar) {
		try {
			String query1 = "delete from dealer.contract where idcar = " + idcar + ";";
			preparedStatement = connection.getDBConnect().prepareStatement(query1);
			preparedStatement.executeUpdate();
			
			String query = "delete from dealer.car where idcar = " + idcar + ";";
			preparedStatement = connection.getDBConnect().prepareStatement(query);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Car> getCar(String idcar) {
		Car rezultat = new Car();
		ArrayList<Car> rez = new ArrayList<Car>();
		try {
			String query = "SELECT * FROM dealer.car WHERE idcar =" + idcar + ";";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				rezultat.setIdcar(Integer.parseInt(resultSet.getString(1 + 0)));
				rezultat.setCarbrand(resultSet.getString(1 + 1));
				rezultat.setCarmodel(resultSet.getString(1 + 2));
				rezultat.setCubiccapacity(Integer.parseInt(resultSet.getString(1 + 3)));
				rezultat.setFuel(resultSet.getString(1 + 4));
				if (resultSet.getString(1 + 5).equals("1")) {
					rezultat.setAvailability(true);
				} else {
					rezultat.setAvailability(false);
				}
				rezultat.setPrice(Float.parseFloat(resultSet.getString(1 + 6)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		rez.add(rezultat);
		return rez;
	}

	public ArrayList<Car> getAllCars() {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		try {
			String query = "SELECT * FROM dealer.car;";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				Car temp = new Car();
				temp.setIdcar(Integer.parseInt(resultSet.getString(1 + 0)));
				temp.setCarbrand(resultSet.getString(1 + 1));
				temp.setCarmodel(resultSet.getString(1 + 2));
				temp.setCubiccapacity(Integer.parseInt(resultSet.getString(1 + 3)));
				temp.setFuel(resultSet.getString(1 + 4));
				System.out.println(resultSet.getString(1 + 5));
				if (resultSet.getString(1 + 5).equals("1")) {
					temp.setAvailability(true);
				} else {
					temp.setAvailability(false);
				}
				temp.setPrice(Float.parseFloat(resultSet.getString(1 + 6)));
				rezultat.add(temp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByBrand(String brand) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = getAllCars();
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getCarbrand().equals(brand)) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByBrand(String brand, ArrayList<Car> currentCarList) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = currentCarList;
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getCarbrand().equals(brand)) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByModel(String model) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = getAllCars();
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getCarmodel().equals(model)) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByModel(String model, ArrayList<Car> currentCarList) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = currentCarList;
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getCarmodel().equals(model)) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByCubiccapacity(String from, String to) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = getAllCars();
		int start = 0, end = 10000;
		if (from.length() > 1 ) {
			start = Integer.parseInt(from);
			
		} else {
			start = 0;
		}
		if (to.length() > 1  ) {
			end = Integer.parseInt(to);
			
		} else {
			end = 10000;
		}
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getCubiccapacity() >= start && temp.getCubiccapacity() <= end) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByCubiccapacity(String from, String to, ArrayList<Car> currentCarList) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = currentCarList;
		int start = 0, end = 10000;
	//	System.out.println("From = "+from + " to = "+to);
		if (from.length() > 1 ) {
			start = Integer.parseInt(from);
			
		} else {
			start = 0;
		}
		if (to.length() > 1  ) {
			end = Integer.parseInt(to);
			
		} else {
			end = 10000;
		}
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getCubiccapacity() >= start && temp.getCubiccapacity() <= end) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByPrice(String from, String to) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = getAllCars();
		float start = 0.0f, end = 1000000.0f;
		if (from.length() > 1) {
			start = Float.parseFloat(from);
			
		} else {
			start = 0;
		}
		if (to.length() > 1) {
			end = Float.parseFloat(to);
		} else {
			end = 1000000;
			
		}
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getPrice() >= start && temp.getPrice() <= end) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByPrice(String from, String to, ArrayList<Car> currentCarList) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = currentCarList;
		float start = 0.0f, end = 1000000.0f;
		if (from.length() > 1) {
			start = Float.parseFloat(from);
			
		} else {
			start = 0;
		}
		if (to.length() > 1) {
			end = Float.parseFloat(to);
		} else {
			end = 1000000;
			
		}
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getPrice() >= start && temp.getPrice() <= end) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByFuel(String fuel) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = getAllCars();
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getFuel().equals(fuel)) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> getCarsByFuel(String fuel, ArrayList<Car> currentCarList) {
		ArrayList<Car> rezultat = new ArrayList<Car>();
		ArrayList<Car> allCars = currentCarList;
		Iterator<Car> it = allCars.iterator();
		while (it.hasNext()) {
			Car temp = it.next();
			if (temp.getFuel().equals(fuel)) {
				rezultat.add(temp);
			}
		}
		return rezultat;
	}

	public ArrayList<Car> filterCars(String brand, String model, String ccFrom, String ccTo, String priceFrom,
			String priceTo, String fuel) {
		ArrayList<Car> carList = getAllCars();
		if (!brand.equals("-")) {
			carList = getCarsByBrand(brand, carList);
		}
		if (!model.equals("-")) {
			carList = getCarsByModel(model, carList);
		}
		if ((!ccFrom.equals("-")) || (!ccTo.equals("-"))) {
			carList = getCarsByCubiccapacity(ccFrom, ccTo, carList);
		}
		if ((!priceFrom.equals("-")) || (!priceTo.equals("-"))) {
			carList = getCarsByPrice(priceFrom, priceTo, carList);
		}
		if (fuel.equals("benzina") || fuel.equals("motorina") || fuel.equals("diesel") || fuel.equals("petrol")
				|| fuel.equals("GPL") || fuel.equals("gpl")) {
			carList = getCarsByFuel(fuel, carList);
		}
		return carList;
	}

	public int frequency(ArrayList<Contract> contractList, Car car) {
		int rez = 0;
		int idcar = car.getIdcar();
		Iterator<Contract> it = contractList.iterator();
		while (it.hasNext()) {
			Contract temp = it.next();
			if (temp.getIdcar() == idcar) {
				rez++;
			}
		}
		return rez;
	}

	public ArrayList<Car> recommendCar(String brand, String model, String ccFrom, String ccTo, String priceFrom,
			String priceTo, String fuel) {
		int max = 0;
		Car tempCar=null;
		ArrayList<Car> mostWantedCar = new ArrayList<Car>();
		ArrayList<Car> carList = filterCars(brand, model, ccFrom, ccTo, priceFrom, priceTo, fuel);
		if (!carList.isEmpty()) {
			mostWantedCar.add(carList.get(0));
			ArrayList<Contract> contractList = new ContractQueries().getAllContracts();
			Iterator<Car> it = carList.iterator();
			while (it.hasNext()) {
				Car temp = it.next();
				int tempFrequency = frequency(contractList, temp);
				if (tempFrequency >= max) {
					tempCar = temp;
					max = tempFrequency;
				}
			}
			if(tempCar!=null) {
				mostWantedCar.remove(0);
				mostWantedCar.add(tempCar);
			}
		}

		return mostWantedCar;
	}

	public boolean isAvailable(String idcar) {
		try {
			String query = "SELECT * FROM dealer.car WHERE idcar =" + idcar + ";";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				if (resultSet.getString(1 + 5).equals("1")) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean checkCar(String idcar) {
		boolean found = false;
		try {
			String query = "SELECT * FROM dealer.car WHERE idcar =" + idcar + ";";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				if (resultSet.isFirst())
					found = true;
				else
					found = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return found;
	}

}
