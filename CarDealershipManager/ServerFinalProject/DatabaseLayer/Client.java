package DatabaseLayer;

public class Client {
//	  `idclient` int(11) NOT NULL AUTO_INCREMENT,
//	  `name` varchar(45) NOT NULL,
//	  `cnp` varchar(13) NOT NULL,
//	  `password` varchar(45) NOT NULL,
//	  `address` varchar(45) NOT NULL,
	private int idclient;
	private String name;
	private String cnp;
	private String password;
	private String address;
	
	public Client() {}

	public Client(int idclient, String name, String cnp, String password, String address) {
		this.idclient = idclient;
		this.name = name;
		this.cnp = cnp;
		this.password = password;
		this.address = address;
	}

	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
