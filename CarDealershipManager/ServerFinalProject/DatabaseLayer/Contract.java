package DatabaseLayer;

public class Contract {
	private int idcontract;
	private String date;
	private int idcar;
	private int idclient;
	public Contract() {}
	public Contract(int idcontract, String date, int idcar, int idclient) {
		this.idcontract = idcontract;
		this.date = date;
		this.idcar = idcar;
		this.idclient = idclient;
	}
	public int getIdcontract() {
		return idcontract;
	}
	public void setIdcontract(int idcontract) {
		this.idcontract = idcontract;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getIdcar() {
		return idcar;
	}
	public void setIdcar(int idcar) {
		this.idcar = idcar;
	}
	public int getIdclient() {
		return idclient;
	}
	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}
	
}
