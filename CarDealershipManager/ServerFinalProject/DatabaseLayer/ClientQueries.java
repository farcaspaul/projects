package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ClientQueries {

    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private DBConnect connection = null;

    public ClientQueries() {
    	connection = new DBConnect();
    }

    public void addClient(String name, String cnp, String password, String address) {
        try {
            String query = "INSERT INTO dealer.client (name, cnp,  address, password) VALUES ('" + name + "', '" + cnp + "', '" +address +"', '" + password + "');";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateClient(String idclient, String name, String cnp, String address) {
        try {
            String query = "update dealer.client set address = '" + address + "' , cnp = '" + cnp + "' , name = '" + name + "' where idclient = " + idclient + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteClient(String idclient) {
        try {
            String query = "delete from dealer.client where idclient = " + idclient + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
            
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteClientByName(String name) {
        try {
            String query = "delete from dealer.client where name = '" + name + "';";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
   
    public ArrayList<Client> getClient(String idclient) {
    	ArrayList<Client> rez = new ArrayList<Client>();
    	Client rezultat = new Client();
        try {
            String query = "SELECT * FROM dealer.client WHERE idclient =" + idclient + ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
            	
                    rezultat.setIdclient(Integer.parseInt(resultSet.getString(1)));
                    rezultat.setName(resultSet.getString(2)); 
                    rezultat.setCnp(resultSet.getString(3));
                    rezultat.setPassword(resultSet.getString(4));
                    rezultat.setAddress(resultSet.getString(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        rez.add(rezultat);
        return rez;
    }

    public ArrayList<Client> getClientByName(String name) {
    	Client rezultat = new Client();
    	ArrayList<Client> rez = new ArrayList<Client>();
        try {
            String query = "SELECT * FROM dealer.client WHERE name =" + "'" + name + "'"+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
            	rezultat.setIdclient(Integer.parseInt(resultSet.getString(0+1)));
                rezultat.setName(resultSet.getString(1+1)); 
                rezultat.setCnp(resultSet.getString(2+1));
                rezultat.setPassword(resultSet.getString(3+1));
                rezultat.setAddress(resultSet.getString(4+1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        rez.add(rezultat);
        return rez;
    }
    
    public ArrayList<Client> getAllClients() {
    	ArrayList<Client> rezultat = new ArrayList<Client>();
        try {
            String query = "SELECT * FROM dealer.client;";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Client temp = new Client();
                temp.setIdclient(Integer.parseInt(resultSet.getString(0+1)));
                temp.setName(resultSet.getString(1+1)); 
                temp.setCnp(resultSet.getString(2+1));
                temp.setPassword(resultSet.getString(3+1));
                temp.setAddress(resultSet.getString(4+1));
                rezultat.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public boolean checkClient(String idclient) {
        boolean found = false;
        try {
            String query = "SELECT * FROM dealer.client WHERE idclient =" + idclient + ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }

    public boolean checkClientByName(String name) {
        boolean found = false;
        try {
            String query = "SELECT * FROM dealer.client WHERE name ='" + name + "';";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }
   
    public boolean checkPassword(char[] password, String name) {
    	String pass="";
    	try {
            String query = "SELECT * FROM dealer.client WHERE name =" + "'" + name + "'"+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
            	pass = resultSet.getString("password");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	if(password.length== pass.length()) {
    		for(int i = 0 ; i < password.length;i++) {
    			if(password[i]!=pass.charAt(i)) {
    				return false;
    			}
    		}
    	}else {
    		return false;
    	}
    	return true;
    }
}
