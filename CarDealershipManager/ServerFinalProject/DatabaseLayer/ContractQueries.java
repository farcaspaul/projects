package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ContractQueries {

	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	private DBConnect connection = null;

	public ContractQueries() {
		connection = new DBConnect();
	}

	public void addContract(String date, String idcar, String idclient) {
		try {
			String query1 = "update dealer.car set availability = false where idcar = '" + idcar +"';";
			preparedStatement = connection.getDBConnect().prepareStatement(query1);
			preparedStatement.executeUpdate();
			
			String query2 = "INSERT INTO dealer.contract (date, idcar, idclient) VALUES ('" + date + "', '" + idcar
					+ "', '" + idclient + "');";
			preparedStatement = connection.getDBConnect().prepareStatement(query2);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateContract(String idcontract, String date, String idcar, String idclient) {
		try {
			String query1 = "update dealer.car set availability = false where idcar = '" + idcar +"';";
			preparedStatement = connection.getDBConnect().prepareStatement(query1);
			preparedStatement.executeUpdate();
			Car temp = new CarQueries().getCar(this.getContract(idcontract).get(0).getIdcar()+"").get(0);
			String query2 = "update dealer.car set availability = true where idcar = '" + temp.getIdcar() +"';";
			preparedStatement = connection.getDBConnect().prepareStatement(query2);
			preparedStatement.executeUpdate();
			
			String query = "update dealer.contract set date = '" + date + "' , idcar = '" + idcar + "' , idclient = '"
					+ idclient + "' where idcontract = " + idcontract + ";";
			preparedStatement = connection.getDBConnect().prepareStatement(query);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteContract(String idcontract) {
		try {
			Car temp = new CarQueries().getCar(this.getContract(idcontract).get(0).getIdcar()+"").get(0); 
			String query1 = "update dealer.car set availability = true where idcar = '" + temp.getIdcar() +"';";
			preparedStatement = connection.getDBConnect().prepareStatement(query1);
			preparedStatement.executeUpdate();
			
			String query = "delete from dealer.contract where idcontract = " + idcontract + ";";
			preparedStatement = connection.getDBConnect().prepareStatement(query);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Contract> getContract(String idcontract) {
		Contract rezultat = new Contract();
		ArrayList<Contract> rez = new ArrayList<Contract>();
		try {
			String query = "SELECT * FROM dealer.contract WHERE idcontract =" + idcontract + ";";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				rezultat.setIdcontract(Integer.parseInt(resultSet.getString(0+1)));
				rezultat.setDate(resultSet.getString(1+1));
				rezultat.setIdcar(Integer.parseInt(resultSet.getString(2+1)));
				rezultat.setIdclient(Integer.parseInt(resultSet.getString(3+1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		rez.add(rezultat);		
		return rez;
	}

	public ArrayList<Contract> getAllContracts() {
		ArrayList<Contract> rezultat = new ArrayList<Contract>();
		try {
			String query = "SELECT * FROM dealer.contract;";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				Contract temp = new Contract();
			//	System.out.println("BELLZ");
				temp.setIdcontract(Integer.parseInt(resultSet.getString(0+1)));
				temp.setDate(resultSet.getString(1+1));
				temp.setIdcar(Integer.parseInt(resultSet.getString(2+1)));
				temp.setIdclient(Integer.parseInt(resultSet.getString(3+1)));
				rezultat.add(temp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rezultat;
	}

	public ArrayList<Contract> getContractsByBrandFromTimeInterval(String brand, String startDate, String endDate) {
		ArrayList<Contract> rezultat = new ArrayList<Contract>();
		try {
			String query = "SELECT contract.date, contract.idcar from dealer.contract INNER JOIN dealer.car\r\n"
					+ "                     on contract.idcar = car.idcar WHERE contract.date >= '" + startDate
					+ "' AND contract.date <= '" + endDate + "';";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				Contract temp = new Contract();
				temp.setDate(resultSet.getString(1+0));
				temp.setIdcar(Integer.parseInt(resultSet.getString(1+1)));
				if (new CarQueries().getCar(temp.getIdcar() + "").get(0).getCarbrand().equals(brand)) {
					rezultat.add(temp);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rezultat;
	}


	public boolean checkContract(String idcontract) {
		boolean found = false;
		try {
			String query = "SELECT * FROM dealer.contract WHERE idcontract =" + idcontract + ";";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				if (resultSet.isFirst())
					found = true;
				else
					found = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return found;
	}
	
	public ArrayList<Car> getInterestReport(String startDate, String endDate) {
		ArrayList<Contract> contractList = new ArrayList<Contract>();
		ArrayList<Car> rez = new ArrayList<Car>();
		ArrayList<Car> carList = new CarQueries().getAllCars();
		ArrayList<Integer> frequencyList = new ArrayList<Integer>();
		CarQueries carQueries = new CarQueries();
		try {
			String query = "SELECT * from dealer.contract WHERE contract.date >= '" + startDate
					+ "' AND contract.date <= '" + endDate + "';";
			statement = connection.getDBConnect().createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				Contract temp = new Contract();
				temp.setIdcontract(Integer.parseInt(resultSet.getString(1+0)));
				temp.setDate(resultSet.getString(1+1));
				temp.setIdcar(Integer.parseInt(resultSet.getString(1+2)));
				temp.setIdclient(Integer.parseInt(resultSet.getString(1+3)));
				contractList.add(temp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i<carList.size();i++) {
			frequencyList.add(carQueries.frequency(contractList,carList.get(i)));
		}
		for(int i = 0 ; i < 3; i++) {
			int max = 0;
			int poz = 0;
			for(int j = 0; j<frequencyList.size();j++) {
				if(frequencyList.get(j)>=max) {
					poz=j;
					max = frequencyList.get(j);
				}
			}
			rez.add(carList.get(poz));
			frequencyList.remove(poz);
		}
		
		
		return rez;
//		frequency(ArrayList<Contract> contractList, Car car)
	}

}
