package DatabaseLayer;

public class Car {
	private int idcar;
	private String carbrand;
	private String carmodel;
	private int cubiccapacity;
	private String fuel;
	private boolean availability;
	private float price;
	public Car(int idcar, String carbrand, String carmodel, int cubiccapacity, String fuel, float price) {
		this.idcar = idcar;
		this.carbrand = carbrand;
		this.carmodel = carmodel;
		this.cubiccapacity = cubiccapacity;
		this.fuel = fuel;
		this.availability=true;
		this.price = price;
	}
	public Car() {}
	public int getIdcar() {
		return idcar;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public void setIdcar(int idcar) {
		this.idcar = idcar;
	}
	public String getCarbrand() {
		return carbrand;
	}
	public void setCarbrand(String carbrand) {
		this.carbrand = carbrand;
	}
	public String getCarmodel() {
		return carmodel;
	}
	public void setCarmodel(String carmodel) {
		this.carmodel = carmodel;
	}
	public int getCubiccapacity() {
		return cubiccapacity;
	}
	public void setCubiccapacity(int cubiccapacity) {
		this.cubiccapacity = cubiccapacity;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public boolean getAvailability() {
		return availability;
	}
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}
	
}
