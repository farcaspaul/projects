package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class EmployeeQueries {

    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private DBConnect connection = null;

    public EmployeeQueries() {
    	connection = new DBConnect();
    }

    public void addEmployee(String name, String address, String password) {
        try {
            String query = "INSERT INTO dealer.employee (name, address, password) VALUES ('" + name + "', '" + address +"', '" + password + "');";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateEmployee(String idemployee, String name, String address) {
        try {
            String query = "update dealer.employee set address = '" + address + "' , name = '" + name + "' where idemployee = " + idemployee + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteEmployee(String idemployee) {
        try {
            String query = "delete from dealer.employee where idemployee = " + idemployee + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
            
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteEmployeeByName(String name) {
        try {
            String query = "delete from dealer.employee where name = '" + name + "';";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public ArrayList<Employee> getEmployee(String idemployee) {
        Employee rezultat = new Employee();
        ArrayList<Employee> rez = new ArrayList<Employee>();
        try {
            String query = "SELECT * FROM dealer.employee WHERE idemployee =" + idemployee + ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                    rezultat.setIdemployee(Integer.parseInt(resultSet.getString(1+0)));
                    rezultat.setName(resultSet.getString(1+1)); 
                    rezultat.setAddress(resultSet.getString(1+2));
                    rezultat.setPassword(resultSet.getString(1+3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        rez.add(rezultat);
        return rez;
    }
    public Employee getEmployeeByName(String name) {
    	Employee rezultat = new Employee();
        try {
            String query = "SELECT * FROM dealer.employee WHERE name =" + "'" + name + "'"+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
            	rezultat.setIdemployee(Integer.parseInt(resultSet.getString(1+0)));
                rezultat.setName(resultSet.getString(1+1)); 
                rezultat.setAddress(resultSet.getString(1+2));
                rezultat.setPassword(resultSet.getString(1+3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }
    
    public ArrayList<Employee> getAllEmployees() {
    	ArrayList<Employee> rezultat = new ArrayList<Employee>();
        try {
            String query = "SELECT * FROM dealer.employee;";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Employee temp = new Employee();
                temp.setIdemployee(Integer.parseInt(resultSet.getString(1+0)));
                temp.setName(resultSet.getString(1+1)); 
                temp.setAddress(resultSet.getString(1+2));
                temp.setPassword(resultSet.getString(1+3));
                rezultat.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public boolean checkEmployee(String idemployee) {
        boolean found = false;
        try {
            String query = "SELECT * FROM dealer.employee WHERE idemployee =" + idemployee + ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }

    public boolean checkEmployeeByName(String name) {
        boolean found = false;
        try {
            String query = "SELECT * FROM dealer.employee WHERE name ='" + name + "';";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }
    public boolean checkPassword(char[] password, String name) {
    	String pass="";
    	try {
            String query = "SELECT * FROM dealer.employee WHERE name =" + "'" + name + "'"+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
            	pass = resultSet.getString("password");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	if(password.length== pass.length()) {
    		for(int i = 0 ; i < password.length;i++) {
    			if(password[i]!=pass.charAt(i)) {
    				return false;
    			}
    		}
    	}else {
    		return false;
    	}
    	return true;
    }
}
