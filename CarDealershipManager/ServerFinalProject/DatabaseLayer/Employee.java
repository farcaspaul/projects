package DatabaseLayer;

public class Employee {
	private int idemployee;
	private String name;
	private String address;
	private String password;
	public Employee() {};
	public Employee(int id,String name, String address, String password) {
		this.idemployee = id;
		this.name = name;
		this.address = address;
		this.password = password;
	}
	public int getIdemployee() {
		return idemployee;
	}
	public void setIdemployee(int id) {
		this.idemployee = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
