package PresentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import utilities.Utilities;

import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class EmployeeScreen {

	private JFrame frmEmployeeMenu;
	private JTextField caridText;
	private JTextField brandText;
	private JTextField modelText;
	private JLabel lblCcFrom;
	private JTextField ccText;
	private JLabel lblPrice;
	private JTextField priceText;
	private JLabel lblFuel;
	private JTextField fuelText;
	private JTextField contractidText;
	private JTextField dateText;
	private JLabel lblCarId_1;
	private JTextField caridContractText;
	private JLabel lblClientId;
	private JTextField clientidText;
	private ScrollPane scrollPaneContracts;
	private ScrollPane scrollPaneClients;
	private JLabel lblClients;
	private JButton addContractButton;
	private JButton btnUpdate_1;
	private JButton deleteContractButton;
	private ClientRequest clientRequest;
	private JTextPane textPaneCar, textPaneContract, textPaneClient;
	private JButton btnRefresh;
	private JLabel lblAvailability;
	private JTextField availabilityText;

	public void refreshTextPane(JTextPane textPane, String input) {
		textPane.setText(input);
	}

	public EmployeeScreen(ClientRequest clientRequest) {
		this.clientRequest = clientRequest;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEmployeeMenu = new JFrame();
		frmEmployeeMenu.setTitle("Employee Menu");
		frmEmployeeMenu.setResizable(false);
		frmEmployeeMenu.setBounds(100, 100, 490, 482);
		frmEmployeeMenu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmEmployeeMenu.getContentPane().setLayout(null);

		JLabel lblCarId = new JLabel("Car id");
		lblCarId.setBounds(10, 11, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblCarId);

		caridText = new JTextField();
		caridText.setBounds(85, 11, 86, 20);
		frmEmployeeMenu.getContentPane().add(caridText);
		caridText.setColumns(10);

		JLabel lblBrand = new JLabel("Brand");
		lblBrand.setBounds(10, 36, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblBrand);

		brandText = new JTextField();
		brandText.setBounds(85, 36, 86, 20);
		frmEmployeeMenu.getContentPane().add(brandText);
		brandText.setColumns(10);

		JLabel lblModel = new JLabel("Model");
		lblModel.setBounds(10, 61, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblModel);

		modelText = new JTextField();
		modelText.setBounds(85, 61, 86, 20);
		frmEmployeeMenu.getContentPane().add(modelText);
		modelText.setColumns(10);

		lblCcFrom = new JLabel("CC");
		lblCcFrom.setBounds(10, 86, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblCcFrom);

		ccText = new JTextField();
		ccText.setBounds(85, 86, 86, 20);
		frmEmployeeMenu.getContentPane().add(ccText);
		ccText.setColumns(10);

		lblPrice = new JLabel("Price");
		lblPrice.setBounds(10, 111, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblPrice);

		priceText = new JTextField();
		priceText.setBounds(85, 111, 86, 20);
		frmEmployeeMenu.getContentPane().add(priceText);
		priceText.setColumns(10);

		lblFuel = new JLabel("Fuel");
		lblFuel.setBounds(10, 136, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblFuel);

		fuelText = new JTextField();
		fuelText.setBounds(85, 136, 86, 20);
		frmEmployeeMenu.getContentPane().add(fuelText);
		fuelText.setColumns(10);

		ScrollPane scrollPaneCar = new ScrollPane();
		scrollPaneCar.setBounds(177, 10, 297, 165);
		textPaneCar = new JTextPane();
		// textPaneCar.setBounds(181, 11, 293, 164);
		// frmEmployeeMenu.getContentPane().add(textPaneCar);
		try {
			refreshTextPane(textPaneCar, Utilities
					.transformString(clientRequest.sendMessage("employee" + " " + "car" + " " + "getAllCars")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		textPaneCar.setEditable(false);
		scrollPaneCar.add(textPaneCar);
		frmEmployeeMenu.getContentPane().add(scrollPaneCar);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String brand = "-";
				if (brandText.getText().length() > 0) {
					brand = brandText.getText();
					brand = brand.replaceAll(" ", "_");
				}
				String model = "-";
				if (modelText.getText().length() > 0) {
					model = modelText.getText();
					model = model.replaceAll(" ", "_");
				}
				String cc = "-";
				if (ccText.getText().length() > 0) {
					cc = ccText.getText();
				}

				String price = "-";
				if (priceText.getText().length() > 0) {
					price = priceText.getText();
				}

				String fuel = "-";
				if (fuelText.getText().length() > 0) {
					fuel = fuelText.getText();
				}

				if (brand.length() > 2 && Utilities.isAlpha(brand)) {
					if (model.length() > 2 && Utilities.isAlphaNumeric(model)) {
						if (cc.length() > 1 && Utilities.isNumeric(cc)) {
							if (price.length() > 1 && Utilities.isNumeric(price)) {
								if (Arrays.asList(Utilities.fuelTypeList).contains(fuel) && Utilities.isAlpha(fuel)) {
									try {
										clientRequest.sendMessage("employee" + " " + "car " + "addCar " + brand + " "
												+ model + " " + cc + " " + fuel + " " + price);
										refreshTextPane(textPaneCar, Utilities.transformString(clientRequest
												.sendMessage("employee" + " " + "car" + " " + "getAllCars")));
										fuelText.setText(null);
										priceText.setText(null);
										ccText.setText(null);
										modelText.setText(null);
										brandText.setText(null);
									} catch (IOException e) {
										e.printStackTrace();
									}
								} else {
									fuelText.setText(null);
									JOptionPane.showMessageDialog(null, "Invalid fuel.", "WARNING",
											JOptionPane.WARNING_MESSAGE);
								}
							} else {
								priceText.setText(null);
								JOptionPane.showMessageDialog(null, "Invalid price.", "WARNING",
										JOptionPane.WARNING_MESSAGE);
							}
						} else {
							ccText.setText(null);
							JOptionPane.showMessageDialog(null, "Invalid cubic capacity.", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					} else {
						modelText.setText(null);
						JOptionPane.showMessageDialog(null, "Invalid model.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
				} else {
					brandText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid brand.", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnAdd.setBounds(20, 186, 89, 23);
		frmEmployeeMenu.getContentPane().add(btnAdd);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String availability = "-";
				if (availabilityText.getText().length() > 0) {
					availability = availabilityText.getText();
				}
				String brand = "-";
				if (brandText.getText().length() > 0) {
					brand = brandText.getText();
					brand = brand.replaceAll(" ", "_");
				}
				String model = "-";
				if (modelText.getText().length() > 0) {
					model = modelText.getText();
					model = model.replaceAll(" ", "_");
				}
				String cc = "-";
				if (ccText.getText().length() > 0) {
					cc = ccText.getText();
				}

				String price = "-";
				if (priceText.getText().length() > 0) {
					price = priceText.getText();
				}

				String fuel = "-";
				if (fuelText.getText().length() > 0) {
					fuel = fuelText.getText();
				}
				String id = "-";
				if (caridText.getText().length() > 0) {
					id = caridText.getText();
				}
				caridText.setText(null);
				fuelText.setText(null);
				priceText.setText(null);
				ccText.setText(null);
				modelText.setText(null);
				brandText.setText(null);
				String isExisting = "";
				try {
					isExisting = clientRequest.sendMessage("employee" + " " + "car" + " " + "getCar" + " " + id);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (Utilities.isNumeric(id) && !(isExisting.charAt(0) == '0')) {
					if (brand.length() > 2 && Utilities.isAlpha(brand)) {
						if (model.length() > 2 && Utilities.isAlphaNumeric(model)) {
							if (cc.length() > 1 && Utilities.isNumeric(cc)) {
								if (price.length() > 1 && Utilities.isNumeric(price)) {
									if (Arrays.asList(Utilities.fuelTypeList).contains(fuel)
											&& Utilities.isAlpha(fuel)) {
										try {
											clientRequest.sendMessage("employee" + " " + "car " + "updateCar " + id
													+ " " + brand + " " + model + " " + cc + " " + fuel + " "
													+ availability + " " + price);
											refreshTextPane(textPaneCar, Utilities.transformString(clientRequest
													.sendMessage("employee" + " " + "car" + " " + "getAllCars")));
											fuelText.setText(null);
											priceText.setText(null);
											ccText.setText(null);
											modelText.setText(null);
											brandText.setText(null);
											caridText.setText(null);
											availabilityText.setText(null);
										} catch (IOException e1) {
											e1.printStackTrace();
										}
									} else {
										fuelText.setText(null);
										JOptionPane.showMessageDialog(null, "Invalid fuel.", "WARNING",
												JOptionPane.WARNING_MESSAGE);
									}
								} else {
									priceText.setText(null);
									JOptionPane.showMessageDialog(null, "Invalid price.", "WARNING",
											JOptionPane.WARNING_MESSAGE);
								}
							} else {
								ccText.setText(null);
								JOptionPane.showMessageDialog(null, "Invalid cubic capacity.", "WARNING",
										JOptionPane.WARNING_MESSAGE);
							}
						} else {
							modelText.setText(null);
							JOptionPane.showMessageDialog(null, "Invalid model.", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					} else {
						brandText.setText(null);
						JOptionPane.showMessageDialog(null, "Invalid brand.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
				} else {
					caridText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid id.", "WARNING", JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		btnUpdate.setBounds(139, 186, 89, 23);
		frmEmployeeMenu.getContentPane().add(btnUpdate);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String id = "-";
				if (caridText.getText().length() > 0) {
					id = caridText.getText();
				}
				String isExisting = "";
				try {
					isExisting = clientRequest.sendMessage("employee" + " " + "car" + " " + "getCar" + " " + id);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (Utilities.isNumeric(id) && !(isExisting.charAt(0) == '0')) {
					try {
						clientRequest.sendMessage("employee" + " " + "car " + "deleteCar " + id);
						refreshTextPane(textPaneCar, Utilities.transformString(
								clientRequest.sendMessage("employee" + " " + "car" + " " + "getAllCars")));
						refreshTextPane(textPaneContract, Utilities.transformString(
								clientRequest.sendMessage("employee" + " " + "contract" + " " + "getAllContracts")));
						caridText.setText(null);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					caridText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid id.", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnDelete.setBounds(267, 186, 89, 23);
		frmEmployeeMenu.getContentPane().add(btnDelete);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 219, 464, 2);
		frmEmployeeMenu.getContentPane().add(separator);

		JLabel lblContracts = new JLabel("Contracts");
		lblContracts.setBounds(220, 227, 79, 14);
		frmEmployeeMenu.getContentPane().add(lblContracts);

		JLabel lblId = new JLabel("Id");
		lblId.setBounds(10, 253, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblId);

		contractidText = new JTextField();
		contractidText.setBounds(66, 250, 86, 20);
		frmEmployeeMenu.getContentPane().add(contractidText);
		contractidText.setColumns(10);

		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(10, 278, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblDate);

		dateText = new JTextField();
		dateText.setBounds(66, 275, 86, 20);
		frmEmployeeMenu.getContentPane().add(dateText);
		dateText.setColumns(10);

		lblCarId_1 = new JLabel("Car id");
		lblCarId_1.setBounds(10, 303, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblCarId_1);

		caridContractText = new JTextField();
		caridContractText.setBounds(66, 300, 86, 20);
		frmEmployeeMenu.getContentPane().add(caridContractText);
		caridContractText.setColumns(10);

		lblClientId = new JLabel("Client id");
		lblClientId.setBounds(10, 328, 55, 14);
		frmEmployeeMenu.getContentPane().add(lblClientId);

		clientidText = new JTextField();
		clientidText.setBounds(66, 325, 86, 20);
		frmEmployeeMenu.getContentPane().add(clientidText);
		clientidText.setColumns(10);

		scrollPaneContracts = new ScrollPane();
		scrollPaneContracts.setBounds(177, 247, 139, 194);
		textPaneContract = new JTextPane();
		scrollPaneContracts.add(textPaneContract);
		frmEmployeeMenu.getContentPane().add(scrollPaneContracts);

		try {
			refreshTextPane(textPaneContract, Utilities.transformString(
					clientRequest.sendMessage("employee" + " " + "contract" + " " + "getAllContracts")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		scrollPaneClients = new ScrollPane();
		scrollPaneClients.setBounds(340, 247, 134, 194);
		textPaneClient = new JTextPane();
		textPaneClient.setEditable(false);
		scrollPaneClients.add(textPaneClient);
		frmEmployeeMenu.getContentPane().add(scrollPaneClients);

		try {
			refreshTextPane(textPaneClient, Utilities
					.transformString(clientRequest.sendMessage("employee" + " " + "client" + " " + "getAllClients")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		lblClients = new JLabel("Clients");
		lblClients.setBounds(395, 227, 46, 14);
		frmEmployeeMenu.getContentPane().add(lblClients);

		addContractButton = new JButton("Add");
		addContractButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String carid = "-";
				if (caridContractText.getText().length() > 0) {
					carid = caridContractText.getText();
				}
				String clientid = "-";
				if (clientidText.getText().length() > 0) {
					clientid = clientidText.getText();
				}

				String isExistingCar = "";
				try {
					isExistingCar = clientRequest.sendMessage("employee" + " " + "car" + " " + "getCar" + " " + carid);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				String isExistingClient = "";
				try {
					isExistingClient = clientRequest
							.sendMessage("employee" + " " + "client" + " " + "getClient" + " " + clientid);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				String[] car= isExistingCar.split("\\s+");
				
				LocalDate localDate = LocalDate.now();
				String currentDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate);
				System.out.println(car[6]);
				if (Utilities.isNumeric(carid) && isExistingCar.charAt(0) != '0' && Utilities.isNumeric(clientid)
						&& isExistingClient.charAt(0) != '0') {
					if(car[6].equals("available")) {
					try {
						clientRequest.sendMessage("employee" + " " + "contract " + "addContract " + currentDate + " "
								+ carid + " " + clientid);
						refreshTextPane(textPaneContract, Utilities.transformString(
								clientRequest.sendMessage("employee" + " " + "contract" + " " + "getAllContracts")));
						refreshTextPane(textPaneCar, Utilities.transformString(
								clientRequest.sendMessage("employee" + " " + "car" + " " + "getAllCars")));
						caridContractText.setText(null);
						clientidText.setText(null);
					} catch (IOException e) {
						e.printStackTrace();
					}
					}else {
						caridContractText.setText(null);
						JOptionPane.showMessageDialog(null, "Unavailable car.", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
				} else {
					caridContractText.setText(null);
					clientidText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid car id / client id.", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		addContractButton.setBounds(38, 353, 89, 23);
		frmEmployeeMenu.getContentPane().add(addContractButton);

		btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String contractId = "-";
				if (contractidText.getText().length() > 0) {
					contractId = contractidText.getText();
				}
				String carid = "-";
				if (caridContractText.getText().length() > 0) {
					carid = caridContractText.getText();
				}
				String clientid = "-";
				if (clientidText.getText().length() > 0) {
					clientid = clientidText.getText();
				}
				String date = "-";
				if (dateText.getText().length() > 0) {
					date = dateText.getText();
				}

				String isExistingCar = "";
				try {
					isExistingCar = clientRequest.sendMessage("employee" + " " + "car" + " " + "getCar" + " " + carid);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				String isExistingClient = "";
				try {
					isExistingClient = clientRequest
							.sendMessage("employee" + " " + "client" + " " + "getClient" + " " + clientid);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				String isExistingContract = "";
				try {
					isExistingContract = clientRequest
							.sendMessage("employee" + " " + "contract" + " " + "getContract" + " " + contractId);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				if (Utilities.isNumeric(contractId) && isExistingContract.charAt(0) != '0') {
					if (Utilities.isNumeric(carid) && isExistingCar.charAt(0) != '0') {
						if (Utilities.isNumeric(clientid) && isExistingClient.charAt(0) != '0') {
							if(Utilities.checkDate(date)) {
							try {
								clientRequest.sendMessage("employee" + " " + "contract " + "updateContract "
										+ contractId + " " + date + " " + carid + " " + clientid);
								refreshTextPane(textPaneContract, Utilities.transformString(clientRequest
										.sendMessage("employee" + " " + "contract" + " " + "getAllContracts")));
								refreshTextPane(textPaneCar, Utilities.transformString(
										clientRequest.sendMessage("employee" + " " + "car" + " " + "getAllCars")));
								caridContractText.setText(null);
								clientidText.setText(null);
								dateText.setText(null);
								contractidText.setText(null);
							} catch (IOException e) {
								e.printStackTrace();
							}
							}else {
								JOptionPane.showMessageDialog(null, "Invalid date.", "WARNING",
										JOptionPane.WARNING_MESSAGE);
								dateText.setText(null);
							}
						} else {
							JOptionPane.showMessageDialog(null, "Invalid client id.", "WARNING",
									JOptionPane.WARNING_MESSAGE);
							caridContractText.setText(null);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Invalid car id.", "WARNING", JOptionPane.WARNING_MESSAGE);
						caridContractText.setText(null);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Invalid contract id.", "WARNING", JOptionPane.WARNING_MESSAGE);
					contractidText.setText(null);
				}
			}
		});
		btnUpdate_1.setBounds(38, 387, 89, 23);
		frmEmployeeMenu.getContentPane().add(btnUpdate_1);

		deleteContractButton = new JButton("Delete");
		deleteContractButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String contractId = "-";
				if (contractidText.getText().length() > 0) {
					contractId = contractidText.getText();
				}
				contractidText.setText(null);
				
				String isExistingContract = "";
				try {
					isExistingContract = clientRequest
							.sendMessage("employee" + " " + "contract" + " " + "getContract" + " " + contractId);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				if (Utilities.isNumeric(contractId) && isExistingContract.charAt(0) != '0') {
				
				try {
					clientRequest.sendMessage("employee" + " " + "contract " + "deleteContract " + contractId);
					refreshTextPane(textPaneContract, Utilities.transformString(
							clientRequest.sendMessage("employee" + " " + "contract" + " " + "getAllContracts")));
					refreshTextPane(textPaneCar, Utilities
							.transformString(clientRequest.sendMessage("employee" + " " + "car" + " " + "getAllCars")));

				} catch (IOException e1) {
					e1.printStackTrace();
				}
				}else {
					JOptionPane.showMessageDialog(null, "Invalid contract id.", "WARNING", JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		deleteContractButton.setBounds(38, 419, 89, 23);
		frmEmployeeMenu.getContentPane().add(deleteContractButton);

		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					refreshTextPane(textPaneCar, Utilities
							.transformString(clientRequest.sendMessage("employee" + " " + "car" + " " + "getAllCars")));
					refreshTextPane(textPaneContract, Utilities.transformString(
							clientRequest.sendMessage("employee" + " " + "contract" + " " + "getAllContracts")));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnRefresh.setBounds(385, 186, 89, 23);
		frmEmployeeMenu.getContentPane().add(btnRefresh);

		lblAvailability = new JLabel("Availability");
		lblAvailability.setBounds(10, 161, 65, 14);
		frmEmployeeMenu.getContentPane().add(lblAvailability);

		availabilityText = new JTextField();
		availabilityText.setBounds(85, 161, 86, 20);
		frmEmployeeMenu.getContentPane().add(availabilityText);
		availabilityText.setColumns(10);

		this.frmEmployeeMenu.setVisible(true);
	}
}
