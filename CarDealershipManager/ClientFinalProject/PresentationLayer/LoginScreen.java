package PresentationLayer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;

public class LoginScreen {
	private static ClientRequest clientRequest;
	private JFrame frmLogin;
	private JTextField userName;
	private JPasswordField password;


	public static void main(String[] args) throws UnknownHostException, IOException {
		clientRequest = new ClientRequest();
		clientRequest.startConnection("127.0.0.1", 4444);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginScreen window = new LoginScreen();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public LoginScreen() {
		initialize();
	}


	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setResizable(false);
		frmLogin.setTitle("Login");
		frmLogin.setBounds(100, 100, 414, 322);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(63, 63, 71, 24);
		frmLogin.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(63, 129, 71, 14);
		frmLogin.getContentPane().add(lblPassword);

		userName = new JTextField();
		userName.setBounds(163, 65, 128, 20);
		frmLogin.getContentPane().add(userName);
		userName.setColumns(10);

		password = new JPasswordField();
		password.setBounds(163, 129, 128, 20);
		frmLogin.getContentPane().add(password);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nume = userName.getText();
				char[] parola = password.getPassword();
				try {
					String resp1 = clientRequest.sendMessage("checkPassword "+new String(parola)+ " " + nume);
					System.out.println(resp1);
					if(resp1.equals("invalid user")) {
						JOptionPane.showMessageDialog(null, "Invalid user.", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}else {
						if(resp1.equals("valid client")) {
							@SuppressWarnings("unused")
							ClientScreen clientScreen = new ClientScreen(clientRequest);
							//clientScreen
						}else {
							if(resp1.equals("valid employee")) {
								//employeeScreen
								@SuppressWarnings("unused")
								EmployeeScreen employeeScreen = new EmployeeScreen(clientRequest);
							}else {
								if(resp1.equals("valid admin")) {
									@SuppressWarnings("unused")
									AdminScreen adminScreen = new AdminScreen(clientRequest);
									//adminScreen
								}
							}
						}
					}
					
					
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		btnLogin.setBounds(20, 218, 89, 23);
		frmLogin.getContentPane().add(btnLogin);

		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				userName.setText(null);
				password.setText(null);
			}
		});
		btnReset.setBounds(163, 218, 89, 23);
		frmLogin.getContentPane().add(btnReset);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					clientRequest.sendMessage(".");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				frmLogin.dispose();
			}
		});
		frmLogin.addWindowListener(new WindowAdapter()
		{
		    public void windowClosing(WindowEvent e)
		    {
		    	try {
					clientRequest.sendMessage(".");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		    	frmLogin.dispose();
		    }
		});
		btnExit.setBounds(309, 218, 89, 23);
		frmLogin.getContentPane().add(btnExit);
		
	}
}
