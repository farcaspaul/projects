package PresentationLayer;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import utilities.Utilities;

import javax.swing.JSeparator;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class ClientScreen {

	private JFrame frmClientMenu;
	private ClientRequest clientRequest;
	private JTextField ccFromText;
	private JTextField ccToText;
	private JTextField priceFromText;
	private JTextField priceToText;
	private JTextField fuelText;
	private JTextField brandText;
	private JTextField modelText;
	private JSeparator separator;
	private JSeparator separator_1;
	private JSeparator separator_2;
	private JTextPane carListText;

	public ClientScreen(ClientRequest clientRequest) {
		this.clientRequest = clientRequest;
		initialize();
	}

	private void refreshCarList(String carList) {
		carListText.setText(carList);
	}

	private void initialize() {
		frmClientMenu = new JFrame();
		frmClientMenu.setResizable(false);
		frmClientMenu.setTitle("Client Menu");
		frmClientMenu.setBounds(100, 100, 468, 310);
		frmClientMenu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmClientMenu.getContentPane().setLayout(null);

		JLabel lblBrand = new JLabel("Brand");
		lblBrand.setBounds(10, 11, 46, 14);
		frmClientMenu.getContentPane().add(lblBrand);

		JLabel lblModel = new JLabel("Model");
		lblModel.setBounds(10, 38, 46, 14);
		frmClientMenu.getContentPane().add(lblModel);

		JLabel lblCubicCapacity = new JLabel("Cubic Capacity");
		lblCubicCapacity.setBounds(11, 80, 85, 14);
		frmClientMenu.getContentPane().add(lblCubicCapacity);

		ccFromText = new JTextField();
		ccFromText.setBounds(10, 106, 86, 20);
		frmClientMenu.getContentPane().add(ccFromText);
		ccFromText.setColumns(10);

		ccToText = new JTextField();
		ccToText.setBounds(106, 106, 86, 20);
		frmClientMenu.getContentPane().add(ccToText);
		ccToText.setColumns(10);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(10, 147, 46, 14);
		frmClientMenu.getContentPane().add(lblPrice);

		priceFromText = new JTextField();
		priceFromText.setBounds(10, 172, 86, 20);
		frmClientMenu.getContentPane().add(priceFromText);
		priceFromText.setColumns(10);

		priceToText = new JTextField();
		priceToText.setText("");
		priceToText.setBounds(106, 172, 86, 20);
		frmClientMenu.getContentPane().add(priceToText);
		priceToText.setColumns(10);

		JLabel lblFuel = new JLabel("Fuel");
		lblFuel.setBounds(10, 220, 46, 14);
		frmClientMenu.getContentPane().add(lblFuel);

		fuelText = new JTextField();
		fuelText.setBounds(106, 217, 86, 20);
		frmClientMenu.getContentPane().add(fuelText);
		fuelText.setColumns(10);

		brandText = new JTextField();
		brandText.setBounds(105, 8, 86, 20);
		brandText.setText(null);
		frmClientMenu.getContentPane().add(brandText);
		brandText.setColumns(10);

		modelText = new JTextField();
		modelText.setBounds(105, 35, 86, 20);
		frmClientMenu.getContentPane().add(modelText);
		modelText.setColumns(10);

		separator = new JSeparator();
		separator.setBounds(10, 63, 197, 2);
		frmClientMenu.getContentPane().add(separator);

		separator_1 = new JSeparator();
		separator_1.setBounds(10, 134, 197, 2);
		frmClientMenu.getContentPane().add(separator_1);

		separator_2 = new JSeparator();
		separator_2.setBounds(10, 201, 197, 2);
		frmClientMenu.getContentPane().add(separator_2);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String brand = "-";
		//		System.out.println("LUNGIME= " +brandText.getText().length());
				if (brandText.getText().length()>0) {
					brand = brandText.getText();
				}
				String model = "-";
				if (modelText.getText().length()>0) {
					model = modelText.getText();
				}
				String ccFrom = "-";
				if (ccFromText.getText().length()>0) {
					ccFrom = ccFromText.getText();
				}
				String ccTo = "-";
				if (ccToText.getText().length()>0) {
					ccTo = ccToText.getText();
				}

				String priceFrom = "-";
				if (priceFromText.getText().length()>0) {
					priceFrom = priceFromText.getText();
				}

				String priceTo = "-";
				if (priceToText.getText().length()>0) {
					priceTo = priceToText.getText();
				}

				String fuel = "-";
				if (fuelText.getText().length()>0) {
					fuel = fuelText.getText();
				}
				if(((!(ccFrom.equals("-"))&&Utilities.isNumeric(ccFrom)) ||  ccFrom.equals("-")) && ((!(ccTo.equals("-"))&& Utilities.isNumeric(ccTo)) ||  ccTo.equals("-"))) {
					if(((!(priceFrom.equals("-"))&&Utilities.isNumeric(priceFrom)) || priceFrom.equals("-") ) && ((!(priceTo.equals("-"))&& Utilities.isNumeric(priceTo)) || priceTo.equals("-"))) {
				try {
					// String brand, String model, String ccFrom, String ccTo, String priceFrom,
					// String priceTo, String fuel
					String result = clientRequest.sendMessage("client" + " " + "filterCars" + " " + brand + " " + model
							+ " " + ccFrom + " " + ccTo + " " + priceFrom + " " + priceTo + " " + fuel);
					refreshCarList(Utilities.transformString(result));
				} catch (IOException e) {
					e.printStackTrace();
				}
					}else {
						JOptionPane.showMessageDialog(null, "Invalid price input.", "WARNING", JOptionPane.WARNING_MESSAGE);
						priceToText.setText(null);
						priceFromText.setText(null);
					}
				}else {
					JOptionPane.showMessageDialog(null, "Invalid cc input.", "WARNING", JOptionPane.WARNING_MESSAGE);
					ccToText.setText(null);
					ccFromText.setText(null);
				}
			}
		});
		btnSearch.setBounds(7, 247, 89, 23);
		frmClientMenu.getContentPane().add(btnSearch);

		JButton btnRecommend = new JButton("Recommend");
		btnRecommend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String brand = "-";
		//		System.out.println("LUNGIME= " +brandText.getText().length());
				if (brandText.getText().length()>0) {
					brand = brandText.getText();
					brand = brand.replaceAll(" ", "_");
				}
				String model = "-";
				if (modelText.getText().length()>0) {
					model = modelText.getText();
					model = model.replaceAll(" ", "_");
				}
				String ccFrom = "-";
				if (ccFromText.getText().length()>0) {
					ccFrom = ccFromText.getText();
				}
				String ccTo = "-";
				if (ccToText.getText().length()>0) {
					ccTo = ccToText.getText();
				}

				String priceFrom = "-";
				if (priceFromText.getText().length()>0) {
					priceFrom = priceFromText.getText();
				}

				String priceTo = "-";
				if (priceToText.getText().length()>0) {
					priceTo = priceToText.getText();
				}

				String fuel = "-";
				if (fuelText.getText().length()>0) {
					fuel = fuelText.getText();
				}

				try {
					// String brand, String model, String ccFrom, String ccTo, String priceFrom,
					// String priceTo, String fuel
					String result = clientRequest.sendMessage("client" + " " + "recommendCar" + " " + brand + " " + model
							+ " " + ccFrom + " " + ccTo + " " + priceFrom + " " + priceTo + " " + fuel);
				//	System.out.println(Utilities.transformString(result));
				//	refreshCarList(Utilities.transformString(result));
					JOptionPane.showMessageDialog(null, Utilities.transformString(result), "Recommandation",
							JOptionPane.PLAIN_MESSAGE);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnRecommend.setFont(new Font("Aharoni", Font.PLAIN, 11));
		btnRecommend.setForeground(Color.RED);
		btnRecommend.setBounds(106, 247, 99, 23);
		frmClientMenu.getContentPane().add(btnRecommend);

		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setBounds(224, 11, 228, 260);
		carListText = new JTextPane();
		try {
			carListText.setText(Utilities.transformString(clientRequest.sendMessage("client" + " " + "filterCars" + " " + "-" + " " + "-"
								+ " " + "-" + " " + "-" + " " + "-" + " " + "-" + " " + "-")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		carListText.setEditable(false);
		scrollPane.add(carListText);
		frmClientMenu.getContentPane().add(scrollPane);

		this.frmClientMenu.setVisible(true);
	}
}
