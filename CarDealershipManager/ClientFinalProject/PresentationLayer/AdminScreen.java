package PresentationLayer;

import javax.swing.JFrame;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import utilities.Utilities;

import java.awt.Color;
import javax.swing.JPasswordField;
import java.awt.ScrollPane;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminScreen {

	private JFrame frmAdminMenu;
	private ClientRequest clientRequest;
	private JTextField idClientText;
	private JTextField clientNameText;
	private JTextField clientAddressText;
	private JTextField cnpText;
	private JPasswordField clientPasswordText;
	private JTextField idEmployeeText;
	private JTextField employeeNameText;
	private JTextField employeeAddressText;
	private JPasswordField employeePasswordText;
	private JTextPane textPaneClient, textPaneEmployee;
	private JTextField startDateText;
	private JTextField endDateText;

	public AdminScreen(ClientRequest clientRequest) {
		this.clientRequest = clientRequest;
		initialize();
	}

	public void refreshTextPane(JTextPane textPane, String input) {
		textPane.setText(input);
	}

	private void initialize() {
		frmAdminMenu = new JFrame();
		frmAdminMenu.setTitle("Admin Menu");
		frmAdminMenu.setBounds(100, 100, 624, 443);
		frmAdminMenu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAdminMenu.getContentPane().setLayout(null);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 200, 491, 2);
		frmAdminMenu.getContentPane().add(separator);

		JLabel lblClients = new JLabel("Clients");
		lblClients.setBounds(10, 11, 46, 14);
		frmAdminMenu.getContentPane().add(lblClients);

		JLabel lblEmployees = new JLabel("Employees");
		lblEmployees.setBounds(10, 213, 68, 14);
		frmAdminMenu.getContentPane().add(lblEmployees);

		JLabel lblId = new JLabel("id");
		lblId.setBounds(10, 36, 46, 14);
		frmAdminMenu.getContentPane().add(lblId);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 61, 46, 14);
		frmAdminMenu.getContentPane().add(lblName);

		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(10, 91, 68, 14);
		frmAdminMenu.getContentPane().add(lblAddress);

		idClientText = new JTextField();
		idClientText.setBounds(79, 33, 86, 20);
		frmAdminMenu.getContentPane().add(idClientText);
		idClientText.setColumns(10);

		clientNameText = new JTextField();
		clientNameText.setBounds(79, 58, 86, 20);
		frmAdminMenu.getContentPane().add(clientNameText);
		clientNameText.setColumns(10);

		clientAddressText = new JTextField();
		clientAddressText.setBounds(79, 84, 86, 20);
		frmAdminMenu.getContentPane().add(clientAddressText);
		clientAddressText.setColumns(10);

		JLabel lblCnp = new JLabel("CNP");
		lblCnp.setBounds(10, 119, 46, 14);
		frmAdminMenu.getContentPane().add(lblCnp);

		cnpText = new JTextField();
		cnpText.setBounds(79, 113, 86, 20);
		frmAdminMenu.getContentPane().add(cnpText);
		cnpText.setColumns(10);

		JLabel lblNewLabel = new JLabel("Password");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBounds(10, 144, 68, 14);
		frmAdminMenu.getContentPane().add(lblNewLabel);

		clientPasswordText = new JPasswordField();
		clientPasswordText.setBounds(79, 141, 86, 20);
		frmAdminMenu.getContentPane().add(clientPasswordText);

		ScrollPane scrollPaneClient = new ScrollPane();
		scrollPaneClient.setBounds(208, 5, 390, 151);
		textPaneClient = new JTextPane();
		try {
			refreshTextPane(textPaneClient, Utilities
					.transformString(clientRequest.sendMessage("admin" + " " + "client" + " " + "getAllClients")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		textPaneClient.setEditable(false);
		scrollPaneClient.add(textPaneClient);
		frmAdminMenu.getContentPane().add(scrollPaneClient);

		//

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = "-";
				if (clientNameText.getText().length() > 0) {
					name = clientNameText.getText();
					name = name.replaceAll(" ", "_");
				}

				String address = "-";
				if (clientAddressText.getText().length() > 0) {
					address = clientAddressText.getText();
					address = address.replaceAll(" ", "_");
				}
				String cnp = "-";
				if (cnpText.getText().length() > 0) {
					cnp = cnpText.getText();
				}
				String password = "-";
				if (new String(clientPasswordText.getPassword()).length() > 0) {
					password = new String(clientPasswordText.getPassword());
				}

				if (name.length() > 2 && Utilities.isAlpha(name)) {
					if (address.length() > 2 && Utilities.isAlpha(address)) {
						if (cnp.length() == 13 && Utilities.isNumeric(cnp)) {
							if (password.length() > 2) {
								try {
									clientRequest.sendMessage("admin" + " " + "client" + " " + "addClient" + " " + name
											+ " " + cnp + " " + password + " " + address);
									refreshTextPane(textPaneClient, Utilities.transformString(clientRequest
											.sendMessage("admin" + " " + "client" + " " + "getAllClients")));
									clientNameText.setText(null);
									clientAddressText.setText(null);
									cnpText.setText(null);
									clientPasswordText.setText(null);
								} catch (IOException e) {
									e.printStackTrace();
								}
							} else {
								clientPasswordText.setText(null);
								JOptionPane.showMessageDialog(null, "Password too short.", "WARNING",
										JOptionPane.WARNING_MESSAGE);
							}

						} else {
							cnpText.setText(null);
							JOptionPane.showMessageDialog(null, "Invalid CNP.", "WARNING", JOptionPane.WARNING_MESSAGE);
						}
					} else {
						clientAddressText.setText(null);
						JOptionPane.showMessageDialog(null, "Invalid address.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}

				} else {
					clientNameText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid name.", "WARNING", JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		btnAdd.setBounds(10, 172, 89, 23);
		frmAdminMenu.getContentPane().add(btnAdd);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = "-";
				if (clientNameText.getText().length() > 0) {
					name = clientNameText.getText();
					name = name.replaceAll(" ", "_");
				}
				String address = "-";
				if (clientAddressText.getText().length() > 0) {
					address = clientAddressText.getText();
					address = address.replaceAll(" ", "_");
				}
				String cnp = "-";
				if (cnpText.getText().length() > 0) {
					cnp = cnpText.getText();
				}
				String id = "-";
				if (idClientText.getText().length() > 0) {
					id = idClientText.getText();
				}
				String isExisting = "";
				try {
					isExisting = clientRequest.sendMessage("admin" + " " + "client" + " " + "getClient" + " " + id);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				if (name.length() > 2 && Utilities.isAlpha(name)) {
					if (address.length() > 2 && Utilities.isAlpha(address)) {
						if (cnp.length() == 13 && Utilities.isNumeric(cnp)) {
							if (Utilities.isNumeric(id) && !(isExisting.charAt(0) == '0')) {
								try {
									clientRequest.sendMessage("admin" + " " + "client" + " " + "updateClient" + " " + id
											+ " " + name + " " + cnp + " " + address);
									refreshTextPane(textPaneClient, Utilities.transformString(clientRequest
											.sendMessage("admin" + " " + "client" + " " + "getAllClients")));
									idClientText.setText(null);
									clientNameText.setText(null);
									clientAddressText.setText(null);
									cnpText.setText(null);
								} catch (IOException e) {
									e.printStackTrace();
								}
							} else {
								clientPasswordText.setText(null);
								JOptionPane.showMessageDialog(null, "Invalid or inexisting ID.", "WARNING",
										JOptionPane.WARNING_MESSAGE);
							}

						} else {
							cnpText.setText(null);
							JOptionPane.showMessageDialog(null, "Invalid CNP.", "WARNING", JOptionPane.WARNING_MESSAGE);
						}
					} else {
						clientAddressText.setText(null);
						JOptionPane.showMessageDialog(null, "Invalid address.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}

				} else {
					clientNameText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid name.", "WARNING", JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		btnUpdate.setBounds(109, 172, 89, 23);
		frmAdminMenu.getContentPane().add(btnUpdate);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = "-";
				if (idClientText.getText().length() > 0) {
					id = idClientText.getText();
				}
				idClientText.setText(null);
				String isExisting = "";
				try {
					isExisting = clientRequest.sendMessage("admin" + " " + "client" + " " + "getClient" + " " + id);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (Utilities.isNumeric(id) && !(isExisting.charAt(0) == '0')) {
					try {
						clientRequest.sendMessage("admin" + " " + "client" + " " + "deleteClient" + " " + id);
						refreshTextPane(textPaneClient, Utilities.transformString(
								clientRequest.sendMessage("admin" + " " + "client" + " " + "getAllClients")));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else {
					clientPasswordText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid or inexisting ID.", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnDelete.setBounds(208, 172, 89, 23);
		frmAdminMenu.getContentPane().add(btnDelete);

		JLabel lblId_1 = new JLabel("id");
		lblId_1.setBounds(10, 241, 46, 14);
		frmAdminMenu.getContentPane().add(lblId_1);

		idEmployeeText = new JTextField();
		idEmployeeText.setBounds(79, 238, 86, 20);
		frmAdminMenu.getContentPane().add(idEmployeeText);
		idEmployeeText.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Name");
		lblNewLabel_1.setBounds(10, 272, 46, 14);
		frmAdminMenu.getContentPane().add(lblNewLabel_1);

		employeeNameText = new JTextField();
		employeeNameText.setBounds(79, 269, 86, 20);
		frmAdminMenu.getContentPane().add(employeeNameText);
		employeeNameText.setColumns(10);

		employeeAddressText = new JTextField();
		employeeAddressText.setBounds(79, 300, 86, 20);
		frmAdminMenu.getContentPane().add(employeeAddressText);
		employeeAddressText.setColumns(10);

		JLabel lblAddress_1 = new JLabel("Address");
		lblAddress_1.setBounds(10, 303, 55, 14);
		frmAdminMenu.getContentPane().add(lblAddress_1);

		employeePasswordText = new JPasswordField();
		employeePasswordText.setBounds(79, 332, 86, 20);
		frmAdminMenu.getContentPane().add(employeePasswordText);

		JLabel label = new JLabel("Password");
		label.setForeground(Color.BLUE);
		label.setBounds(10, 335, 68, 14);
		frmAdminMenu.getContentPane().add(label);

		JButton button = new JButton("Add");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = "-";
				if (employeeNameText.getText().length() > 0) {
					name = employeeNameText.getText();
					name = name.replaceAll(" ", "_");
				}
				String address = "-";
				if (employeeAddressText.getText().length() > 0) {
					address = employeeAddressText.getText();
					address = address.replaceAll(" ", "_");
				}
				String password = "-";
				if (new String(employeePasswordText.getPassword()).length() > 0) {
					password = new String(employeePasswordText.getPassword());
				}

				if (name.length() > 2 && Utilities.isAlpha(name)) {
					if (address.length() > 2 && Utilities.isAlpha(address)) {
						if (password.length() > 2) {
							try {
								clientRequest.sendMessage("admin" + " " + "employee" + " " + "addEmployee" + " " + name
										+ " " + address + " " + password);
								refreshTextPane(textPaneEmployee, Utilities.transformString(clientRequest
										.sendMessage("admin" + " " + "employee" + " " + "getAllEmployees")));
								employeePasswordText.setText(null);
								employeeAddressText.setText(null);
								employeeNameText.setText(null);
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						} else {
							employeePasswordText.setText(null);
							JOptionPane.showMessageDialog(null, "Password too short.", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}

					} else {
						employeeAddressText.setText(null);
						JOptionPane.showMessageDialog(null, "Invalid address.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
				} else {
					employeeNameText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid name.", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		button.setBounds(10, 370, 89, 23);
		frmAdminMenu.getContentPane().add(button);

		JButton button_1 = new JButton("Update");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = "-";
				if (employeeNameText.getText().length() > 0) {
					name = employeeNameText.getText();
					name = name.replaceAll(" ", "_");
				}
				String address = "-";
				if (employeeAddressText.getText().length() > 0) {
					address = employeeAddressText.getText();
					address = address.replaceAll(" ", "_");
				}

				String id = "-";
				if (idEmployeeText.getText().length() > 0) {
					id = idEmployeeText.getText();
				}
				employeeNameText.setText(null);
				employeeAddressText.setText(null);
				employeePasswordText.setText(null);
				String isExisting = "";
				try {
					isExisting = clientRequest.sendMessage("admin" + " " + "employee" + " " + "getEmployee" + " " + id);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (Utilities.isNumeric(id) && !(isExisting.charAt(0) == '0')) {
					if (name.length() > 2 && Utilities.isAlpha(name)) {
						if (address.length() > 2 && Utilities.isAlpha(address)) {
							try {
								clientRequest.sendMessage("admin" + " " + "employee" + " " + "updateEmployee" + " " + id
										+ " " + name + " " + address);
								refreshTextPane(textPaneEmployee, Utilities.transformString(clientRequest
										.sendMessage("admin" + " " + "employee" + " " + "getAllEmployees")));
								idEmployeeText.setText(null);
								employeeAddressText.setText(null);
								employeeNameText.setText(null);
							} catch (IOException e1) {
								e1.printStackTrace();
							}

						} else {
							employeeAddressText.setText(null);
							JOptionPane.showMessageDialog(null, "Invalid address.", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					} else {
						employeeNameText.setText(null);
						JOptionPane.showMessageDialog(null, "Invalid name.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
				} else {
					idEmployeeText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid or inexisting ID.", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		button_1.setBounds(109, 370, 89, 23);
		frmAdminMenu.getContentPane().add(button_1);

		JButton button_2 = new JButton("Delete");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = "-";
				if (idEmployeeText.getText().length() > 0) {
					id = idEmployeeText.getText();
				}
				idEmployeeText.setText(null);
				String isExisting = "";
				try {
					isExisting = clientRequest.sendMessage("admin" + " " + "employee" + " " + "getEmployee" + " " + id);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (Utilities.isNumeric(id) && !(isExisting.charAt(0) == '0')) {

					try {
						clientRequest.sendMessage("admin" + " " + "employee" + " " + "deleteEmployee" + " " + id);
						refreshTextPane(textPaneEmployee, Utilities.transformString(
								clientRequest.sendMessage("admin" + " " + "employee" + " " + "getAllEmployees")));
						idEmployeeText.setText(null);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else {
					idEmployeeText.setText(null);
					JOptionPane.showMessageDialog(null, "Invalid or inexisting ID.", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		button_2.setBounds(208, 370, 89, 23);
		frmAdminMenu.getContentPane().add(button_2);

		ScrollPane scrollPaneEmployee = new ScrollPane();
		scrollPaneEmployee.setBounds(208, 206, 390, 151);
		textPaneEmployee = new JTextPane();
		try {
			refreshTextPane(textPaneEmployee, Utilities
					.transformString(clientRequest.sendMessage("admin" + " " + "employee" + " " + "getAllEmployees")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		textPaneEmployee.setEditable(false);
		scrollPaneEmployee.add(textPaneEmployee);
		frmAdminMenu.getContentPane().add(scrollPaneEmployee);

		JButton reportButton = new JButton("Report");
		reportButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String startDate = "-";

				String endDate = "-";

				if (startDateText.getText().length() > 0) {
					if (Utilities.checkDate(startDateText.getText())) {
						startDate = startDateText.getText();
						if (endDateText.getText().length() > 0) {
							if (Utilities.checkDate(endDateText.getText())) {
								endDate = endDateText.getText();
								try {
									String results = clientRequest.sendMessage("employee" + " " + "car" + " " + "getInterestReport" + " "
											+ startDate + " " + endDate);
									File file = new File ("C:/Users/Paul/Desktop/file.txt");
								    PrintWriter printWriter = new PrintWriter (file);
								    String[] resultsForPrinting = results.split("newLine");
								    for(int i = 0 ; i < resultsForPrinting.length; i++) {
								    	printWriter.println ("Locul " + (i+1)+": id "+resultsForPrinting[i]);
								    }
								    
								    printWriter.close ();
									startDateText.setText(null);
									endDateText.setText(null);
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							} else {
								endDateText.setText(null);
								JOptionPane.showMessageDialog(null, "Invalid end date.", "WARNING",
										JOptionPane.WARNING_MESSAGE);
							}
						}
						} else {
							startDateText.setText(null);
							JOptionPane.showMessageDialog(null, "Invalid start date.", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					}
				}
			
		});
		reportButton.setForeground(Color.RED);
		reportButton.setBounds(307, 370, 89, 23);
		frmAdminMenu.getContentPane().add(reportButton);

		startDateText = new JTextField();
		startDateText.setBounds(406, 371, 86, 20);
		frmAdminMenu.getContentPane().add(startDateText);
		startDateText.setColumns(10);

		endDateText = new JTextField();
		endDateText.setBounds(512, 371, 86, 20);
		frmAdminMenu.getContentPane().add(endDateText);
		endDateText.setColumns(10);

		JLabel label_1 = new JLabel("-");
		label_1.setBounds(500, 374, 46, 14);
		frmAdminMenu.getContentPane().add(label_1);

		this.frmAdminMenu.setVisible(true);
	}
}
