verificare_imparitate macro
local le_loop,final,hopa,gata
initializare
lea edi,info
le_loop:
mov al,[info+ebx]
cmp al,0
je final
inc ebx
jmp le_loop

final:
mov eax,0
mov eax,ebx
mov ebx,2
mov edx,0
div ebx
cmp edx,0
je hopa
jmp gata
hopa:
gresit

gata:

endm verificare_imparitate

verificare_date macro mult
local hopa,final,loop_ul
initializare
verificare_imparitate
initializare
mov esi,mult
loop_ul:

	mov cl,[esi+eax]			; primul element	
	mov dl,[esi+eax+1]			; urmatorul element
	
	 cmp edx,0					; verific daca nu cumva suntem la ultimul element
	 je final					; sar la final daca suntem
	cmp edx,32					; verific daca urmatorul element este corect, adica spatiu gol
	jne hopa					; sar la textul erorii daca nu
		
	inc eax						;
	inc eax						; incrementez de 2 ori ca sa sar peste spatiile goale
cmp ecx,0						; daca nu am ajuns la sfarsit, reluam bucla
jne loop_ul
jmp final

hopa:
gresit							; macro-ul erorii

final:

endm verificare_date


citire_date macro mult, dim
LOCAL le_loop,punct,hopa,final

	lea eax,info				; sirul dat de la tastatura
	mov esi,mult
	le_loop:
	mov edx,0

	mov dl,[eax+edi]
	mov [esi+ebx],dl			; salvez in multimea1 elementele
	
	inc ebx						
	inc edi						;  - - -- - incrementez o data si verific daca s-a ajuns la final
	mov dl,[eax+edi]
	inc edi						; - - -- - - incrementez a doua oara si daca nu s-a ajuns la final o sa sar peste spatiul gol :^)
	inc lungime_date			;
	inc lungime_date			; incrementez si contorul de lungime a datelor de 2 ori
	cmp dl,0					; verific daca s-a terminat sirul de date
	jne le_loop
	sub ebx,1
	mov [dim], ebx				; memorez dimensiunea sirului de info
initializare					; registrii -> 0
; verificare_date mult
endm citire_date



initializare macro
	mov eax,0
	mov ecx,0
	mov edx,0
	mov ebx,0
	mov edi,0
	mov esi,0
endm initializare


afisare macro mult,dim
local le_loop
initializare

	mov ebx, mult				; element 1 = [edx+0]
	mov eax,0					; contor = 0
	
	le_loop:
	push eax
	push ebx
	push [ebx+eax]
	push offset formatc_
	call printf
	add esp,8
	pop ebx
	pop eax
	mov ecx,eax					;
	inc eax				
	cmp ecx,dim					; verific daca nu am ajuns la ultimul element (contorul = lungimea multimii)
	jne le_loop
	
	initializare
endm afisare


linie_noua macro
	push edx
	push ecx
	push eax
	push ebx
	push 0AH
	push offset formatc
	call printf
	add esp,8
	pop ebx
	pop eax
	pop ecx
	pop edx

endm linie_noua


gresit macro				;afiseaza mesajul cu greseala introducerii sirului
linie_noua
push offset greseala
push offset formats
call printf
add esp,8

push offset greseala
push fisier
call fprintf
add esp,8
jmp PANIC
endm gresit


unicitate macro mult, dim
local le_loop,hopa,final,in_loop,afara
initializare
mov temp,0
mov esi, mult
mov al, [esi] 					;  [eax ]  =   primul element

mov edx,1
mov bl, [esi+edx]				; [ebx]    =   urmatorul element

mov edx, dim
mov cl,[esi+edx]	  			; [ecx] = ultimul element
mov edx,0
cmp dim,0
je final

le_loop:
 in_loop:
 		add edx,1
		mov bl,[esi+edx]
		cmp bl,al				; verific daca am gasit element egal
		je hopa					; sar la mesaj

		push edx

		add edx,temp			; temp indica de unde sa incep sa compar ca sa nu compar elementele deja parcurse
		
		cmp edx,dim				; verific daca am ajuns la sfarsit
		je afara				; sar la noua iteratie

		pop edx
		
		jmp in_loop
	afara:
	add esp,4
	inc esi
	mov al, [esi] 				; trec la urmatorul element
	inc temp					; incrementez contorul
	mov edx,0
	mov edx,temp

	
	cmp edx,dim					; verific daca nu am ajuns la sfarsit
	je final					; sar la sfarsit daca am ajuns
	mov bl,[esi]
	mov edx,0
	jmp le_loop
	
 
 hopa:

	push edx
	push ecx
	push eax
	push ebx
	push eax
	push offset duplicat
	call printf
	add esp,8
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push eax
	push offset duplicat
	push fisier
	call fprintf
	add esp,12
	pop edx
	pop ecx
	pop ebx
	pop eax
	linie_noua_fisier
	jmp panic					; panic este eticheta de la finalul programului

	final:
 push offset fara_duplicat
 call printf
 add esp,4
 
 	push eax
	push ebx
	push ecx
	push edx
	push offset fara_duplicat
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	linie_noua_fisier
endm unicitate 



apartenenta macro mult,dim
local le_loop,hopa,final,sarit
initializare
mov temp,0
mov esi, mult					; primul element
mov al,[esi]					;

le_loop:
cmp al,element_cautat			; compar cu elementul cautat
je hopa							; sar la mesaj daca sunt egale
mov ecx,temp					; verific daca nu am ajuns la final
cmp ecx,dim						;
je final						; sar la final daca am ajuns si afisez mesajul
inc temp						; incrementez contorul
inc esi							; trec la urmatorul numar
mov al,[esi]					;
jmp le_loop						; urmatoarea iteratie a buclei

hopa:
	push edx
	push ecx
	push eax
	push ebx
	push offset gasit
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset gasit
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	jmp sarit
final:
	push edx
	push ecx
	push eax
	push ebx
	push offset nu_tocmai
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset nu_tocmai
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	linie_noua_fisier
	
sarit:
endm apartenenta



reuniune macro
local le_loop,gata,le_loop2,afara_egal,afara_normal,final,fara_virgula
	push edx
	push ecx
	push eax
	push ebx
	push offset paranteza_d		; paranteza deschisa '('
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset paranteza_d		; paranteza deschisa '('
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
mov ok,0
lea esi,mult1
mov edi,0
mov ebx, dimensiune1
mov al,[esi+edi]
mov cl,[esi+ebx]				; ultimul element din sirul 1
le_loop:						; printez toate elementele multimii 1 fiindca ele sigur sunt unice
	push edx
	push ecx
	push eax
	push ebx
	push eax
	push offset formatc_
	call printf
	add esp,8
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push eax
	push offset formatc_
	push fisier
	call fprintf
	add esp,12
	pop edx
	pop ecx
	pop ebx
	pop eax
	

	
	cmp edi,dimensiune1
	je gata
	push edx
	push ecx
	push eax
	push ebx
	push offset virgula
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset virgula
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	inc edi
	mov al,[esi+edi] 
	jmp le_loop
	
	gata:	
	initializare
	
	push edx
	push ecx
	push eax
	push ebx
	push offset virgula
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset virgula
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
lea esi,mult1
mov al,[esi]
mov ebx, dimensiune1
mov cl,[esi+ebx]				; ultimul element din sirul 1

lea edi,mult2
mov ebx,dimensiune2
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov ebx,0
mov bl,[edi]

le_loop2:						; compar fiecare element din a doua multime cu fiecare element din prima multime

mov ebx, 0						; primul element din a doua multime
mov bl,[edi]					;

cmp bl,al						; compar cu primul element din prima multime
je afara_egal					; sar la eticheta in care trec la urmatorul element din a doua multime fara sa il printez
								; pe actualul deoarece el exista deja in prima multime

cmp al,cl						; verific daca nu am ajuns la finalul primei multimi
je afara_normal					; daca am ajuns in acest punct fara sa fi gasit duplicate sar la eticheta
								; in care printez elementul din multimea 2

inc esi							; trec la urmatorul element din prima multime spre a-l compara cu elementul din multimea 2
mov al,[esi]					;

jmp le_loop2


afara_egal:

lea esi,mult1					; primul element din sirul 1
mov al,[esi]					;
mov ebx, dimensiune1
mov cl,[esi+ebx]				; ultimul element din sirul 1



push edi						; salvez unde am ajuns in al doilea sir
lea edi,mult2
mov ebx,dimensiune2
mov dl,[edi+ebx]				; ultimul element din sirul 2 
					
pop edi							; pozitia in sir
mov bl,[edi]					;
cmp [edi],dl					; verific daca nu am ajuns la sfarsitul celui de-al doilea sir
je final
inc edi
jmp le_loop2

afara_normal:					; se printeaza elementul din a doua multime
cmp ok,0
je fara_virgula
	push edx
	push ecx
	push eax
	push ebx
	push offset virgula
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset virgula
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
fara_virgula:
mov ok,1
		push edx
		push ecx
		push eax
		push ebx
		push ebx
		push offset formatc_
		call printf
		add esp,8
		pop ebx
		pop eax
		pop ecx
		pop edx
		
	push eax
	push ebx
	push ecx
	push edx
	push ebx
	push offset formatc_
	push fisier
	call fprintf
	add esp,12
	pop edx
	pop ecx
	pop ebx
	pop eax
		
lea esi,mult1
mov al,[esi]
mov ebx, dimensiune1
mov cl,[esi+ebx]				; ultimul element din sirul 1



push edi
lea edi,mult2
mov ebx,dimensiune2
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov bl,[edi]
pop edi

cmp [edi],dl
je final
inc edi

jmp le_loop2

final:
	push edx
	push ecx
	push eax
	push ebx
	push offset paranteza_i				; paranteza inchisa ')'
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset paranteza_i
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	linie_noua_fisier
	mov ok,0
endm reuniune





intersectie macro
local le_loop,gata,le_loop2,afara_egal,afara_normal,final,fara
	push edx
	push ecx
	push eax
	push ebx
	push offset paranteza_d
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx

	push eax
	push ebx
	push ecx
	push edx
	push offset paranteza_d
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	initializare

lea esi,mult1
mov al,[esi]
mov ebx, dimensiune1
mov cl,[esi+ebx]				; ultimul element din sirul 1

mov ebx,0

lea edi,mult2
mov ebx,dimensiune2
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov ebx,0
mov bl,[edi]
mov ok,1

le_loop2:

mov ebx, 0
mov bl,[edi]

cmp bl,al
je afara_egal

cmp al,cl
je afara_normal

inc esi
push esi

mov al,[esi]
mov ebx, dimensiune1
mov cl,[esi+ebx]
pop esi
mov al,[esi]
jmp le_loop2


afara_egal:
cmp ok,1
je fara
	push edx
	push ecx
	push eax
	push ebx
	push offset virgula
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset virgula
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	fara:
	mov ok,0
		push edx
		push ecx
		push eax
		push ebx
		push ebx
		push offset formatc_
		call printf
		add esp,8
		pop ebx
		pop eax
		pop ecx
		pop edx
		
	push eax
	push ebx
	push ecx
	push edx
	push ebx
	push offset formatc_
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
lea esi,mult1					; primul element din sirul 1
mov al,[esi]					;
mov ebx,9				
mov ebx, dimensiune1
mov cl,[esi+ebx]				; ultimul element din sirul 1



push edi						; salvez unde am ajuns in al doilea sir

lea edi,mult2
mov ebx,dimensiune2
mov dl,[edi+ebx]				; ultimul element din sirul 2 
					
pop edi							; pozitia in sir
mov bl,[edi]					;
cmp bl,dl					; verific daca nu am ajuns la sfarsitul celui de-al doilea sir
je final
	inc edi
jmp le_loop2


afara_normal:
		
lea esi,mult1
mov al,[esi]
mov ebx, dimensiune1
mov cl,[esi+ebx]				; ultimul element din sirul 1

push edi
lea edi,mult2
mov ebx,dimensiune2
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov bl,[edi]
pop edi

cmp [edi],dl
je final
inc edi

jmp le_loop2

final:
	push edx
	push ecx
	push eax
	push ebx
	push offset paranteza_i
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset paranteza_i
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	linie_noua_fisier
	
endm intersectie


diferenta macro
local le_loop,gata,le_loop2,afara_egal,afara_normal,final
	push edx
	push ecx
	push eax
	push ebx
	push offset paranteza_d
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx

	push eax
	push ebx
	push ecx
	push edx
	push offset paranteza_d
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	initializare

lea esi,mult2
mov al,[esi]
mov ebx, dimensiune2
mov cl,[esi+ebx]				; ultimul element din sirul 1

lea edi,mult1
mov ebx,dimensiune1
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov ebx,0
mov bl,[edi]

le_loop2:


mov ebx, 0
mov bl,[edi]

cmp bl,al
je afara_egal

cmp al,cl
je afara_normal

inc esi
mov al,[esi]

jmp le_loop2


afara_egal:

lea esi,mult2					; primul element din sirul 1
mov al,[esi]					;
mov ebx, dimensiune2
mov cl,[esi+ebx]				; ultimul element din sirul 1



push edi						; salvez unde am ajuns in al doilea sir
lea edi,mult1
mov ebx,dimensiune1
mov dl,[edi+ebx]				; ultimul element din sirul 2 
					
pop edi							; pozitia in sir
mov bl,[edi]					;
cmp [edi],dl					; verific daca nu am ajuns la sfarsitul celui de-al doilea sir
je final
inc edi

jmp le_loop2


afara_normal:
		
		push edx
		push ecx
		push eax
		push ebx
		push ebx
		push offset formatc_
		call printf
		add esp,8
		pop ebx
		pop eax
		pop ecx
		pop edx
		
	push eax
	push ebx
	push ecx
	push edx
	push ebx
	push offset formatc_
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
		
lea esi,mult2
mov al,[esi]
mov ebx, dimensiune2
mov cl,[esi+ebx]				; ultimul element din sirul 1

push edi
lea edi,mult1
mov ebx,dimensiune1
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov bl,[edi]
pop edi

cmp [edi],dl
je final
inc edi
	push edx
	push ecx
	push eax
	push ebx
	push offset virgula
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset virgula
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
jmp le_loop2

final:
	push edx
	push ecx
	push eax
	push ebx
	push offset paranteza_i
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push offset paranteza_i
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	linie_noua_fisier
endm diferenta


produs_cartezian macro
local le_loop,gata,le_loop2,afara_egal,afara_normal,final

	initializare

lea esi,mult2
mov al,[esi]
mov ebx, dimensiune2
mov cl,[esi+ebx]				; ultimul element din sirul 1

lea edi,mult1
mov ebx,dimensiune1
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov ebx,0
mov bl,[edi]

le_loop2:


mov ebx, 0
mov bl,[edi]

	push edx
	push ecx
	push eax
	push ebx
	push eax
	push ebx
	push offset format_produs
	call printf
	add esp,12
	pop ebx
	pop eax
	pop ecx
	pop edx
	
	push eax
	push ebx
	push ecx
	push edx
	push eax
	push ebx
	push offset format_produs
	push fisier
	call fprintf
	add esp,16
	pop edx
	pop ecx
	pop ebx
	pop eax

cmp al,cl
je afara_normal

	push edx
	push ecx
	push eax
	push ebx
	push offset virgula
	call printf
	add esp,4
	pop ebx
	pop eax
	pop ecx
	pop edx

	push eax
	push ebx
	push ecx
	push edx
	push offset virgula
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
inc esi
mov al,[esi]

jmp le_loop2


afara_normal:
		
lea esi,mult2
mov al,[esi]
mov ebx, dimensiune2
mov cl,[esi+ebx]				; ultimul element din sirul 1

push edi
lea edi,mult1
mov ebx,dimensiune1
mov dl,[edi+ebx]				; ultimul element din sirul 2 
mov bl,[edi]
pop edi

cmp [edi],dl
je final
inc edi

jmp le_loop2

final:
linie_noua_fisier
endm produs_cartezian


mesaje_inceput macro
	push offset le_mesaj_0
	call printf
	add esp,4
	linie_noua
	push offset le_mesaj_1
	call printf
	add esp,4
	linie_noua
	push offset le_mesaj_2
	call printf
	add esp,4
	linie_noua
	push offset le_mesaj_3
	call printf
	add esp,4
	linie_noua
	push offset le_mesaj_4
	call printf
	add esp,4
	linie_noua
	push offset le_mesaj_5
	call printf
	add esp,4
	linie_noua
	push offset le_mesaj_6
	call printf
	add esp,4
	linie_noua
	push offset le_mesaj_7
	call printf
	add esp,4
	linie_noua
	push offset cursor
	call printf
	add esp,4
endm mesaje_inceput

linie_noua_fisier macro
	push eax
	push ebx
	push ecx
	push edx
	push 0AH
	push offset formatc
	push fisier
	call fprintf
	add esp,12
	pop edx
	pop ecx
	pop ebx
	pop eax
endm linie_noua_fisier

print_info_fisier macro
	push eax
	push ebx
	push ecx
	push edx
	push offset info
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	linie_noua_fisier
endm print_info_fisier

print_rezultat_fisier macro
	push eax
	push ebx
	push ecx
	push edx
	push offset rezultat
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
endm print_rezultat_fisier


interfata macro
	push offset sir_optiune
	; push offset formats
	call gets
	add esp,4

	mov eax,0
	mov al,[sir_optiune]
	mov optiune,eax
	
	; linie_noua
	cmp optiune, '0'
	je panic
	
	cmp optiune,'1'					;verificare unicitate
	je optiune_1
	jmp over1
	optiune_1:
	; 
	
	push eax
	push ebx
	push ecx
	push edx
	push offset le_mesaj_1
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	linie_noua_fisier
	
	push offset mesaj_mult1
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4

	print_info_fisier
	
	verificare_date offset info
	
	mov ecx, offset dimensiune1
	citire_date	offset mult1, ecx
	linie_noua
	print_rezultat_fisier
	unicitate offset mult1, dimensiune1	
	linie_noua
	
	; push offset mesaj_mult2
	; call printf
	; add esp,4
	
	; linie_noua
	; push offset cursor
	; call printf
	; add esp,4
	
	; push offset info
	; call gets
	; add esp,4
	
	; print_info_fisier
	
	; verificare_date offset info
	
	; mov ecx, offset dimensiune2
	; citire_date	offset mult2, ecx
	
	; linie_noua
	
	; print_rezultat_fisier
	; unicitate offset mult2	, dimensiune2	

	jmp panic
	over1:
	
	cmp optiune,'2'					; apartenenta unui element la multime
	je optiune_2
	jmp over2
	optiune_2:
	
	push eax
	push ebx
	push ecx
	push edx
	push offset le_mesaj_2
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	linie_noua_fisier
	
	
	push offset mesaj_mult1
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4
	
	print_info_fisier
	
	verificare_date offset info
	
	mov ecx, offset dimensiune1
	citire_date	offset mult1, ecx
	
	linie_noua
	unicitate offset mult1, dimensiune1	
	linie_noua
	
	push offset mesaj_cautare
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset element_cautat
	push offset formatc
	call scanf
	add esp,8
	print_rezultat_fisier
	push eax
	mov eax,0
	mov al, element_cautat
	push eax
	push ebx
	push ecx
	push edx
	push eax
	push offset formatc_
	push fisier
	call fprintf
	add esp,12
	pop edx
	pop ecx
	pop ebx
	pop eax
	pop eax
	
	push offset le_mesaj_2
	call printf
	add esp,4
	linie_noua
	
	apartenenta offset mult1,dimensiune1

	over2:
	
	cmp optiune,'3'					; reuniunea
	je optiune_3
	jmp over3
	optiune_3:
	
	push eax
	push ebx
	push ecx
	push edx
	push offset le_mesaj_3
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	linie_noua_fisier
	
	
	push offset mesaj_mult1
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4
	
	print_info_fisier
	
	verificare_date offset info
	
	mov ecx, offset dimensiune1
	citire_date	offset mult1, ecx
	
	linie_noua
	unicitate offset mult1, dimensiune1	
	linie_noua
	
	push offset mesaj_mult2
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4

	print_info_fisier
	
	verificare_date offset info

	mov ecx, offset dimensiune2
	citire_date	offset mult2, ecx
	linie_noua
	unicitate offset mult2, dimensiune2	
	linie_noua
	push offset le_mesaj_3
	call printf
	add esp,4
	linie_noua
	print_rezultat_fisier
	reuniune
	over3:
	
	cmp optiune,'4'					; intersectia
	je optiune_4
	jmp over4
	optiune_4:
	
	push eax
	push ebx
	push ecx
	push edx
	push offset le_mesaj_4
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	linie_noua_fisier
	
	
	push offset mesaj_mult1
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4
	
	print_info_fisier
	
	verificare_date offset info
	
	mov ecx, offset dimensiune1
	citire_date	offset mult1, ecx
	linie_noua
	unicitate offset mult1, dimensiune1	
	linie_noua
	push offset mesaj_mult2
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4

	print_info_fisier
	
	verificare_date offset info

	mov ecx, offset dimensiune2
	citire_date	offset mult2, ecx
	linie_noua
	unicitate offset mult2, dimensiune2	
	linie_noua
	push offset le_mesaj_4
	call printf
	add esp,4
	linie_noua
	print_rezultat_fisier
	intersectie
	over4:
	
	cmp optiune,'5'					; diferenta
	je optiune_5
	jmp over5
	optiune_5:
	
	push eax
	push ebx
	push ecx
	push edx
	push offset le_mesaj_5
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	linie_noua_fisier
	
	
	push offset mesaj_mult1
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4
	
	print_info_fisier
	
	verificare_date offset info
	
	mov ecx, offset dimensiune1
	citire_date	offset mult1, ecx
	linie_noua
	unicitate offset mult1, dimensiune1	
	linie_noua
	push offset mesaj_mult2
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4

	print_info_fisier
	
	verificare_date offset info

	mov ecx, offset dimensiune2
	citire_date	offset mult2, ecx
	linie_noua
	unicitate offset mult2, dimensiune2	
	linie_noua
	push offset le_mesaj_5
	call printf
	add esp,4
	linie_noua
	print_rezultat_fisier
	diferenta
	over5:
	
	cmp optiune,'6'					; produs_cartezian
	je optiune_6
	jmp over6
	optiune_6:
	
	push eax
	push ebx
	push ecx
	push edx
	push offset le_mesaj_6
	push fisier
	call fprintf
	add esp,8
	pop edx
	pop ecx
	pop ebx
	pop eax
	
	linie_noua_fisier
	
	
	push offset mesaj_mult1
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4
	
	print_info_fisier
	
	verificare_date offset info
	
	mov ecx, offset dimensiune1
	citire_date	offset mult1, ecx
	linie_noua
	unicitate offset mult1, dimensiune1	
	linie_noua
	push offset mesaj_mult2
	call printf
	add esp,4
	
	linie_noua
	push offset cursor
	call printf
	add esp,4
	
	push offset info
	call gets
	add esp,4

	print_info_fisier
	
	verificare_date offset info

	mov ecx, offset dimensiune2
	citire_date	offset mult2, ecx
	linie_noua
	unicitate offset mult2, dimensiune2
	linie_noua
	push offset le_mesaj_6
	call printf
	add esp,4
	linie_noua
	print_rezultat_fisier
	produs_cartezian
	over6:
endm interfata

deschidere_fisier macro
push offset mod_citire
push offset nume_fisier
call fopen
add esp, 8
mov fisier,eax
mov eax,0
endm deschidere_fisier


inchidere_fisier macro
	push fisier
	call fclose
	add esp,4
endm inchidere_fisier