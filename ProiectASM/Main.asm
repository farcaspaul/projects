.386
.model flat, stdcall


includelib msvcrt.lib
include library.asm
extern exit: proc
extern printf: proc
extern gets:proc
extern scanf:proc
extern getchar:proc
extern fopen:proc
extern fprintf:proc
extern fclose:proc
extern sscanf:proc

public start

.data
fisier dd ?
mod_citire db "w",0
nume_fisier db  "fisier.txt.",0
lungime_date dd 0
mult1 db 100 dup(0),0
dimensiune1 dd 0    			; dimensiunea multimii 1
dimensiune2 dd 0				; dimensiunea multimii 2
mult2 db 100 dup(0),0
paranteza_d db "( ",0
paranteza_i db ")",0
formatc db "%c",0
formatc_ db "%c ",0
formatd db "%d",0
formatd_ db "%d ",0
formats db "%s",0
formats_ db "%s ",0
virgula db ",",0
format_produs db "( %c, %c )",0
mesaj_cautare db "Dati element cautat: ",0
nu_tocmai db "nu apartine!",0
gasit db "Apartine!",0
duplicat db "%c este duplicat.",0
mesaj_mult1 db "Dati multimea 1: ",0
mesaj_mult2 db "Dati multimea 2: ",0
greseala db "Sirul este introdus gresit deoarece nu s-a respectat formatul <caracter>spatiu..",0
info db ?,0
counter dd 0	
lungime_info dd 0
element_cautat db ?
temp dd 0
ok db 0
sir_optiune db 50 dup(?),0
le_mesaj_0 db "Introduceti o operatie cu multimi:",0 
le_mesaj_1 db "1. Verificare",0
le_mesaj_2 db "2. Apartenenta",0
le_mesaj_3 db "3. Reuniune",0
le_mesaj_4 db "4. Intersectie",0
le_mesaj_5 db "5. Diferenta",0
le_mesaj_6 db "6. Produs cartezian",0
le_mesaj_7 db "0. Exit",0
optiune dd 0
cursor db ">",0
fara_duplicat db "Nu exista duplicate.",0
rezultat db "Rezultat: ",0
.code

start:

initializare    			; registrii -> 0

deschidere_fisier

mesaje_inceput				; afiseaza interfata

interfata					; ruleaza interfata

	PANIC:
inchidere_fisier
	push 0
	call exit
end start