CREATE DATABASE if not exists `bank`  /*!40100 DEFAULT CHARACTER SET utf8 */ ;

CREATE TABLE if not exists `account` (
  `idaccount` int(11) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(45) NOT NULL,
  `account_money` float NOT NULL,
  `account_date` datetime NOT NULL,
  `account_idclient` int(11) NOT NULL,
  PRIMARY KEY (`idaccount`),
  KEY `idclient_idx` (`account_idclient`),
  CONSTRAINT `account_idclient` FOREIGN KEY (`account_idclient`) REFERENCES `client` (`idclient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;


CREATE TABLE if not exists`activity` (
  `idactivity` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  `idemployee` int(11) NOT NULL,
  PRIMARY KEY (`idactivity`),
  KEY `idemployee_idx` (`idemployee`),
  CONSTRAINT `idemployee` FOREIGN KEY (`idemployee`) REFERENCES `employee` (`idemployee`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

CREATE TABLE if not exists`client` (
  `idclient` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `cnp` varchar(13) NOT NULL,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`idclient`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


CREATE TABLE if not exists`employee` (
  `idemployee` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`idemployee`),
  UNIQUE KEY `idemployee_UNIQUE` (`idemployee`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;


