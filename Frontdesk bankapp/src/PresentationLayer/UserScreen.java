package PresentationLayer;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import BusinessLayer.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.ScrollPane;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;

public class UserScreen {

	private JFrame frmUserMenu;
	private JTextField typeText;
	private JLabel lblCash;
	private JTextField cashText;
	private JTextPane accountsText,clientsText;
	private String username;
	private String userId;
	private User user;
	private JTextField idAccountText;
	private JTextField clientNameText;
	private JTextField clientAddressText;
	private JTextField clientCNPText;
	private JTextField clientIdText;

	// public UserScreen() {
	// initialize();
	// }

	public UserScreen(String name) {

		username = name;
		user = new User();
		System.out.println("User name = " + username);
		userId = (String) user.getEmployeeByName(username)[0];
		initialize();
		frmUserMenu.setVisible(true);

	}

	public boolean isAlpha(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isAlphaNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !(c == '.'))
				return false;
		}

		return true;
	}

	private void updateAccountsText() {
		int i = 0;
		String result = "";
	//	System.out.println("User id = " + userId);
		Object[][] objects = user.getAllAccounts();

		while (objects[i][0] != null) {
			// System.out.println(objects[i][0] + " " + objects[i][1] + " " +
			// objects[i][2]);
			System.out.println("Counter = " + i);
			// System.out.println(objects[i][0] + " " + objects[i][1] + " " + objects[i][2]
			// + " " + objects[i][3] + " " + objects[i][4]);
			result += objects[i][0] + " " + objects[i][1] + " " + objects[i][2] + " " + objects[i][3] + " "
					+ objects[i][4] + "\n";

			// employeeComboBox.addItem(objects[i][0] + " " + objects[i][1] + " " +
			// objects[i][2]);
			i++;
		}
		accountsText.setText(result);
	}

	private void updateClientsText() {
		int i = 0;
		String result = "";
	//	System.out.println("User id = " + userId);
		Object[][] objects = user.getAllClients();

		while (objects[i][0] != null) {
			// System.out.println(objects[i][0] + " " + objects[i][1] + " " +
			// objects[i][2]);
			System.out.println("Counter = " + i);
			// System.out.println(objects[i][0] + " " + objects[i][1] + " " + objects[i][2]
			// + " " + objects[i][3] + " " + objects[i][4]);
			result += objects[i][0] + " " + objects[i][1] + " " + objects[i][2] + " " + objects[i][3] +"\n";

			// employeeComboBox.addItem(objects[i][0] + " " + objects[i][1] + " " +
			// objects[i][2]);
			i++;
		}
		clientsText.setText(result);
	}
	
	private void initialize() {
		frmUserMenu = new JFrame();
		frmUserMenu.setResizable(false);
		frmUserMenu.setTitle("User Menu");
		frmUserMenu.setBounds(100, 100, 856, 383);
		frmUserMenu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmUserMenu.getContentPane().setLayout(null);

		JButton btnAddAccount = new JButton("Add account");
		btnAddAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String id = idAccountText.getText();
				String accountType = typeText.getText();
				String cash = cashText.getText();
		//		String employeeName = employeeText.getText();
				if (user.checkClient(id)) {
					if (isNumeric(cash)) {
						if (isAlpha(accountType)) {
							LocalDate localDate = LocalDate.now();

							user.addAcount(accountType, cash,
									DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate), id);
							typeText.setText(null);
							cashText.setText(null);
							idAccountText.setText(null);
						//	employeeText.setText(null);
							updateAccountsText();
						} else {
							JOptionPane.showMessageDialog(null, "Wrong account type input!", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Cash input error!", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Inexistent client!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnAddAccount.setBounds(24, 189, 119, 23);
		frmUserMenu.getContentPane().add(btnAddAccount);

		typeText = new JTextField();
		typeText.setBounds(280, 189, 119, 20);
		frmUserMenu.getContentPane().add(typeText);
		typeText.setColumns(10);

		accountsText = new JTextPane();
		accountsText.setEditable(false);
		accountsText.setBounds(0, 150, 221, 91);
		
		clientsText = new JTextPane();
		clientsText.setEditable(false);
		//clientsText.setBounds(0, 150, 221, 91);
		
		JLabel lblType = new JLabel("Type");
		lblType.setBounds(210, 193, 46, 14);
		frmUserMenu.getContentPane().add(lblType);

		lblCash = new JLabel("Cash");
		lblCash.setBounds(210, 220, 46, 14);
		frmUserMenu.getContentPane().add(lblCash);

		cashText = new JTextField();
		cashText.setBounds(280, 216, 119, 20);
		frmUserMenu.getContentPane().add(cashText);
		cashText.setColumns(10);

		ScrollPane accountsScroll = new ScrollPane();
		accountsScroll.setBounds(167, 10, 257, 164);
		updateAccountsText();
		accountsScroll.add(accountsText);
		frmUserMenu.getContentPane().add(accountsScroll);

		JButton deleteButton = new JButton("Delete");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String accountId = idAccountText.getText();
				if (isNumeric(accountId) && user.getAccount(accountId) != null) {
					user.deleteAccount(accountId);
					idAccountText.setText(null);
					updateAccountsText();
				} else {
					JOptionPane.showMessageDialog(null, "Invalid account!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		deleteButton.setBounds(24, 11, 104, 23);
		frmUserMenu.getContentPane().add(deleteButton);

		JSeparator separator = new JSeparator();
		separator.setBounds(24, 306, 802, 2);
		frmUserMenu.getContentPane().add(separator);

		JButton updateButton = new JButton("Update");
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String accountId = idAccountText.getText();
				String type = typeText.getText();
				String cash = cashText.getText();
				if (isNumeric(accountId)) {
					if (user.getAccount(accountId) != null) {
						if (isNumeric(cash)) {
							user.updateAccount(accountId, cash, type);
							updateAccountsText();
							cashText.setText(null);
							idAccountText.setText(null);
							typeText.setText(null);
							
						} else {
							JOptionPane.showMessageDialog(null, "Invalid cash amount!", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}

					} else {
						JOptionPane.showMessageDialog(null, "Inexistent account!", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Invalid account id!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		updateButton.setBounds(24, 45, 104, 23);
		frmUserMenu.getContentPane().add(updateButton);

		JButton transferButton = new JButton("Transfer");
		transferButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				TransferScreen transferScreen = new TransferScreen(user, userId);
				
			}
		});
		transferButton.setBounds(24, 79, 104, 23);
		frmUserMenu.getContentPane().add(transferButton);

		JButton billsButton = new JButton("Bills");
		billsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				BillsScreen billsScreen = new BillsScreen(user,userId);
			}
		});
		billsButton.setBounds(24, 113, 104, 23);
		frmUserMenu.getContentPane().add(billsButton);

		JLabel lblId = new JLabel("Id");
		lblId.setBounds(210, 247, 46, 14);
		frmUserMenu.getContentPane().add(lblId);

		idAccountText = new JTextField();
		idAccountText.setBounds(280, 244, 119, 20);
		frmUserMenu.getContentPane().add(idAccountText);
		idAccountText.setColumns(10);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateAccountsText();
			}
		});
		btnRefresh.setBounds(335, 321, 99, 23);
		frmUserMenu.getContentPane().add(btnRefresh);
		
		ScrollPane scrollPaneClients = new ScrollPane();
		scrollPaneClients.setBounds(443, 10, 397, 164);
		updateClientsText();
		scrollPaneClients.add(clientsText);
		frmUserMenu.getContentPane().add(scrollPaneClients);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(465, 193, 46, 14);
		frmUserMenu.getContentPane().add(lblName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(465, 220, 61, 14);
		frmUserMenu.getContentPane().add(lblAddress);
		
		JLabel lblCnp = new JLabel("CNP");
		lblCnp.setBounds(465, 250, 46, 14);
		frmUserMenu.getContentPane().add(lblCnp);
		
		clientNameText = new JTextField();
		clientNameText.setBounds(521, 190, 179, 20);
		frmUserMenu.getContentPane().add(clientNameText);
		clientNameText.setColumns(10);
		
		clientAddressText = new JTextField();
		clientAddressText.setBounds(521, 217, 179, 20);
		frmUserMenu.getContentPane().add(clientAddressText);
		clientAddressText.setColumns(10);
		
		clientCNPText = new JTextField();
		clientCNPText.setBounds(521, 247, 179, 20);
		frmUserMenu.getContentPane().add(clientCNPText);
		clientCNPText.setColumns(10);
		
		JLabel lblId_1 = new JLabel("Id");
		lblId_1.setBounds(465, 281, 46, 14);
		frmUserMenu.getContentPane().add(lblId_1);
		
		clientIdText = new JTextField();
		clientIdText.setBounds(521, 278, 46, 20);
		frmUserMenu.getContentPane().add(clientIdText);
		clientIdText.setColumns(10);
		
		JButton addClientButton = new JButton("Add");
		addClientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			//	String id = clientIdText.getText();
				String name = clientNameText.getText();
				String CNP = clientCNPText.getText();
				String address = clientAddressText.getText();
				if (isAlpha(address)) {
					if (isNumeric(CNP)&&CNP.length()==13) {
						if (isAlpha(name)) {
							
							user.addClient(name, CNP, address);
							clientCNPText.setText(null);
							clientNameText.setText(null);
							clientAddressText.setText(null);
							updateClientsText();
						} else {
							JOptionPane.showMessageDialog(null, "Wrong name input!", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null, "CNP input error!", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Invalid address!", "WARNING", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		addClientButton.setBounds(731, 189, 89, 23);
		frmUserMenu.getContentPane().add(addClientButton);
		
		JButton updateClientButton = new JButton("Update");
		updateClientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = clientIdText.getText();
				String name = clientNameText.getText();
				String CNP = clientCNPText.getText();
				String address = clientAddressText.getText();
				if (isNumeric(id) && user.checkClient(id)) {
					if (address.length() > 0) {
						if (isAlpha(address)) {
							user.updateClientAddress(id, address);
						} else {
							JOptionPane.showMessageDialog(null, "Invalid address!", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					}
					if (CNP.length() > 0) {
						if (isNumeric(CNP) && CNP.length() == 13) {
							user.updateClientCNP(id, CNP);
						} else {
							JOptionPane.showMessageDialog(null, "CNP input error!", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					}
					if (name.length() > 0) {
						if (isAlpha(name)) {
							user.updateClientName(id, name);
						} else {
							JOptionPane.showMessageDialog(null, "Wrong name input!", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					}
				}
				clientIdText.setText(null);
				clientCNPText.setText(null);
				clientAddressText.setText(null);
				clientNameText.setText(null);
				updateClientsText();

			}
		});
		updateClientButton.setBounds(731, 229, 89, 23);
		frmUserMenu.getContentPane().add(updateClientButton);
	}
}
