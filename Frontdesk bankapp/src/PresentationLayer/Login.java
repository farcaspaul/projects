package PresentationLayer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import BusinessLayer.User;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frmLogin;
	private JTextField userName;
	private JPasswordField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setResizable(false);
		frmLogin.setTitle("Login");
		frmLogin.setBounds(100, 100, 414, 322);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(63, 63, 71, 24);
		frmLogin.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(63, 129, 71, 14);
		frmLogin.getContentPane().add(lblPassword);

		userName = new JTextField();
		userName.setBounds(163, 65, 128, 20);
		frmLogin.getContentPane().add(userName);
		userName.setColumns(10);

		password = new JPasswordField();
		password.setBounds(163, 129, 128, 20);
		frmLogin.getContentPane().add(password);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String userText = userName.getText();
				char[] adminPass = { 'a', 'd', 'm', 'i', 'n' };
	//			char[] userPass = { 'u', 's', 'e', 'r' };
				char[] passwordText = password.getPassword();
				// System.out.println(passwordText);
				boolean passCheck = true;
				if (passwordText.length == adminPass.length) {
					for (int i = 0; i < adminPass.length; i++) {
						// System.out.println(adminPass[i] + " vs " + passwordText[i]);
						if (adminPass[i] != passwordText[i]) {
							passCheck = false;
							break;
						}
					}
					if (passCheck && userText.equals("admin")) {
						// Admin admin = new Admin();
						System.out.println("Admin screen turned on.");
						@SuppressWarnings("unused")
						AdminScreen adminScreen = new AdminScreen();
						// admin screen
					}

				} else {
					// user checking
					User user = new User();

					passCheck = true;
					if (user.checkEmployeeByName(userText)) {
						passCheck = user.checkPassword(passwordText, userText);
						if (!passCheck) {
							JOptionPane.showMessageDialog(null, "Wrong password.", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						} else {
							// user screen
							System.out.println("User screen turned on.");
							@SuppressWarnings("unused")
							UserScreen userScreen = new UserScreen(userText);
							// JOptionPane.showMessageDialog(null, "KANSEI DERIFTO!", "WARNING",
							// JOptionPane.WARNING_MESSAGE);

						}

					} else {
						JOptionPane.showMessageDialog(null, "Invalid user.", "ERROR", JOptionPane.WARNING_MESSAGE);
					}
				}
			}
		});
		btnLogin.setBounds(20, 218, 89, 23);
		frmLogin.getContentPane().add(btnLogin);

		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				userName.setText(null);
				password.setText(null);
			}
		});
		btnReset.setBounds(163, 218, 89, 23);
		frmLogin.getContentPane().add(btnReset);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmLogin.dispose();
			}
		});
		btnExit.setBounds(309, 218, 89, 23);
		frmLogin.getContentPane().add(btnExit);
		
	}
}
