package PresentationLayer;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


import BusinessLayer.Admin;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.ScrollPane;
import javax.swing.JPasswordField;

public class AdminScreen {

	private JFrame frmAdminMenu;
	private JTextField idText;
	private JTextField nameText;
	private JTextField addressText;
	private JTextField startDateText;
	private JTextField endDateText;
	private JTextPane employeeText;
	private Admin admin;
	private JPasswordField passwordText;


	public AdminScreen() {
		initialize();

	}

	private void updateEmployeeText() {
		int i = 0;
		String result = "";
		Object[][] objects = admin.getAllEmployees();

		while (objects[i][0] != null) {
			// System.out.println(objects[i][0] + " " + objects[i][1] + " " +
			// objects[i][2]);
			result += objects[i][0] + " " + objects[i][1] + " " + objects[i][2] + "\n";

			// employeeComboBox.addItem(objects[i][0] + " " + objects[i][1] + " " +
			// objects[i][2]);
			i++;
		}
		employeeText.setText(result);

	}

	public boolean isAlpha(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isAlphaNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c))
				return false;
		}

		return true;
	}

	public static boolean checkDate(String date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(date.trim());
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	}
	
	private void initialize() {
		frmAdminMenu = new JFrame();
		frmAdminMenu.setTitle("Admin Menu");
		frmAdminMenu.setBounds(100, 100, 384, 334);
		frmAdminMenu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAdminMenu.getContentPane().setLayout(null);
		admin = new Admin();
		
		JLabel lblId = new JLabel("Id");
		lblId.setBounds(37, 162, 46, 14);
		frmAdminMenu.getContentPane().add(lblId);
		
		idText = new JTextField();
		idText.setEditable(true);
		idText.setEnabled(true);
		idText.setBounds(22, 187, 39, 20);
		idText.setText(null);
		frmAdminMenu.getContentPane().add(idText);
		
		idText.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(122, 162, 46, 14);
		frmAdminMenu.getContentPane().add(lblName);
		
		nameText = new JTextField();
		nameText.setBounds(71, 187, 134, 20);
		nameText.setText(null);
		frmAdminMenu.getContentPane().add(nameText);
		nameText.setColumns(10);
		
		addressText = new JTextField();
		addressText.setBounds(215, 187, 128, 20);
		addressText.setText(null);
		frmAdminMenu.getContentPane().add(addressText);
		addressText.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(255, 162, 69, 14);
		frmAdminMenu.getContentPane().add(lblAddress);
		
		JButton btnReport = new JButton("Report");
		btnReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = new File ("C:/Users/Paul/Desktop/file.txt");
				PrintWriter printWriter;
				if (checkDate(startDateText.getText()) && checkDate(endDateText.getText())) {
					if(isNumeric(idText.getText())) {
						Object[][] data = admin.viewEmployeeActivity(idText.getText(),startDateText.getText(),endDateText.getText());
						 
						try {
							printWriter = new PrintWriter (file);
							int i = 0;
							while (!(data[i][0]==null)) {
							//	System.out.println(data[i][0] + " " + data[i][1] + " " + data[i][2] + " " +data[i][3]);
								printWriter.println(data[i][0] + " " + data[i][1] + " " + data[i][2] + " " + data[i][3]);
								i++;
							}
							printWriter.close();

						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						idText.setText(null);
						startDateText.setText(null);
						endDateText.setText(null);
						      
					}else
					{
						JOptionPane.showMessageDialog(null, "Wrong id input!", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
					
				}else {
					JOptionPane.showMessageDialog(null, "Wrong date input!", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnReport.setBounds(10, 112, 89, 23);
		frmAdminMenu.getContentPane().add(btnReport);
		
		startDateText = new JTextField();
		startDateText.setBounds(122, 113, 86, 20);
		frmAdminMenu.getContentPane().add(startDateText);
		startDateText.setColumns(10);
		
		endDateText = new JTextField();
		endDateText.setBounds(238, 113, 86, 20);
		frmAdminMenu.getContentPane().add(endDateText);
		endDateText.setColumns(10);
		
		JLabel lblTo = new JLabel("to");
		lblTo.setBounds(215, 116, 46, 14);
		frmAdminMenu.getContentPane().add(lblTo);
		
		employeeText = new JTextPane();
		employeeText.setEditable(false);
		employeeText.setBounds(0, 150, 221, 91);
	//	frmAdminMenu.getContentPane().add(employeeText);
		
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setBounds(122, 10, 221, 100);
		frmAdminMenu.getContentPane().add(scrollPane);
		scrollPane.add(employeeText);
		frmAdminMenu.setVisible(true);
		updateEmployeeText();
		
		
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (isAlpha(nameText.getText())) {
					if (!admin.checkEmployeeByName(nameText.getText())) {
						// nume bun
						if (isAlphaNumeric(addressText.getText())) {
							// adresa buna
							String pass = new String(passwordText.getPassword());
							if (pass.length() > 1) {
								admin.addEmployee(nameText.getText(), addressText.getText(),pass);
								nameText.setText(null);
								addressText.setText(null);
								idText.setText(null);
								updateEmployeeText();
							} else {
								JOptionPane.showMessageDialog(null, "Invalid password!", "WARNING",
										JOptionPane.WARNING_MESSAGE);
							}
						} else {
							JOptionPane.showMessageDialog(null, "Wrong address input!", "WARNING",
									JOptionPane.WARNING_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Name already exists!", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Wrong name input!", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnCreate.setBounds(10, 10, 89, 23);
		frmAdminMenu.getContentPane().add(btnCreate);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(isAlpha(nameText.getText()) && isAlpha(addressText.getText()) && isNumeric(idText.getText())) {
					
					admin.updateEmployee(idText.getText(), nameText.getText(), addressText.getText());
					nameText.setText(null);
					idText.setText(null);
					addressText.setText(null);
					updateEmployeeText();
				}else {
					JOptionPane.showMessageDialog(null, "Wrong input!", "WARNING",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnUpdate.setBounds(10, 44, 89, 23);
		frmAdminMenu.getContentPane().add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(!nameText.getText().equals("")) {
					if(isAlpha(nameText.getText())) {
						admin.deleteEmployeeByName(nameText.getText());
						nameText.setText(null);
						updateEmployeeText();
					}else {
						JOptionPane.showMessageDialog(null, "Wrong name input!", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
				}else {
					if(isNumeric(idText.getText())) {
						admin.deleteEmployee(idText.getText());
						System.out.println("Deleting user with id = "+idText.getText());
						idText.setText(null);
						updateEmployeeText();
					}else {
						JOptionPane.showMessageDialog(null, "Wrong id input!", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					}
				}
			}
		});
		btnDelete.setBounds(10, 78, 89, 23);
		frmAdminMenu.getContentPane().add(btnDelete);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(111, 214, 76, 14);
		frmAdminMenu.getContentPane().add(lblPassword);
		
		passwordText = new JPasswordField();
		passwordText.setBounds(71, 234, 134, 20);
		frmAdminMenu.getContentPane().add(passwordText);
		frmAdminMenu.setVisible(true);
		
	}
}
