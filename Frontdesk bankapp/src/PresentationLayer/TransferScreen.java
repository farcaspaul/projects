package PresentationLayer;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


import BusinessLayer.User;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;

public class TransferScreen {

	private JFrame frmTransferMenu;
	private JTextField idFromText;
	private JTextField idToText;
	private JTextField amountText;
	private String userId;
	private User user;



	public TransferScreen(User user, String userId) {
		initialize();
		this.userId = userId;
		this.user = user;
	}
	public boolean isAlpha(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isAlphaNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !(c == '.'))
				return false;
		}

		return true;
	}
	

	
	private void initialize() {
		frmTransferMenu = new JFrame();
		frmTransferMenu.setTitle("Transfer Menu");
		frmTransferMenu.setBounds(100, 100, 545, 117);
		frmTransferMenu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmTransferMenu.getContentPane().setLayout(null);
		
		JLabel lblFrom = new JLabel("ID From:");
		lblFrom.setBounds(10, 11, 58, 14);
		frmTransferMenu.getContentPane().add(lblFrom);
		
		idFromText = new JTextField();
		idFromText.setBounds(59, 8, 86, 20);
		frmTransferMenu.getContentPane().add(idFromText);
		idFromText.setColumns(10);
		
		JLabel lblTo = new JLabel("ID To:");
		lblTo.setBounds(367, 11, 58, 14);
		frmTransferMenu.getContentPane().add(lblTo);
		
		idToText = new JTextField();
		idToText.setBounds(435, 8, 86, 20);
		frmTransferMenu.getContentPane().add(idToText);
		idToText.setColumns(10);
		
		amountText = new JTextField();
		amountText.setBounds(246, 8, 86, 20);
		frmTransferMenu.getContentPane().add(amountText);
		amountText.setColumns(10);
		
		JLabel lblAmount = new JLabel("Amount:");
		lblAmount.setBounds(178, 11, 58, 14);
		frmTransferMenu.getContentPane().add(lblAmount);
		
		JButton proceedButton = new JButton("Proceed");
		proceedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//String name = employeeNameText.getText();
				String from = idFromText.getText();
				String to = idToText.getText();
				String amount = amountText.getText();
			//	if(user.checkEmployeeByName(name)) {
					System.out.println("SECOND ACCOUNT TYPE = "+(String)user.getAccount(to)[1]);
					if(isNumeric(from)  && user.getAccount(from)[1]!=null && !(((String)(user.getAccount(from)[1])).equals(""))) {
						if(isNumeric(to)  && user.getAccount(to)[1]!=null &&!(((String)(user.getAccount(to)[1])).equals(""))) {
							if(isNumeric(amount)) {
								if(Float.parseFloat(user.getMoney(from))>= Float.parseFloat(amount)) {
									LocalDate localDate = LocalDate.now();
									user.transfer(from, to, amount);
						//			System.out.println("transfer" + "DATA = "+DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate)+" IDUL ANGAJAT = "+(String)user.getEmployeeByName(name)[0]);
									user.addActivity( "transfer" ,DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate),userId );
								//	employeeNameText.setText(null);
									idFromText.setText(null);
									idToText.setText(null);
									amountText.setText(null);
								}else {
									JOptionPane.showMessageDialog(null, "Not enough money!", "WARNING", JOptionPane.WARNING_MESSAGE);
								}
							}else {
								JOptionPane.showMessageDialog(null, "Invalid amount account!", "WARNING", JOptionPane.WARNING_MESSAGE);
							}
						}else {
							JOptionPane.showMessageDialog(null, "Invalid destination account!", "WARNING", JOptionPane.WARNING_MESSAGE);
						}
					}else {
						JOptionPane.showMessageDialog(null, "Invalid source account!", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
//				}else {
//					JOptionPane.showMessageDialog(null, "Invalid employee!", "WARNING", JOptionPane.WARNING_MESSAGE);
//				}
			}
		});
		proceedButton.setBounds(10, 44, 89, 23);
		frmTransferMenu.getContentPane().add(proceedButton);
		frmTransferMenu.setVisible(true);
	}

}
