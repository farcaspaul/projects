package PresentationLayer;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import BusinessLayer.User;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;

public class BillsScreen {

	private JFrame frmBillsMenu;
	private JTextField accountIdText;
	private JTextField moneyText;
	private User user;
	private String userId;


	public BillsScreen(User user,String userId) {
		this.user = user;
		this.userId = userId;
		initialize();
		
	}

	public boolean isAlpha(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isAlphaNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) && !(c == ' '))
				return false;
		}

		return true;
	}

	public boolean isNumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !(c == '.'))
				return false;
		}

		return true;
	}
	
	private void initialize() {
		frmBillsMenu = new JFrame();
		frmBillsMenu.setTitle("Bills Menu");
		frmBillsMenu.setBounds(100, 100, 276, 137);
		frmBillsMenu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmBillsMenu.getContentPane().setLayout(null);
		
		JLabel lblPayingAccount = new JLabel("Paying account id:");
		lblPayingAccount.setBounds(10, 11, 113, 14);
		frmBillsMenu.getContentPane().add(lblPayingAccount);
		
		accountIdText = new JTextField();
		accountIdText.setBounds(122, 8, 119, 20);
		frmBillsMenu.getContentPane().add(accountIdText);
		accountIdText.setColumns(10);
		
		JLabel lblBillValue = new JLabel("Bill value:");
		lblBillValue.setBounds(52, 42, 71, 14);
		frmBillsMenu.getContentPane().add(lblBillValue);
		
		moneyText = new JTextField();
		moneyText.setBounds(122, 39, 119, 20);
		frmBillsMenu.getContentPane().add(moneyText);
		moneyText.setColumns(10);
		
		JButton proceedButton = new JButton("Proceed");
		proceedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String idaccount = accountIdText.getText();
				String money = moneyText.getText();
				if(isNumeric(idaccount)) {
					if(user.getAccount(idaccount)[1]!=null && !(((String)(user.getAccount(idaccount)[1])).equals("")) ) {
						if(isNumeric(money)) {
							if(Float.parseFloat((String)user.getMoney(idaccount)) >= Float.parseFloat(money)) {
									LocalDate localDate = LocalDate.now();
									user.setMoney(idaccount, money);
									user.addActivity("bills", DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate),userId );
									accountIdText.setText(null);
									moneyText.setText(null);

							}else {
								JOptionPane.showMessageDialog(null, "Not enough money!", "WARNING", JOptionPane.WARNING_MESSAGE);
							}
						}else {
							JOptionPane.showMessageDialog(null, "Invalid money amount!", "WARNING", JOptionPane.WARNING_MESSAGE);
						}
					}else {
						JOptionPane.showMessageDialog(null, "Inexistent account!", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
				}
			}
			
		});
		proceedButton.setBounds(152, 70, 89, 23);
		frmBillsMenu.getContentPane().add(proceedButton);
		frmBillsMenu.setVisible(true);
	}

}
