package Testing;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import BusinessLayer.Admin;
import BusinessLayer.User;
public class Main {

	public static void main(String[] args) {
		LocalDate localDate = LocalDate.now();
		Admin adminTest = new Admin();
		User userTest = new User();
		adminTest.addEmployee("FakeName", "FakeAddress", "FakePassword");
		assert adminTest.checkEmployeeByName("FakeName"):"Add employee not working";
		userTest.addClient("FakeClientName", "1234567890123", "FakeClientAddress");
		assert userTest.checkClientByName("FakeClientName"):"Add client not working";
		userTest.updateClientName((String)userTest.getClientByName("FakeClientName")[0], "EvenFakerClientName");
		assert !(userTest.checkClientByName("FakeClientName")):"Update client name not working";
		userTest.addAcount("SixFitthy", "6.50", DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate), (String)userTest.getClientByName("EvenFakerClientName")[0]);
		userTest.deleteClient((String)userTest.getClientByName("EvenFakerClientName")[0]);
		assert !(userTest.checkClientByName("EvenFakerClientName")):"Delete client not working";
		adminTest.deleteEmployee((String)adminTest.getEmployeeByName("FakeName")[0]);
		assert !(adminTest.checkEmployeeByName("FakeName")):"Delete employee not working";
//		
	}
}
