package BusinessLayer;

import DatabaseLayer.ActivityQueries;
import DatabaseLayer.EmployeeQueries;

public class Admin {
	private EmployeeQueries employeeQueries = new EmployeeQueries();
	private ActivityQueries activityQueries = new ActivityQueries();
	
    public void addEmployee(String name, String address, String password){
        employeeQueries.addEmployee(name, address, password);
    }

    public void updateEmployee(String idemployee, String name, String address){
    	employeeQueries.updateEmployee(idemployee, name, address);
    }

    public void deleteEmployee(String idemployee){
    	employeeQueries.deleteEmployee(idemployee);
    }
    
    public void deleteEmployeeByName(String name){
    	employeeQueries.deleteEmployeeByName(name);
    }

    public Object[] getEmployee(String idemployee){
        return employeeQueries.getEmployee(idemployee);
    }
    public Object[] getEmployeeByName(String name){
        return employeeQueries.getEmployeeByName(name);
    }
    public Object[][] getAllEmployees(){
        return employeeQueries.getAllEmployees();
    }

    public Object[][] viewAllActivity(){
        return activityQueries.viewAllActivity();
    }

    public boolean checkEmployeeByName(String name) {
    	return employeeQueries.checkEmployeeByName(name);
    }
    public boolean checkEmployee(String idemployee) {
    	return employeeQueries.checkEmployee(idemployee);
    }
    public Object[][] viewEmployeeActivity(String idemployee, String startDate, String endDate){
        return activityQueries.viewEmployeeActivity(idemployee, startDate, endDate);
    }

}
