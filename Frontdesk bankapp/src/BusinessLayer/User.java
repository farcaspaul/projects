package BusinessLayer;

import DatabaseLayer.AccountQueries;
import DatabaseLayer.ActivityQueries;
import DatabaseLayer.ClientQueries;
import DatabaseLayer.EmployeeQueries;

public class User {

	ClientQueries clientQueries = new ClientQueries();
	AccountQueries accountQueries = new AccountQueries();
	ActivityQueries activityQueries = new ActivityQueries();
	EmployeeQueries employeeQueries = new EmployeeQueries();
    public void addClient(String name, String cnp, String address ){
    	clientQueries.addClient(name, cnp, address);
    }

    public void updateClientName(String idclient, String name){
    	clientQueries.updateClientName(idclient, name);
    }

    public void updateClientAddress(String idclient, String address){
    	clientQueries.updateClientAddress(idclient, address);
    }
    
    public void updateClientCNP(String idclient, String CNP){
    	clientQueries.updateClientCNP(idclient, CNP);
    }

    public Object[] getClient(String idclient) {
        return clientQueries.getClient(idclient);
    }
    
    public Object[] getClientByName(String name) {
        return clientQueries.getClientByName(name);
    }
    
    public boolean checkClientByName(String name) {
    	return clientQueries.checkClientByName(name);
    }

    public Object[][] getAllClients(){
        return clientQueries.getAllClients();
    }

    public void addAcount(String type, String money, String date, String idclient){
    	accountQueries.addAccount(type, money, date, idclient);
    }

    public void updateAccount(String idaccount, String money, String type){
    	accountQueries.updateAccount(idaccount, money,type);
    }

    public void deleteAccount(String idaccount){
    	accountQueries.deleteAccount(idaccount);
    }

    public Object[][] getAllAccounts(){
        return accountQueries.getAllAccounts();
    }

    public Object[][] getAllAccountsOfClient(String idclient){
        return accountQueries.getAllAccountsOfClient(idclient);
    }

    public Object[] getAccount(String idaccount){
        return accountQueries.getAccount(idaccount);
    }

    public void transfer(String idFrom, String idTo, String amount){
        accountQueries.transfer(idFrom, idTo, amount);
    }

    public String getMoney(String idaccount){
        return accountQueries.getMoney(idaccount);
    }
    public void setMoney(String idaccount, String amount){
        accountQueries.setMoney(idaccount,amount);
    }

    public void addActivity(String idemployee, String type, String date){
    	activityQueries.addActivity(idemployee, type, date);
    }

    public boolean checkEmployee(String idemployee){
        return employeeQueries.checkEmployee(idemployee);
    }
    public Object[] getEmployeeByName(String name) {
    	return employeeQueries.getEmployeeByName(name);
    }
    
    public boolean checkClient(String id){
        return clientQueries.checkClient(id);
    }
    
    public boolean checkEmployeeByName(String name){
        return employeeQueries.checkEmployeeByName(name);
    }
    public boolean checkPassword(char[] password, String name) {
    	return employeeQueries.checkPassword(password, name);
    }
    public void deleteClient(String clientId) {
    	clientQueries.deleteClient(clientId);
    }

}
