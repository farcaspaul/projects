package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EmployeeQueries {

    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private DBConnect connection = null;

    public EmployeeQueries() {
    	connection = new DBConnect();
    }

    public void addEmployee(String name, String address, String password) {
        try {
            String query = "INSERT INTO bank.employee (name, address, password) VALUES ('" + name + "', '" + address +"', '" + password + "');";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateEmployee(String idemployee, String name, String address) {
        try {
            String query = "update bank.employee set address = '" + address + "' , name = '" + name + "' where idemployee = " + idemployee + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteEmployee(String idemployee) {
        try {
        	String query2 = "delete from bank.activity where idemployee = " + idemployee + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query2);
            preparedStatement.executeUpdate();
            String query = "delete from bank.employee where idemployee = " + idemployee + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
            
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteEmployeeByName(String name) {
        try {
        	String query2 = "delete from bank.activity where name = " + name + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query2);
            preparedStatement.executeUpdate();
            String query = "delete from bank.employee where name = '" + name + "';";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public Object[] getEmployee(String idemployee) {
        Object[] rezultat = new Object[4];
        try {
            String query = "SELECT * FROM bank.employee WHERE idemployee =" + idemployee + ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                for (int i = 0; i < 4; i++) {
                    rezultat[i] = resultSet.getString(i + 1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public Object[] getEmployeeByName(String name) {
        Object[] rezultat = new Object[4];
        try {
            String query = "SELECT * FROM bank.employee WHERE name =" + "'" + name + "'"+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                for (int i = 0; i < 4; i++) {
                    rezultat[i] = resultSet.getString(i + 1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }
    
    public Object[][] getAllEmployees() {
        Object[][] rezultat = new Object[1000][4];
        try {
            String query = "SELECT * FROM bank.employee;";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int row = resultSet.getRow() - 1;
                for (int i = 0; i < 4; i++) {
                    rezultat[row][i] = resultSet.getString(i + 1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public boolean checkEmployee(String idemployee) {
        boolean found = false;
        try {
            String query = "SELECT * FROM bank.employee WHERE idemployee =" + idemployee + ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }

    public boolean checkEmployeeByName(String name) {
        boolean found = false;
        try {
            String query = "SELECT * FROM bank.employee WHERE name ='" + name + "';";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }
    public boolean checkPassword(char[] password, String name) {
    	String pass="";
    	try {
            String query = "SELECT * FROM bank.employee WHERE name =" + "'" + name + "'"+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
            	pass = resultSet.getString("password");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    	if(password.length== pass.length()) {
    		for(int i = 0 ; i < password.length;i++) {
    			if(password[i]!=pass.charAt(i)) {
    				return false;
    			}
    		}
    	}
    	return true;
    }
}
