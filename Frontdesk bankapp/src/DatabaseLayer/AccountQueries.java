package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AccountQueries {

    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private DBConnect connection = null;

    public AccountQueries(){
    	connection = new DBConnect();
    }

    public void addAccount(String type, String money, String date, String idclient){
        try{
            String query = "INSERT INTO bank.account (account_type, account_money, account_date, account_idclient) VALUES ('" +type+ "', '" +money+ "', '" +date+ "', '" +idclient+ "');";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateAccount(String idaccount, String money, String type){
        try{
            String query = "update bank.account set account_money = '" +money+ "', account_type ='"+ type +"' WHERE idaccount = " +idaccount+ ";" ;
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAccount(String idaccount) {
        try{
            String query = "delete from bank.account where idaccount = " +idaccount+ ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Object[][] getAllAccounts(){
        Object[][] rezultat = new Object[1000][5];
        try{
            String query = "SELECT * FROM bank.account;";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                int row = resultSet.getRow() - 1;
                for(int i=0; i<5; i++){
                    rezultat[row][i] = resultSet.getString(i+1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public Object[][] getAllAccountsOfClient(String idclient){
        Object[][] rezultat = new Object[1000][5];
        try{
            String query = "SELECT account.idaccount, account.account_type, account.account_money, account.account_date, account.account_idclient FROM bank.account inner join bank.client " +
                    "on account.account_idclient = client.idclient where account.account_idclient =" +idclient+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                int row = resultSet.getRow() - 1;
                for(int i=0; i<5; i++){
                    rezultat[row][i] = resultSet.getString(i+1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public Object[] getAccount(String idaccount){
        Object[] rezultat = new Object[5];
        try{
            String query = "SELECT * FROM bank.account WHERE idaccount =" +idaccount+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                for(int i=0; i<5; i++){
                    rezultat[i] = resultSet.getString(i+1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public void transfer(String idFrom, String idTo, String amount){
        try{
            String query = "update bank.account set account_money = account_money - " +amount+ " WHERE idaccount = " +idFrom+ ";" ;
            String query1 = "update bank.account set account_money = account_money + " +amount+ " WHERE idaccount = " +idTo+ ";" ;
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
            preparedStatement = connection.getDBConnect().prepareStatement(query1);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getMoney(String idaccount){
        String money = "";
        try {
            String query = "Select * from bank.account where idaccount = " +idaccount;
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                money = resultSet.getString("account_money");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return money;
    }
    public void setMoney(String idaccount, String amount) {
    	try{
            String query = "update bank.account set account_money = account_money-" +amount+ " WHERE idaccount = " +idaccount+ ";" ;
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
