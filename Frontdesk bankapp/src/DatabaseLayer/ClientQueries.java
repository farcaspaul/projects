package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ClientQueries {

    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private DBConnect connection = null;

    public ClientQueries() {
    	connection = new DBConnect();
    }


    public void addClient(String name, String cnp, String address ) {
        try {
            String query = "INSERT INTO client (name, cnp, address) VALUES ('" + name + "', '" + cnp + "', '" + address+ "');";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updateClient(String idlient, String name, String CNP, String address) {}

    public void updateClientName(String idclient, String name) {
        try {
            String query = "update bank.client set name = '" + name + "' WHERE idclient = " + idclient + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateClientCNP(String idclient, String CNP) {
        try {
            String query = "update bank.client set cnp = '" + CNP + "' WHERE idclient = " + idclient + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateClientAddress(String idclient, String address) {
        try {
            String query = "update bank.client set address = '" + address + "' WHERE idclient = " + idclient + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Object[] getClient(String idclient) {
        Object[] rezultat = new Object[4];
        try {
            String query = "SELECT * FROM bank.client WHERE idclient =" + idclient + ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                for (int i = 0; i < 4; i++) {
                    rezultat[i] = resultSet.getString(i + 1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }
    
    public Object[] getClientByName(String name) {
    	Object[] rezultat = new Object[4];
        try {
            String query = "SELECT * FROM bank.client WHERE name =" + "'" + name + "'"+ ";";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                for (int i = 0; i < 4; i++) {
                    rezultat[i] = resultSet.getString(i + 1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }
    
    
    
    public Object[][] getAllClients() {
        Object[][] rezultat = new Object[1000][4];
        try {
            String query = "SELECT * FROM bank.client;";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int row = resultSet.getRow() - 1;
                for (int i = 0; i < 4; i++) {
                    rezultat[row][i] = resultSet.getString(i + 1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }
    
    public boolean checkClient(String id) {
        boolean found = false;
        try {
            String query = "SELECT * FROM bank.client WHERE idclient ='" + id + "';";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }
    
    public boolean checkClientByName(String name) {
        boolean found = false;
        try {
            String query = "SELECT * FROM bank.client WHERE name ='" + name + "';";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (resultSet.isFirst())
                    found = true;
                else
                    found = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }
    
    public void deleteClient(String idClient) {
        try {
        	String query2 = "delete from bank.account where account_idclient = " + idClient + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query2);
            preparedStatement.executeUpdate();
            String query = "delete from bank.client where idclient = " + idClient + ";";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
            
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
}
