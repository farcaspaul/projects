package DatabaseLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ActivityQueries {

    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private DBConnect connection = null;

    public ActivityQueries(){
    	connection = new DBConnect();
    }

    
    
    
    public void addActivity(String type, String date, String idemployee){
        try{

            String query = "INSERT INTO bank.activity (type, date, idemployee) VALUES ('" +type+ "', '" +date+ "', " + idemployee + ");";
            preparedStatement = connection.getDBConnect().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
    
    
    public Object[][] viewAllActivity(){
        Object[][] rezultat = new Object[1000][4];
        try{
            String query = "SELECT * FROM bank.activity;";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                int row = resultSet.getRow() - 1;
                for(int i=0; i<5; i++){
                    rezultat[row][i] = resultSet.getString(i+1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }

    public Object[][] viewEmployeeActivity(String idemployee, String startDate, String endDate){
        Object[][] rezultat = new Object[50][4];
        try{
            String query = "SELECT activity.idactivity, activity.idemployee, activity.type, activity.date from bank.activity INNER JOIN bank.employee\r\n" + 
            		"                     on activity.idemployee = employee.idemployee WHERE activity.idemployee = '"+idemployee+"' AND activity.date >= '"+startDate+"' AND activity.date <= '"+endDate+"';";
            statement = connection.getDBConnect().createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                int row = resultSet.getRow() - 1;
                for(int i=0; i<4; i++){
                    rezultat[row][i] = resultSet.getString(i+1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rezultat;
    }
}
